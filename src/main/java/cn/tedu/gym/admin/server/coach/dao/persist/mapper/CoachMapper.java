package cn.tedu.gym.admin.server.coach.dao.persist.mapper;

import cn.tedu.gym.admin.server.coach.pojo.entity.Coach;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachListItemVO;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachStandardVO;
import cn.tedu.gym.admin.server.user.pojo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CoachMapper extends BaseMapper<Coach> {
    /**
     * 查询教练列表
     * @return 教练列表
     */
    List<CoachListItemVO> list();

    /**
     * 根据教练id查询教练详情
     * @param id 教练id
     * @return 封装教练信息的对象
     */
    CoachStandardVO getStandardById(Long id);

    /**
     * 添加教练
     *
    void addNew(CoachAddNewParam coachAddNewParam);
     * @return*/

//    /**
//     * 根据教练id修改教练详情
//     * @param coach 教练详情对象
//     * @return 修改成功的条数
//     */
//    int updateById(Coach coach);
//
//    /**
//     * 根据教练id修改教练详情
//     * @param user 用户详情对象
//     * @return 修改成功的条数
//     */
//    int updateById(User user);



}