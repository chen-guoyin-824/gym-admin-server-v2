package cn.tedu.gym.admin.server.member.pojo.vo;

import io.swagger.models.auth.In;
import lombok.Data;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;

import java.time.LocalDateTime;

@Data
public class CartStandardVO {
    private Long id;
    private String username; //会员名字
    private String name;    //会员卡等级名称
    private Integer count;//剩余次数
    private Integer status;//当前状态
    private Integer salary;//剩余金额
    private String address;//场馆地址
    private LocalDateTime gmtCreate; // '数据创建时间',
}
