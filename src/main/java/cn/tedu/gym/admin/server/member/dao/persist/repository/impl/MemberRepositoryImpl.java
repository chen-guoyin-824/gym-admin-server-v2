package cn.tedu.gym.admin.server.member.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import cn.tedu.gym.admin.server.member.dao.persist.mapper.MemberMapper;
import cn.tedu.gym.admin.server.member.dao.persist.repository.MemberRepository;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMember;
import cn.tedu.gym.admin.server.member.pojo.vo.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Wrapper;
import java.util.List;

@Slf4j
@Repository
public class MemberRepositoryImpl implements MemberRepository {
    @Autowired
    MemberMapper memberMapper;

    @Override
    public PageData<MemberListItemVO> list1(Integer pageNum, Integer pageSize) {
        log.debug("开始执行[查询会员列表]的数据访问, 参数: 页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<MemberListItemVO> list = memberMapper.list1();
        System.out.println("----------------------------------------------------");
        list.forEach(System.out::println);
        PageInfo<MemberListItemVO> pageInfo = new PageInfo<>(list);
        PageData<MemberListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public int insert(GymMember gymMember) {
        log.debug("开始执行[插入会员数据], 参数: {}", gymMember);
        return memberMapper.insert(gymMember);
    }

    @Override
    public MemberStandardVO getStandardById(Long id) {
        log.debug("开始执行[根据会员id查询会员详情], 参数: {}", id);
        MemberStandardVO standardById = memberMapper.getStandardById(id);
        return standardById;
    }

//    @Override
//    public MemberListItemDTO getListById(Long id) {
//        log.debug("开始执行[根据会员id查询会员详情], 参数: {}", id);
//        MemberListItemDTO listById = memberMapper.getListById(id);
//        return listById;
//    }

    @Override
    public PageData<MemberBookingListItemVO> listBooking(Integer pageNum, Integer pageSize) {
        log.debug("开始执行[查询会员预约记录列表]的数据请求, 页码:{},每页记录数:{}",pageNum,pageSize);
        PageHelper.startPage(pageNum,pageSize);
        List<MemberBookingListItemVO> list = memberMapper.listBooking();
        PageInfo<MemberBookingListItemVO>pageInfo=new PageInfo<>(list);
        PageData<MemberBookingListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public MemberListItemVO getListByCardId(Long id) {
        log.debug("开始执行[根据会员卡id查询会员详情], 参数: {}", id);
        MemberListItemVO standardById = memberMapper.getListById(id);
        return standardById;
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除标签数据】，参数：{}", id);
        return memberMapper.deleteById(id);
    }

    @Override
    public List<MemberNewListVO> memberNew() {
        log.debug("开始执行【用于首页展示最新办卡】");
        return memberMapper.memberNew();
    }

    @Override
    public HomeMemberVO homeMember() {
        log.debug("开始执行【用于首页展示会员相关】");
        return memberMapper.homeMember();
    }


}
