package cn.tedu.gym.admin.server.gymClass.dao.persist.mapper;

import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassMember;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberBookMapper extends BaseMapper<ClassMember> {

    /**
     * 根据课程ID查询预约详情列表
     * @return 预约详情列表
     */
    List<ClassBookDetailListVO> listBookingDetail(Long bookClassId);

}
