package cn.tedu.gym.admin.server.admin.service;

import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminAddNewParam;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminUpdateInfoParam;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserListItemVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;

public interface IAdminService {
    /**
     * 启用状态的显示文本
     */
    String ENABLE_TEXT[] = {"启用","禁用" };
    /**
     * 根据id查询管理员信息
     * @param id 管理员id
     * @return
     */
    UserListItemVO adminInfoById(Long id);

    /**
     * 修改管理员信息
     * @param adminUpdateInfoParam 封装了修改新数据的对象
     * @return
     */
    int updateAdminInfoById(AdminUpdateInfoParam adminUpdateInfoParam);


    /**
     * 修改管理员头像
     * @param id id
     * @param imgUrl 头像路径
     * @return
     */
    int updateAvatar(Long id,String imgUrl);
    /**
     * 添加管理员
     * @param adminAddNewParam 封装了新增管理员信息的对象
     * @return 受影响的行数
     */
    void addNewAdmin(AdminAddNewParam adminAddNewParam);
    /**
     * 查询管理员列表
     * @param pageNum 页码
     * @return
     */
    PageData<UserDetailListVO> adminDetailList(Integer pageNum);
    /**
     * 查询管理员列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return
     */
    PageData<UserDetailListVO> adminDetailList(Integer pageNum, Integer pageSize);
    /**
     * 根据ID删除管理员
     * @param id 管理员ID
     * @return
     */
    void deleteById(Long id);
    /**
     * 启用用户
     *
     * @param id 用户ID
     */
    void setEnable(Long id);

    /**
     * 禁用用户
     *
     * @param id 用户ID
     */
    void setDisable(Long id);
}
