package cn.tedu.gym.admin.server.gymInfo.service.impl;

import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.gymInfo.dao.persist.repository.IGymInfoRepository;
import cn.tedu.gym.admin.server.gymInfo.pojo.entity.GymInfo;
import cn.tedu.gym.admin.server.gymInfo.pojo.param.GymInfoAddParam;
import cn.tedu.gym.admin.server.gymInfo.pojo.param.GymInfoUpdateInfoParam;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoListItemVO;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoStandardVO;
import cn.tedu.gym.admin.server.gymInfo.service.IGymInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @auther xw
 * @create 2023/7/7
 */
@Service
@Slf4j
public class GymInfoService implements IGymInfoService {
    //定义注入到分页参数中的默认大小默认值
    @Value("5")
    private Integer defaultQueryPageSize;
    @Autowired
    private IGymInfoRepository gymInfoRepository;

    public GymInfoService(){
        log.info("创建业务对象: GymInfoService");
    }
    @Override
    public void addNew(GymInfoAddParam addParam) {
        log.debug("开始处理【新增场馆】的业务，参数：{}", addParam);
        String title = addParam.getTitle();
        int count = gymInfoRepository.countByTitle(title);
        if (count > 0) {
            String message = "新增场馆失败，场馆名称已经被占用！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        GymInfo gymInfo = new GymInfo();
        BeanUtils.copyProperties(addParam, gymInfo);
        int rows = gymInfoRepository.insert(gymInfo);
        if (rows != 1) {
            String message = "新增场馆失败，服务器忙，请稍后再试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }


    /**
     * 根据id删除
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        log.info("service开始处理删除,id{}",id);

        //TODO 删除业务的逻辑功能
        gymInfoRepository.deleteById(id);
    }


    @Override
    public PageData<GymInfoListItemVO> list(Integer pageNum) {
        log.info("service层开始处理[查询场馆列表]业务,页码:{}",pageNum);
        PageData<GymInfoListItemVO> pageData = gymInfoRepository.list(pageNum, defaultQueryPageSize);
        return pageData;
    }
    @Override
    public PageData<GymInfoListItemVO> list(Integer pageNum,Integer pageSize) {
        log.info("service层开始处理[查询场馆列表]业务,页码:{}",pageNum,pageSize);
        PageData<GymInfoListItemVO> pageData = gymInfoRepository.list(pageNum, pageSize);
        return pageData;
    }

    //通过setEnable()和setDisable()调用updateEnableById()更改状态
    @Override
    public void setEnable(Long id) {
        updateEnableById(id, 1);
    }
    @Override
    public void setDisable(Long id) {
        updateEnableById(id, 0);
    }
    private void updateEnableById(Long id, Integer enable) {
        log.debug("开始处理【{}场馆】的业务，ID：{}，目标状态：{}", ENABLE_TEXT[enable], id, enable);
        GymInfoStandardVO queryResult = gymInfoRepository.getStandardById(id);
        //时效性验证
        if (queryResult == null) {
            String message = ENABLE_TEXT[enable] + "场馆失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        if (queryResult.getEnable().equals(enable)) {
            String message = ENABLE_TEXT[enable] + "场馆失败，当前用户已经处于"
                    + ENABLE_TEXT[enable] + "状态！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        GymInfo gymInfo = new GymInfo();
        gymInfo.setId(id);
        gymInfo.setEnable(enable);
        log.debug("即将修改数据，参数：{}", gymInfo);
        int rows = gymInfoRepository.updateById(gymInfo);
        if (rows != 1) {
            String message = ENABLE_TEXT[enable] + "用户失败，服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }

        // 将用户状态同步写入到Redis中
        // Integer enableByUserId = userCacheRepository.getEnableByUserId(id);
        // if (enableByUserId != null) {
        //     userCacheRepository.saveEnableByUserId(id, enable);
        // }
    }

    @Override
    public void updateById(Long id ,GymInfoUpdateInfoParam gymInfoUpdateInfoParam) {
        log.debug("开始处理【修改场馆】的业务，id：{}", gymInfoUpdateInfoParam);
        GymInfoStandardVO queryResult = gymInfoRepository.getStandardById(id);
        if(queryResult == null){
            String message = "修改场馆失败,尝试修改的场馆数据不存在!";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,message);
        }

        int count = gymInfoRepository.countByTitle(gymInfoUpdateInfoParam.getTitle());
        if(count>0 && !gymInfoUpdateInfoParam.getTitle().equals(queryResult.getTitle())){
            String message = "修改场馆失败,该名称已被占用!";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,message);
        }

        GymInfo gymInfo = new GymInfo();
        gymInfo.setId(id);
        BeanUtils.copyProperties(gymInfoUpdateInfoParam,gymInfo);
        int rows = gymInfoRepository.updateById(gymInfo);
        if(rows !=1){
            String message = "修改场馆失败,服务器忙,请稍后再试";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT,message);
        }

    }

    @Override
    public List<GymInfoStandardVO> getStandardByAddress(String address) {
        log.debug("开始处理【按地址查询】的业务，address：{}", address);
        List<GymInfoStandardVO> list = gymInfoRepository.getStandardByAddress(address);
        return list;
    }

    @Override
    public GymInfoStandardVO getStandardById(Long id) {
        log.debug("开始处理【按Id查询】的业务，id：{}", id);
        GymInfoStandardVO queryResult = gymInfoRepository.getStandardById(id);
        return queryResult;
    }


}
