package cn.tedu.gym.admin.server.equipment.pojo.param;


import lombok.Data;

import java.io.Serializable;
@Data
public class EquipmentAddNewParam implements Serializable {
    /**
     * 分类id
     */
    private Long typeId;
    /**
     * 器材名称
     */
    private String name;

    /**
     * 器材图片
     */
    private String img;

    /**
     * 是否启用， 1=启用， 0=不启用
     */
    private Integer enable;

    /**
     * 器材状态
     */
    private Integer state;

    /**
     * 备注信息
     */
    private String remark;

}
