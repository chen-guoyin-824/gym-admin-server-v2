package cn.tedu.gym.admin.server.coach.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName("gym_coach")
public class Coach {
    /**
     * 教练ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 个人签名
     */
    private String target;
    /**
     * 个人简介
     */
    private String profile;
    /**
     * 从业时间
     */
    private String worktime;
    /**
     * 擅长标签
     */
    private String skills;

    private String photo;
    /**
     * 每日私教课上限
     */
    private Integer upperLimit;
    /**
     * 场馆id
     */
    private Long gymId;
    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;
    /**
     * 数据最后一次修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;
}
