package cn.tedu.gym.admin.server.equipment.pojo.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
/**
 * 列表项VO类：器材信息
 *
 */
@Data
public class EquipmentListItemVO implements Serializable {
    /**
     * 器材id
     */
    private Long id;
    /**
     * 分类名字
     */
    private String typeName;
    /**
     * 分类名字
     */
    private Long typeId;
    /**
     * 器材名称
     */

    private String name;

    /**
     * 器材图片
     */
    private String img;

    /**
     * 是否启用， 1=启用， 0=不启用
     */
    private int enable;

    /**
     * 器材状态
     */
    private Integer state;

    /**
     * 备注信息
     */
    private String remark;


}
