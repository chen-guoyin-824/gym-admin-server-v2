package cn.tedu.gym.admin.server.coach.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CoachListItemVO implements Serializable {
    private Integer id;
    private Integer userId;
//    教练名
    private String name;
//    性别
    private String sex;
//    电话
    private String phone;
//    所属场馆
    private String address;
//    角色排序
    private Integer sort;
//    启用状态
    private Integer enable;

}
