package cn.tedu.gym.admin.server.coach.dao.persist.repository;


import cn.tedu.gym.admin.server.coach.pojo.entity.Coach;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachListItemVO;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;

public interface CoachRepository {

    /**
     * 添加教练信息
     * @param  */
    void insert(Coach coach);
    /**
     * 查询教练列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageData<CoachListItemVO> list(Integer pageNum, Integer pageSize);
    /**
     * 插入课程
     * @param
     * @return 影响课程表的行数
     *//*
    int insert(GymClass gymClass);

    */
    /*
     * 根据id查询教练详情
     * @param id 课程id
     * @return 封装课程详情的对象
     */
   CoachStandardVO getStandardById(Long id);


  /**
     * 预约记录列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 课程预约列表
     *//*
    PageData<ClassBookListItemVO> listBooking(Integer pageNum, Integer pageSize);*/

    /**
     * 根据ID删除教练
     * @param id 教练ID
     * @return
     */
    int deleteById(Long id);

    /**
     * 根据ID修改教练
     * @param coach 教练信息
     * @return
     */
    int updateById(Coach coach);
}