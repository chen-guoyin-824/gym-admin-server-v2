package cn.tedu.gym.admin.server.gymInfo.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import cn.tedu.gym.admin.server.gymInfo.dao.persist.mapper.GymInfoMapper;
import cn.tedu.gym.admin.server.gymInfo.dao.persist.repository.IGymInfoRepository;
import cn.tedu.gym.admin.server.gymInfo.pojo.entity.GymInfo;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoListItemVO;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @auther xw
 * @create 2023/7/7
 */
@Slf4j
@Repository
public class GymInfoRepository implements IGymInfoRepository {
    @Autowired
    GymInfoMapper gymInfoMapper;

    @Override
    public int insert(GymInfo gymInfo) {
        log.info("repository处理添加,参数{}",gymInfo);
        return gymInfoMapper.insert(gymInfo);
    }

    @Override
    public int deleteById(Long id) {
        log.info("repository处理删除业务,id{}",id);
        return gymInfoMapper.deleteById(id);
    }

    @Override
    public int updateById(GymInfo gymInfo) {
        log.debug("开始执行【根据ID修改标签数据】，参数：{}", gymInfo);
        return gymInfoMapper.updateById(gymInfo);
    }

    @Override
    public List<GymInfoStandardVO> getStandardByAddress(String address) {
        log.debug("开始执行【根据地址查询场馆】的数据访问，参数：{}", address);

        return gymInfoMapper.getStandardByAddress(address);
    }

    @Override
    public GymInfoStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询场馆】的数据访问，参数：{}", id);
        return gymInfoMapper.getStandardById(id);
    }

    @Override
    public PageData<GymInfoListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询场馆列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum,pageSize);
        List<GymInfoListItemVO> list = gymInfoMapper.list();
        PageInfo<GymInfoListItemVO> pageInfo =new PageInfo<>(list);
        PageData<GymInfoListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public int countByTitle(String title) {
        log.debug("开始执行【根据名称查询场馆】的数据访问，场馆名称:{}",title);
        QueryWrapper<GymInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("title",title);
        return gymInfoMapper.selectCount(queryWrapper);
    }


}
