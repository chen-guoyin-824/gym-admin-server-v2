package cn.tedu.gym.admin.server.gymClass.controller;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassBookAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassBookInsertParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;
import cn.tedu.gym.admin.server.gymClass.service.ClassBookService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalTime;

@RestController
@RequestMapping("/gym/class/book")
@Slf4j
@Validated
@Api(tags = "2.1.课程预约管理模块")
public class ClassBookController {
    @Autowired
    ClassBookService classBookService;

    @PostMapping("add-new")
    @ApiOperation("添加排期")
    @ApiOperationSupport(order = 110)
    public JsonResult addNew(@Valid ClassBookInsertParam classBookInsertParam){
        log.debug("开始处理[添加添加排期]的业务, 参数: {}", classBookInsertParam);
        ClassBookAddNewParam classBookAddNewParam = new ClassBookAddNewParam();
        BeanUtils.copyProperties(classBookInsertParam, classBookAddNewParam);
        classBookAddNewParam.setClassDate(LocalDate.parse(classBookInsertParam.getClassDate()));
        classBookAddNewParam.setClassTime(LocalTime.parse(classBookInsertParam.getClassTime()));
        classBookService.insertBookClass(classBookAddNewParam);
        return JsonResult.ok();
    }

    @GetMapping("list-booking")
    @ApiOperation("查询预约记录列表")
    @ApiOperationSupport(order = 430)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "long"),
    })
    public JsonResult listBooking(@Range(min = 1, message = "请提交有效的页码值！") Integer page){
        log.debug("开始处理[查询预约记录列表]的业务, 参数: 页码: {}", page);
        PageData<ClassBookListItemVO> pageData = classBookService.listBooking(page);
        return JsonResult.ok(pageData);
    }

    @GetMapping("get-booking-detail")
    @ApiOperation("根据预约ID查询预约详情")
    @ApiOperationSupport(order = 431)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookId", value = "预约ID", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult getBookingDetail(@Range(min = 1, message = "请提交有效的课程ID值！")Long bookId){
        log.debug("开始处理[根据预约ID查询预约详情列表]的业务, 课程ID:{}",bookId);
        ClassBookStartedVO bookStandardById = classBookService.getBookStandardById(bookId);
        return JsonResult.ok(bookStandardById);
    }

    @GetMapping("list-ByClassIdANDCoachId")
    @ApiOperation("根据课程ID和教练ID查询列表")
    @ApiOperationSupport(order = 432)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "classId", value = "课程ID", defaultValue = "1", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "coachId", value = "教练ID", defaultValue = "1", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageNum", value = "页码", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult listByClassIdANDCoachId(Long classId,Long coachId,Integer pageNum){
        log.debug("开始处理[根据课程ID和教练ID查询列表]的请求, 课程ID:{},教练ID:{},页码:{}",classId, coachId,pageNum);
        PageData<ClassBookListItemVO> pageData = classBookService.listByClassIdANDCoachId(classId,coachId,pageNum);
        return JsonResult.ok(pageData);
    }
}
