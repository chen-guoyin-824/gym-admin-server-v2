package cn.tedu.gym.admin.server.coach.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.coach.dao.persist.mapper.CoachMapper;
import cn.tedu.gym.admin.server.coach.dao.persist.repository.CoachRepository;
import cn.tedu.gym.admin.server.coach.pojo.entity.Coach;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachListItemVO;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import cn.tedu.gym.admin.server.user.dao.persist.mapper.UserMapper;
import cn.tedu.gym.admin.server.user.pojo.entity.User;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
public class CoachRepositoryImpl implements CoachRepository {

    @Autowired
    CoachMapper coachMapper;
    @Autowired
    UserMapper userMapper;

    @Override
    public void insert(Coach coach) {
        log.debug("开始执行[添加教练]的数据访问, 参数:{}", coach);
       coachMapper.insert(coach);
    }

    @Override
    public PageData<CoachListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行[查询教练列表]的数据访问, 参数: 页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<CoachListItemVO> list = coachMapper.list();
        PageInfo<CoachListItemVO> pageInfo = new PageInfo<>(list);
        PageData<CoachListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public CoachStandardVO getStandardById(Long id) {
        log.debug("开始执行[根据会员id查询教练详情], 参数: {}", id);
        CoachStandardVO standardById = coachMapper.getStandardById(id);
        return standardById;
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行[根据教练id删除教练], 参数: {}", id);
        return coachMapper.deleteById(id);
    }

    @Override
    public int updateById(Coach coach) {
        log.debug("开始执行【根据ID修改教练数据】，参数：{}", coach);
        return coachMapper.updateById(coach);
    }

}