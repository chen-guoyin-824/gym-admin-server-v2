package cn.tedu.gym.admin.server.member.pojo.vo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MemberStandardVO {
    private Long  id;          // '会员详情id',
    private Long userId;      // '用户id',
    private Long coachId;//教练Id
    private String birthday;     // '生日',
    private String idcard;       // '身份证号',
    private String city;         // '城市',
    private String career;       // '职业',
    private Integer height;       // '身高',
    private Integer weight;       // '体重',
    private String signature;    // '个性签名',
    private String exp;          // '健身经验',
    private String target; //'健身目的',
    private String coachName;//教练名字
    private LocalDateTime gmtCreate;//数据创建时间
    private LocalDateTime gmtModified;//数据最后修改时间
    private String username;//用户名
    private String name;//昵称
    private Integer gender;//性别
    private String phone;//手机号
    private String avatar;//头像

}
