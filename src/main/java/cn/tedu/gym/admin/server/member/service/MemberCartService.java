package cn.tedu.gym.admin.server.member.service;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMemberCart;
import cn.tedu.gym.admin.server.member.pojo.param.GymMemberCartAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.param.MemberAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.vo.CartStandardVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface MemberCartService {
    void updateCountById(Long id);
    void updateStatusById(Long id);
    void insert(GymMemberCartAddNewParam gymMemberCartAddNewParam);
    void insertPlus(MemberAddNewParam memberAddNewParam);
    void delete(Long id);
    GymMemberCart getStandardById(Long id);
    PageData<GymMemberCart> list(Integer pageNum);
    PageData<GymMemberCart> list(Integer pageNum, Integer pageSize);
    void addTimesById(Long id, int counts, boolean manuallyInput);
    void addTimesById(Long id);
    List<CartStandardVO> getListById(Long id);

}
