package cn.tedu.gym.admin.server.member.pojo.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CartStandard1VO {
    private Long id;
    private  String username; //会员名字
    private String name;    //会员卡等级名称
    private Integer count;//剩余次数
    private Integer status;//当前状态
    private  Integer salary;//剩余金额
    private String address;//场馆地址
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate; // '数据创建时间',
}
