package cn.tedu.gym.admin.server.equipment.pojo.vo;


import lombok.Data;

import java.time.LocalDateTime;
/**
 * 标准VO类：器材信息
 *
 */
@Data
public class EquipmentStandardVO {
    /**
     * 器材id
     */
    private Long id;
    /**
     * 分类id
     */
    private Long typeId;
    /**
     * 器材名称
     */
    private String name;

    /**
     * 器材图片
     */
    private String img;


    /**
     * 是否启用， 1=启用， 0=不启用
     */
    private Integer enable;

    /**
     * 器材状态
     */
    private Integer state;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 数据创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 数据最后修改时间
     */
    private LocalDateTime gmtModified;

}
