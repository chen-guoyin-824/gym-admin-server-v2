package cn.tedu.gym.admin.server.member.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;
/**
 * 查询会员的VO类
 */
@Data
@TableName("gym_member")
public class GymMember {
    @TableId(type = IdType.AUTO)
    private Long  id;          // '会员详情id',
    private Long userId;      // '用户id',
    private String birthday;     // '生日',
    private String idcard;       // '身份证号',
    private String city;         // '城市',
    private String career;       // '职业',
    private Integer height;       // '身高',
    private Integer weight;       // '体重',
    private String signature;    // '个性签名',
    private String exp;          // '健身经验',
    private Long coachId;//教练id
    private String target; //'健身目的',
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate; // '数据创建时间',
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified; // '数据最后修改时间'
}
