package cn.tedu.gym.admin.server.admin.dao.persist.mapper;

import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserListItemVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminMapper extends BaseMapper<User> {
    //查询管理员信息
    UserListItemVO adminInfoById(Long id);
    /**
     * 查询管理员信息详情
     * @param id 管理员id
     * @return
     */
    UserStandardVO adminDetailById(Long id);

    /**
     * 查询管理员列表
     * @return
     */
    List<UserDetailListVO>adminDetailList();


}
