package cn.tedu.gym.admin.server.gymClass.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * 查询课程预约记录的VO类
 */
@Data
public class ClassBookListItemVO implements Serializable {
    /**
     * 课程id
     */
    private Long id;
    /**
     * 课程名称
     */
    private String name;
    /**
     * 课程时长
     */
    private Integer duration;
    /**
     * 教练名字
     */
    private String coachName;
    /**
     * 课程日期(年月日)
     */
    private LocalDate classDate;
    /**
     * 上课时间
     */
    private LocalTime classTime;
    /**
     * 预约人数
     */
    private Integer bookNumber;
}
