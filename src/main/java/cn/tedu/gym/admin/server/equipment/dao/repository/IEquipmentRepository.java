package cn.tedu.gym.admin.server.equipment.dao.repository;

import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.equipment.pojo.entity.Equipment;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentTypeListVO;

import java.util.List;

/**
 * 处理器材数据的存储库接口
 *
 */
public interface IEquipmentRepository {

    /**
     * 查询器材列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return
     */
    PageData<EquipmentListItemVO> list(Integer pageNum, Integer pageSize);

    /**
     * 根据ID删除器材
     * @param id 器材ID
     * @return
     */
    int deleteById(Long id);


    /**
     * 根据ID查询器材信息
     * @param id 器材ID
     * @return
     */
    EquipmentStandardVO getStandardVOById(Long id);

    /**
     * 新增器材
     * @param equipment 封装了器材信息的类
     * @return
     */
    int insert(Equipment equipment);

    /**
     * 查询器材类别列表
     * @return
     */
    List<EquipmentTypeListVO> typeList();

    /**
     * 根据器材id修改器材的数据
     * @param equipment 封装了器材id和新的数据的对象
     * @return 受影响的行数
     */
    int updateById(Equipment equipment);

}

