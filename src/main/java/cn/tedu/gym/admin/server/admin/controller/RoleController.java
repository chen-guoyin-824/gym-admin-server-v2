package cn.tedu.gym.admin.server.admin.controller;

import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;
import cn.tedu.gym.admin.server.admin.service.IRoleService;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 处理角色相关请求的控制器
 *
 */
@Slf4j
@RestController
@RequestMapping("/admin")
@Validated
@Api(tags = "1.2. 账号管理-角色管理")
public class RoleController {

    @Autowired
    private IRoleService roleService;

    public RoleController() {
        log.debug("创建控制器类对象：RoleController");
    }

    @GetMapping("/roles")
    @ApiOperation("查询角色列表")
    @ApiOperationSupport(order = 420)
    public JsonResult list() {
        log.debug("开始处理【查询角色列表】的请求，无参数");
        List<RoleListItemVO> list = roleService.list();
        return JsonResult.ok(list);
    }

}
