package cn.tedu.gym.admin.server.equipment.service.impl;

import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserStandardVO;
import cn.tedu.gym.admin.server.equipment.dao.repository.IEquipmentRepository;
import cn.tedu.gym.admin.server.equipment.pojo.entity.Equipment;
import cn.tedu.gym.admin.server.equipment.pojo.param.EquipmentAddNewParam;
import cn.tedu.gym.admin.server.equipment.pojo.param.EquipmentUpdateParam;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentStandardVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentTypeListVO;
import cn.tedu.gym.admin.server.equipment.service.IEquipmentService;
import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 处理器材数据的业务实现类
 *
 */
@Slf4j
@Service
public class EquipmentServiceImpl implements IEquipmentService {
    @Value("${gym.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;

    @Autowired
    private IEquipmentRepository equipmentRepository;

    @Override
    public PageData<EquipmentListItemVO> list(Integer pageNum ) {
        log.debug("开始处理查询器材列表的业务请求,页码:{}",pageNum);
        return equipmentRepository.list(pageNum,defaultQueryPageSize);
    }
    @Override
    public PageData<EquipmentListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始处理查询器材列表的业务请求,页码:{},每页记录数:{}",pageNum,pageSize);
        return equipmentRepository.list(pageNum,pageSize);
    }

    @Override
    public void deleteById(Long id) {
        log.debug("开始处理根据ID删除器材的业务请求,ID:{}",id);
        //查询器材是否存在
        EquipmentStandardVO equipmentStandard = equipmentRepository.getStandardVOById(id);
        if (equipmentStandard==null){
            String message="操作失败,当前器材不存在!";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,message);
        }
        int rows = equipmentRepository.deleteById(id);
        if (rows!=1){
            String message="服务器忙,请稍后再试!";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_UNKNOWN,message);
        }
    }

    @Override
    public int insert(EquipmentAddNewParam equipmentAddNewParam) {
        log.debug("开始处理新增器材的业务请求,参数:{}",equipmentAddNewParam);
        Equipment equipment=new Equipment();
        BeanUtils.copyProperties(equipmentAddNewParam,equipment);
        equipmentRepository.insert(equipment);
        return 0;
    }

    @Override
    public List<EquipmentTypeListVO> typeList() {
        return equipmentRepository.typeList();
    }

    @Override
    public void setEnable(Long id) {
        updateEnableById(id,1);
    }

    @Override
    public void setDisable(Long id) {
        updateEnableById(id,0);
    }

    @Override
    public void updateById(EquipmentUpdateParam equipmentUpdateParam) {

        EquipmentStandardVO standardVO = equipmentRepository.getStandardVOById(equipmentUpdateParam.getId());
        if (standardVO == null) {
            String message = "修改器材失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        Equipment equipment=new Equipment();
        BeanUtils.copyProperties(equipmentUpdateParam,equipment);
        equipment.setEnable(null);
        equipmentRepository.updateById(equipment);
    }

    private void updateEnableById(Long id, Integer enable) {
        log.debug("开始处理【{}器材】的业务，ID：{}，目标状态：{}", ENABLE_TEXT[enable], id, enable);
        EquipmentStandardVO standardVO = equipmentRepository.getStandardVOById(id);
        if (standardVO == null) {
            String message = ENABLE_TEXT[enable] + "器材失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        if (standardVO.getEnable().equals(enable)) {
            String message = ENABLE_TEXT[enable] + "器材失败，当前器材已经处于"
                    + ENABLE_TEXT[enable] + "状态！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        Equipment equipment=new Equipment();
        equipment.setId(id);
        equipment.setEnable(enable);
        log.debug("即将修改数据，参数：{}", equipment);
        int rows = equipmentRepository.updateById(equipment);
        if (rows != 1) {
            String message = ENABLE_TEXT[enable] + "器材失败，服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }

    }
}
