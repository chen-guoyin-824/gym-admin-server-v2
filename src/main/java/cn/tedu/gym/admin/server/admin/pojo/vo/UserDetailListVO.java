package cn.tedu.gym.admin.server.admin.pojo.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class UserDetailListVO implements Serializable {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;

    /**
     * 姓名
     */
    private String name;
    /**
     * 性别,0表示保密,1表示男,2表示女
     */
    private Integer gender;
    /**
     * 电话号码
     */
    private String phone;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 启用 0表示启用,1表示禁用
     */
    private Integer enable;
    /**
     * 排序序号
     */
    private Integer sort;
}
