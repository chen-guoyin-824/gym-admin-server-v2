package cn.tedu.gym.admin.server.member.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 查询会员的VO类
 */
@Data
@TableName("gym_member_card")
public class GymMemberCart {
    @TableId(type = IdType.AUTO)
    private Long id;          // '会员卡id',
    private Long memberId;          // '会员id',
    private String name;    //会员卡等级名称
    private Integer memberLevel; //会员卡等级
    private Integer count; //剩余次数
    private Integer status; //会员卡状态
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate; // '数据创建时间',
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified; // '数据最后修改时间'
}
