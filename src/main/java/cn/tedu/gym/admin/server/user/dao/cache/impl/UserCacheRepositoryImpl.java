package cn.tedu.gym.admin.server.user.dao.cache.impl;

import cn.tedu.gym.admin.server.common.pojo.po.UserJwtInfoPO;
import cn.tedu.gym.admin.server.user.dao.cache.UserCacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

@Repository
public class UserCacheRepositoryImpl implements UserCacheRepository {
    @Value("${gym.jwt.duration-in-minute}")
    private Long durationInMinute;
    @Value("${gym.auth.auth-in-second}")
    private Long authTime;
    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;
    @Override
    public void saveLoginInfo(String jwt, UserJwtInfoPO userJwtInfoPO) {
        String key = "user:jwt:" + jwt;
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        //将得到的信息存放在缓存当中, 并且设置在缓存中的存活时长为JWT设置的一样的时长
        opsForValue.set(key, userJwtInfoPO, durationInMinute, TimeUnit.MINUTES);
    }

    @Override
    public void saveEnableByUserId(Long userId, Integer enable) {
        String key = "user:enable:" + userId;
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        //将得到的信息存放在缓存当中, 并且设置在缓存中的存活时长为JWT设置的一样的时长
        opsForValue.set(key, enable, durationInMinute, TimeUnit.MINUTES);
    }

    @Override
    public void saveAuthByUuid(String uuid, String code) {
        String key = "user:auth:" + uuid;
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        opsForValue.set(key, code, authTime, TimeUnit.SECONDS);
    }

    @Override
    public Integer getEnableByUserId(Long userId) {
        String key = "user:enable:" + userId;
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        Serializable serializable = opsForValue.get(key);
        Integer enable = (Integer) serializable;
        return enable;
    }

    @Override
    public UserJwtInfoPO getLoginInfo(String jwt) {
        String key = "user:jwt:" + jwt;
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        Serializable serializable = opsForValue.get(key);
        UserJwtInfoPO userJwtInfoPO = (UserJwtInfoPO) serializable;
        return userJwtInfoPO;
    }

    @Override
    public String getAuthByUuid(String uuid) {
        String key = "user:auth:" + uuid;
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        Serializable serializable = opsForValue.get(key);
        String code = (String) serializable;
        return code;
    }

//    @Override
//    public boolean deleteList() {
//        return false;
//    }
}
