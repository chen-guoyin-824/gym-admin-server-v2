package cn.tedu.gym.admin.server.common.pojo.po;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户登录信息的PO, 此类数据将存入到Redis中
 */
@Data
public class UserJwtInfoPO implements Serializable {
    /**
     * 权限列表的JSON字符串
     */
    private String authoritiesJsonString;
    /**
     * 用户登录时的IP地址
     */
    private String ip;
    /**
     * 用户登录时使用的浏览器的信息
     */
    private String userAgent;
}