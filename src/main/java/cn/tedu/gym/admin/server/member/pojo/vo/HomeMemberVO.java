package cn.tedu.gym.admin.server.member.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 该类用于首页展示
 */
@Data
public class HomeMemberVO implements Serializable {
    /**
     * 累计会员
     */
    private Integer countMember;
    /**
     * 今日新增会员
     */
    private Integer countToday;
    /**
     * 昨日新增会员
     */
    private Integer countYesterday;
}
