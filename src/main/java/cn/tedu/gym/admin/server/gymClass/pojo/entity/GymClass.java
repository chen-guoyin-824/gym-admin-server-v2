package cn.tedu.gym.admin.server.gymClass.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@TableName("gym_class")
public class GymClass implements Serializable {
    /**
     * 数据id
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 课程名称
     */
    private String name;
    /**
     * 课程图片
     */
    private String image;
    /**
     * 课程类别, 1=团课课程, 2=私教课课程
     */
    private Integer type;
    /**
     * 课程时长/分钟
     */
    private Integer time;
    /**
     * 最大上课人数
     */
    private Integer personMax;
    /**
     * 最少上课人数
     */
    private Integer personMin;
    /**
     * 教练ID
     */
    private Long coachId;
    /**
     * 课时费
     */
    private Double money;
    /**
     * 课程备注
     */
    private String note;
    /**
     * 课程标签
     */
    private String tag;
    /**
     * 课程介绍
     */
    private String introduce;
    /**
     * 排序序号
     */
    private Integer sort;
    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;
    /**
     * 数据最后一次修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;
}
