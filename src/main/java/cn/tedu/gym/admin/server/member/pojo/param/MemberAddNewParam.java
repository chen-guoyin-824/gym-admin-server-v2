package cn.tedu.gym.admin.server.member.pojo.param;

import lombok.Data;

@Data
public class MemberAddNewParam {
    //用户数据
    private String image;               //'存储头像',
    private Integer memberLevel;        //'会员等级',
    private String name;                // '存储姓名',
    private String username;            //'用户名',
    private Long sort;                  //'排序'
    private String password;            //'密码'
    private String gender;              //'性别'
    private String phone;               // '电话号码',
    //会员数据
    private Integer enable;             // '是否启用',
    private String birthday;            // '生日',
    private String idcard;              // '身份证号',
    private String city;                // '城市',
    private String career;              // '职业',
    private Integer height;             // '身高',
    private Integer weight;             // '体重',
    private Long coachId;               // '教练id',
    private String signature;            // '个性签名',
    private String exp;                 // '健身经验',
    private String target;              //'健身目的',

}
