package cn.tedu.gym.admin.server.user.dao.cache;

import cn.tedu.gym.admin.server.common.pojo.po.UserJwtInfoPO;

public interface UserCacheRepository {
    /**
     * 向Redis中存入数据
     * @param jwt
     * @param userJwtInfoPO
     */
    void saveLoginInfo(String jwt, UserJwtInfoPO userJwtInfoPO);

    void saveEnableByUserId(Long userId, Integer enable);

    Integer getEnableByUserId(Long userId);

    void saveAuthByUuid(String uuid, String code);

    /**
     * 根据JWT从Redis中获取用户登录信息
     * @param jwt JWT
     * @return
     */
    UserJwtInfoPO getLoginInfo(String jwt);

    String getAuthByUuid(String uuid);

//    /**
//     *
//     * @return
//     */
//    boolean deleteList();
}
