package cn.tedu.gym.admin.server.gymClass.pojo.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class MemberBookAddNewParam implements Serializable {
    /**
     * 会员id
     */
    private Long memberId;
    /**
     * 预约课程id
     */
    private Long bookClassId;
}
