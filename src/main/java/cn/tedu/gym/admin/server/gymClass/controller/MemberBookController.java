package cn.tedu.gym.admin.server.gymClass.controller;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.gymClass.pojo.param.MemberBookAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import cn.tedu.gym.admin.server.gymClass.service.MemberBookService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gym/member/book")
@Slf4j
@Validated
@Api(tags = "3.1.会员预约管理模块")
public class MemberBookController {
    @Autowired
    MemberBookService memberBookService;

    @PostMapping("add-new")
    @ApiOperation("会员预约课程")
    @ApiOperationSupport(order = 110)
    public JsonResult addNew(MemberBookAddNewParam memberBookAddNewParam){
        log.debug("开始处理[会员预约课程]的业务, 参数: {}", memberBookAddNewParam);
        memberBookService.addNew(memberBookAddNewParam);
        return JsonResult.ok();
    }

    @GetMapping("cancel-by-id")
    @ApiOperation("取消预约")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "数据ID", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult cancelById(Long id){
        log.debug("开始处理[取消预约]的业务, ID: {}", id);
        memberBookService.cancelById(id);
        return JsonResult.ok();
    }

    @GetMapping("update-sign-in-status")
    @ApiOperation("设置签到状态为已签到")
    @ApiOperationSupport(order = 300)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "数据ID", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult updateSignStatusById(Long id){
        log.debug("开始处理[设置签到状态为已签到]的业务, ID: {}", id);
        memberBookService.setSignedInStatus(id);
        return JsonResult.ok();
    }

    @GetMapping("list-booking-detail")
    @ApiOperation("根据课程ID查询预约详情列表")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookClassId", value = "课程ID", defaultValue = "1", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult listBookingDetail(@Range(min = 1, message = "请提交有效的课程ID值！")Long bookClassId, @Range(min = 1, message = "请提交有效的页码值！") Integer page){
        log.debug("开始处理[根据课程ID查询预约详情列表]的业务, 课程ID:{},页码: {}",bookClassId, page);
        PageData<ClassBookDetailListVO> pageData = memberBookService.listBookingDetail(bookClassId, page);
        return JsonResult.ok(pageData);
    }
}
