package cn.tedu.gym.admin.server.user.controller;

import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.user.pojo.param.UserLoginInfoParam;
import cn.tedu.gym.admin.server.user.pojo.vo.UserLoginResultVO;
import cn.tedu.gym.admin.server.user.service.UserService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

/**
 * 处理用户相关请求的控制器类
 *
 */
@Slf4j
@RestController
@RequestMapping("/gym/users")
@Validated
@Api(tags = "1.1. 账号管理-用户管理")
public class UserController {
    @Autowired
    private UserService userService;

    public UserController() {
        log.info("创建控制器对象：UserController");
    }

    @PostMapping("/login")
    @ApiOperation("用户登录")
    @ApiOperationSupport(order = 10)
    public JsonResult login(@Validated UserLoginInfoParam userLoginInfoParam,
                            @ApiIgnore HttpServletRequest request) {
        log.debug("开始处理【用户登录】的请求，参数：{}", userLoginInfoParam);
        String remoteAddr = request.getRemoteAddr();
        String userAgent = request.getHeader("User-Agent");
        UserLoginResultVO userLoginResultVO = userService.login(userLoginInfoParam, remoteAddr, userAgent, request);
        return JsonResult.ok(userLoginResultVO);
    }

    @PostMapping("/logout")
    @ApiOperation("用户退出登录")
    @ApiOperationSupport(order = 11)
    public JsonResult logout() {
        log.debug("开始处理【用户退出登录】的请求，无参数");
        SecurityContextHolder.clearContext();
        return JsonResult.ok();
    }
}
