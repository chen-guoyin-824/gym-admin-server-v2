package cn.tedu.gym.admin.server.admin.controller;

import cn.tedu.gym.admin.server.admin.pojo.param.AdminAddNewParam;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminUpdateInfoParam;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserListItemVO;
import cn.tedu.gym.admin.server.admin.service.IAdminService;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@Api(tags = "1.4.管理员模块")
@RequestMapping("admin/")
public class AdminController {

    @Autowired
    IAdminService adminService;

    public AdminController() {
        log.debug("创建控制器类对象:AdminController");
    }

    @GetMapping("/info")
    @ApiOperation("查询管理员信息")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "管理员ID", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult adminInfoById(Long id){
        log.debug("开始处理[查询管理员信息]的请求,管理员id:{}",id);
        UserListItemVO adminInfo = adminService.adminInfoById(id);
        return JsonResult.ok(adminInfo);
    }

    @PostMapping("/update-info")
    @ApiOperation("修改管理员信息")
    @ApiOperationSupport(order = 300)
    public JsonResult updateAdminInfoById(AdminUpdateInfoParam adminUpdateInfoParam){
        log.debug("开始处理[修改管理员信息]的请求,参数:{}",adminUpdateInfoParam);
        adminService.updateAdminInfoById(adminUpdateInfoParam);
        return JsonResult.ok();
    }
    @PostMapping("/update-avatar")
    @ApiOperation("修改管理员头像")
    @ApiOperationSupport(order = 310)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "管理员ID", defaultValue = "1", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "imgUrl", value = "头像路径", paramType = "query", dataType = "String")

    })
    public JsonResult updateAvatar(Long id,String imgUrl){
        log.debug("开始处理[修改管理员信息]的请求,管理员ID:{},头像路径:{}",id,imgUrl);
        adminService.updateAvatar(id,imgUrl);
        return JsonResult.ok();
    }

    @PostMapping("/add-new")
    @PreAuthorize("hasAnyAuthority('/admin/add-new')")
    @ApiOperation("新增管理员")
    @ApiOperationSupport(order = 100)
    public JsonResult addNewAdmin(AdminAddNewParam adminAddNewParam){
        log.debug("开始处理[新增管理员]的请求,参数:{}",adminAddNewParam);
        adminService.addNewAdmin(adminAddNewParam);
        return JsonResult.ok();
    }
    @GetMapping("/admin-list")
    @ApiOperation("查询管理员列表")
    @ApiOperationSupport(order = 410)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult adminDetailList(Integer pageNum){
        log.debug("开始处理[查询管理员列表]的请求,页码:{}",pageNum);
        PageData<UserDetailListVO> pageData = adminService.adminDetailList(pageNum);
        return JsonResult.ok(pageData);
    }

    @PostMapping("/deleteById")
    @ApiOperation("根据ID删除管理员")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "管理员ID", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult deleteById(Long id){
        log.debug("开始处理[根据ID删除管理员]的请求,ID:{}",id);
        adminService.deleteById(id);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/enable")
    @ApiOperation("启用用户")
    @ApiOperationSupport(order = 310)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "long")
    })
    public JsonResult setEnable(@PathVariable Long id) {
        log.debug("开始处理【启用用户】的请求，参数：{}", id);
        adminService.setEnable(id);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/disable")
    @ApiOperation("禁用用户")
    @ApiOperationSupport(order = 311)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "long")
    })
    public JsonResult setDisable(@PathVariable Long id) {
        log.debug("开始处理【禁用用户】的请求，参数：{}", id);
        adminService.setDisable(id);
        return JsonResult.ok();
    }
}
