package cn.tedu.gym.admin.server.admin.service;

import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;

import java.util.List;

public interface IRoleService {

    /**
     * 查询角色列表
     *
     * @return 角色列表
     */
    List<RoleListItemVO> list();
}
