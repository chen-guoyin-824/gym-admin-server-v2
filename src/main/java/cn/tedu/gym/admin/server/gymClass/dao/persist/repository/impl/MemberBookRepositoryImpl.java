package cn.tedu.gym.admin.server.gymClass.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.ClassBookMapper;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.MemberBookMapper;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.MemberBookRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassMember;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class MemberBookRepositoryImpl implements MemberBookRepository {
    @Autowired
    MemberBookMapper memberBookMapper;

    @Override
    public int insert(ClassMember classMember) {
        log.debug("开始执行[插入会员预约课程], 参数: {}", classMember);
        return memberBookMapper.insert(classMember);
    }

    @Override
    public PageData<ClassBookDetailListVO> listBookingDetail(Long bookClassId, Integer pageNum, Integer pageSize) {
        log.debug("开始执行[根据课程ID查询预约详情列表]的数据请求,课程ID:{} ",bookClassId);
        PageHelper.startPage(pageNum,pageSize);
        List<ClassBookDetailListVO> list = memberBookMapper.listBookingDetail(bookClassId);
        PageInfo<ClassBookDetailListVO> pageInfo=new PageInfo<>(list);
        PageData<ClassBookDetailListVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public int cancelById(Long id) {
        log.debug("开始执行[取消预约]的数据访问, ID: {}", id);
        return memberBookMapper.deleteById(id);
    }

    @Override
    public int update(ClassMember classMember) {
        log.debug("开始执行[根据会员预约id修改会员预约信息], 参数: {}", classMember);
        return memberBookMapper.updateById(classMember);
    }
}
