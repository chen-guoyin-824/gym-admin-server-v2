package cn.tedu.gym.admin.server.gymClass.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 课程预约详情VO类
 */
@Data
public class ClassBookDetailListVO implements Serializable {
    /**
     * 数据id
     */
    private Long id;
    /**
     * 会员名字
     */
    private String username;
    /**
     * 会员电话
     */
    private String phone;
    /**
     * 签到状态
     */
    private Integer signInStatus;
    /**
     * 上课时间
     */
    private LocalTime classTime;
    /**
     * 预约时间
     */
    private LocalDateTime bookTime;
    /**
     * 课程名称
     */
    private String className;
    /**
     * 课程预约人数
     */
    private Integer bookNumber;

    /**
     * 课程时长
     */
    private  Integer duration;
    /**
     * 上课日期
     */
    private LocalDate classDate;
}
