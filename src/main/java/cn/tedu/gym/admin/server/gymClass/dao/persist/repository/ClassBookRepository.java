package cn.tedu.gym.admin.server.gymClass.dao.persist.repository;


import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassBook;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;

import java.time.LocalDate;
import java.time.LocalTime;

public interface ClassBookRepository {

    /**
     * 插入预约课程
     * @return 影响课程预约表的行数
     */
    int insertBookClass(ClassBook classBook);

    /**
     * 修改预约课程信息
     * @param classBook 预约课程实体类
     * @return 影响记录数
     */
    int update(ClassBook classBook);

    /**
     * 根据预约ID查询详情
     * @param id 预约id
     * @return 预约详情
     */
    ClassBookStartedVO selectById(Long id);

    /**
     * 预约记录列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 课程预约列表
     */
    PageData<ClassBookListItemVO> listBooking(Integer pageNum, Integer pageSize);

    /**
     * 通过上课日期和时间查询预约课程信息
     * @param date 上课日期
     * @param time 上课时间
     * @return 预约课程信息对象
     */
    ClassBookStartedVO getStandardByDateTime(LocalDate date, LocalTime time);

    /**
     * 根据课程ID和教练ID查询列表
     * @param classId 课程ID
     * @param coachId 教练ID
     * @return 预约课程列表
     */
    PageData<ClassBookListItemVO>listByClassIdANDCoachId(Long classId, Long coachId, Integer pageNum, Integer pageSize);

}
