package cn.tedu.gym.admin.server.user.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class UserLoginInfoParam implements Serializable {
    /**
     * 用户名
     */
    @NotNull(message = "请提交用户名")
    @Pattern(regexp = "^[a-zA-Z0-9\\u4e00-\\u9fa5]{4,15}$",
            message = "用户名必须是4~15长度的字符组成，且不允许使用标点符号")
    @ApiModelProperty(value = "用户名", required = true)
    private String username;

    /**
     * 密码（原文）
     */
    @NotNull(message = "请提交密码")
    @Pattern(regexp = "^.{4,15}$",
            message = "密码必须是4~15长度的字符组成，且不允许使用标点符号")
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    /**
     * 验证码
     */
    @NotNull(message = "请提交验证码")
    @ApiModelProperty(value = "验证码", required = true)
    private String code;

}
