package cn.tedu.gym.admin.server.gymClass.controller;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassUpdateInfoParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.*;
import cn.tedu.gym.admin.server.gymClass.service.ClassService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/gym/class")
@Slf4j
@Validated
@Api(tags = "1.1.课程管理模块")
public class ClassController {
    @Autowired
    ClassService classService;

    @PostMapping("/add-new")
    @ApiOperation("添加课程")
    @ApiOperationSupport(order = 110)
    public JsonResult addNew(@Valid ClassAddNewParam classAddNewParam){
        log.debug("开始处理[添加课程]的业务, 参数: {}", classAddNewParam);
        classService.addNew(classAddNewParam);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/delete")
    @ApiOperation("根据ID删除课程")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "课程ID", required = true, dataType = "long")
    })
    public JsonResult updateInfoById(@PathVariable @Range(min = 1, message = "请提交有效的课程ID值！") Long id) {
        log.debug("开始处理【根据ID删除课程】的请求，ID：{}", id);
        classService.deleteById(id);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/update")
    @ApiOperation("根据id修改课程详情")
    @ApiOperationSupport(order = 300)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "课程ID", required = true, dataType = "long")
    })
    public JsonResult updateInfoById(@PathVariable @Range(min = 1, message = "请提交有效的课程ID值！") Long id,
                                     @Valid ClassUpdateInfoParam classUpdateInfoParam) {
        log.debug("开始处理【根据id修改课程详情】的请求，ID：{}，新数据：{}", id, classUpdateInfoParam);
        classService.updateById(id, classUpdateInfoParam);
        return JsonResult.ok();
    }

    @GetMapping("list")
    @ApiOperation("查询课程列表")
    @ApiOperationSupport(order = 410)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "queryType", value = "查询类型", example = "all"),
            @ApiImplicitParam(name = "type", value = "课程类型", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult list(Integer page, String queryType, Integer type){
        log.debug("开始处理[查询课程列表]的业务, 参数: 页码: {}", page);
        Integer pageNum = page==null ? 1 : page;
        PageData<ClassListItemVO> pageData;
        if ("all".equals(queryType)){
            pageData = classService.list(pageNum, Integer.MAX_VALUE, type);
        }else {
            pageData = classService.list(pageNum, type);
        }
        return JsonResult.ok(pageData);
    }

    @GetMapping("list-by-coach-id")
    @ApiOperation("根据教练id查询课程列表")
    @ApiOperationSupport(order = 414)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coachId", value = "教练id", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult listByCoachId(Long coachId){
        log.debug("开始处理[根据课程id查询课程列表]的业务, 参数: 教练id: {}", coachId);
        List<ClassListItemVO> list = classService.listByCoachId(coachId);
        return JsonResult.ok(list);
    }

    @GetMapping("standard-by-id")
    @ApiOperation("根据id查询课程信息")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "课程id", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult getStandardById(Long id){
        log.debug("开始处理[根据id查询课程详情]的业务, 参数: 页码: {}", id);
        ClassStandardVO standardById = classService.getStandardById(id);
        return JsonResult.ok(standardById);
    }

    @GetMapping("list-class-table")
    @ApiOperation("根据上课日期和时间查询周课表")
    @ApiOperationSupport(order = 430)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "date", value = "上课日期", defaultValue = "2000-01-01", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "time", value = "上课时间", defaultValue = "08:30:00",paramType = "query", dataType = "string")
    })
    public JsonResult listByWeekDate(String date, String time){
        log.debug("开始处理[根据上课日期和时间查询周课表]的业务, 参数: 上课日期: {}, 上课时间: {}", date, time);
        LocalDate localDate = LocalDate.parse(date);
        LocalTime localTime = LocalTime.parse(time);
        List<ClassBookTableListItemVO> classBookTableList = classService.listByWeekTime(localDate, localTime);
        return JsonResult.ok(classBookTableList);
    }

    @GetMapping("class-operate")
    @ApiOperation("查询系统首页团课经营")
    @ApiOperationSupport(order = 440)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "classDate", value = "当前时间(年月日)", defaultValue = "2023-06-01", paramType = "query", dataType = "LocalDate")
    })
    public JsonResult getClassOperate(@RequestParam("classDate") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate classDate,Integer type){
        System.out.println(classDate.getClass().getName());
        log.debug("开始执行[用于系统首页展示经营概况的团课查询], 参数: {}", classDate);
        List<HomeClassOperateVO> list = classService.classOperate(classDate,type);
        for (HomeClassOperateVO operateVO : list) {
            operateVO.setTime(operateVO.getClassData()+"");
        }
        return JsonResult.ok(list);
    }

    @GetMapping("class-money")
    @ApiOperation("查询系统首页课程收入")
    @ApiOperationSupport(order = 450)
    public JsonResult classMoney(){
        log.debug("开始执行[用于系统首页展示课程收入]");
        ClassMoneyCountVO moneyCount = classService.classMoneyCount();
        return JsonResult.ok(moneyCount);
    }

}
