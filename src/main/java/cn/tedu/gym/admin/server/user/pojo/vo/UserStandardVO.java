package cn.tedu.gym.admin.server.user.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserStandardVO implements Serializable {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 性别
     */
    private String gender;
    /**
     * 电话号码
     */
    private String phone;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 是否启用
     */
    private Integer enable;
}
