package cn.tedu.gym.admin.server.gymClass.service.impl;

import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.ClassMapper;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassBookRepository;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.MemberBookRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassBook;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassBookAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassListItemVO;
import cn.tedu.gym.admin.server.gymClass.service.ClassBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Transactional
public class ClassBookServiceImpl implements ClassBookService {
    @Value("${gym.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;

    @Autowired
    ClassBookRepository classBookRepository;

    @Autowired
    ClassMapper classMapper;

    @Autowired
    MemberBookRepository memberBookRepository;

    @Override
    public void insertBookClass(ClassBookAddNewParam classBookAddNewParam) {
        log.debug("开始处理[新增预约课程排期]的业务, 参数: {}", classBookAddNewParam);
        List<ClassListItemVO> list = classMapper.listByCoachId(classBookAddNewParam.getCoachId());
        List<Long> classList = new ArrayList<>();
        for (ClassListItemVO classListItemVO : list) {
            classList.add(classListItemVO.getId());
        }
        if (!classList.contains(classBookAddNewParam.getClassId())){
            String message = "添加排期有误, 该教练没有教授该课程";
            log.error(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        ClassBookStartedVO standardByDateTime = classBookRepository.getStandardByDateTime(classBookAddNewParam.getClassDate(), classBookAddNewParam.getClassTime());
        if (standardByDateTime!=null){
            String message = "添加排期失败, 该时间段已经有课程了";
            log.error(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }
        ClassBook classBook = new ClassBook();
        BeanUtils.copyProperties(classBookAddNewParam, classBook);
        classBook.setBookNumber(0);
        int rows = classBookRepository.insertBookClass(classBook);
        if (rows != 1) {
            String message = "添加排期失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }

    }

    @Override
    public PageData<ClassBookListItemVO> listBooking(Integer pageNum) {
        log.debug("开始处理[查询预约记录列表]的业务, 参数: 页码: {}", pageNum);
        PageData<ClassBookListItemVO> pageData = classBookRepository.listBooking(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public PageData<ClassBookListItemVO> listBooking(Integer pageNum, Integer pageSize) {
        log.debug("开始处理[查询预约记录列表]的业务, 参数: 页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageData<ClassBookListItemVO> pageData = classBookRepository.listBooking(pageNum, pageSize);
        return pageData;
    }

    @Override
    public PageData<ClassBookListItemVO> listByClassIdANDCoachId(Long classId, Long coachId, Integer pageNum, Integer pageSize) {
        log.debug("开始执行[根据课程ID和教练ID查询列表]的业务,课程ID:{},教练ID:{} ",classId,coachId);
        //TODO 查询课程是否存在,教练是否存在
        PageData<ClassBookListItemVO> pageData = classBookRepository.listByClassIdANDCoachId(classId,coachId,pageNum,pageSize);
        return pageData;
    }

    @Override
    public PageData<ClassBookListItemVO> listByClassIdANDCoachId(Long classId, Long coachId, Integer pageNum) {
        log.debug("开始执行[根据课程ID和教练ID查询列表]的业务,课程ID:{},教练ID:{},页码:{}",classId,coachId,pageNum);
        //TODO 查询课程是否存在,教练是否存在
        PageData<ClassBookListItemVO> pageData = classBookRepository.listByClassIdANDCoachId(classId,coachId,pageNum,defaultQueryPageSize);
        return pageData;
    }

    @Override
    public ClassBookStartedVO getBookStandardById(Long id) {
        log.debug("开始执行[根据预约id查询预约详情]的业务, 参数: {}", id);
        //TODO 根据id查询数据库数据是否存在
        return classBookRepository.selectById(id);
    }
}
