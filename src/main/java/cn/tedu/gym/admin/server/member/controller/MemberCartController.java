package cn.tedu.gym.admin.server.member.controller;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMemberCart;
import cn.tedu.gym.admin.server.member.pojo.param.GymMemberCartAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.vo.CartStandard1VO;
import cn.tedu.gym.admin.server.member.pojo.vo.CartStandardVO;
import cn.tedu.gym.admin.server.member.service.MemberCartService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/member")
@Slf4j
@Validated
@Api(tags = "1.3.会员卡管理模块")
public class MemberCartController {
    @Autowired
    MemberCartService memberCartService;

    @PostMapping("/cart-insert")
    @ApiOperation("添加会员卡")
    @ApiOperationSupport(order = 110)
    public JsonResult addNew(@Valid GymMemberCartAddNewParam gymMemberCartAddNewParam){
        log.debug("开始处理[添加会员]的业务, 参数: {}", gymMemberCartAddNewParam);
        memberCartService.insert(gymMemberCartAddNewParam);
        return JsonResult.ok();
    }

    @PostMapping("/cart-update-status")
    @ApiOperation("根据ID修改会员卡状态")
    @ApiOperationSupport(order = 115)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会员卡id", defaultValue = "1", paramType = "query", dataType = "Long")
    })
    public JsonResult updateStatus(@Valid Long id ){

        log.debug("开始处理[修改会员卡状态]的业务, 参数: {}", id);
        memberCartService.updateStatusById(id);
        return JsonResult.ok();
    }

    @PostMapping("/cart-update-down-count")
    @ApiOperation("根据ID删减会员卡次数")
    @ApiOperationSupport(order = 120)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会员卡id", defaultValue = "1", paramType = "query", dataType = "Long")
    })
    public JsonResult updateDownCount(@Valid Long id ){
        log.debug("开始处理[修改会员卡次数]的业务, 参数: {}", id);
        memberCartService.updateCountById(id);
        return JsonResult.ok();
    }
//   @ApiImplicitParam(name = "manuallyInput", value = "添加方式", defaultValue = "true", paramType = "query", dataType = "boolean")
    @PostMapping("/cart-update-up-count")
    @ApiOperation("根据ID增加会员卡次数")
    @ApiOperationSupport(order = 125)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会员卡id", defaultValue = "1", paramType = "query", dataType = "Long"),
    })
    public JsonResult updateUpCount(@Valid Long id){
        log.debug("开始处理[修改会员卡次数]的业务, 参数: {}", id);
        memberCartService.addTimesById(id);
        return JsonResult.ok();
    }

    @GetMapping("cart-list")
    @ApiOperation("查询会员卡列表")
    @ApiOperationSupport(order = 415)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "queryType", value = "查询类型", example = "all")
    })
    public JsonResult list(Integer page, String queryType){
        log.debug("开始处理[查询会员列表]的业务, 参数: 页码: {}", page);
        Integer pageNum = page==null ? 1 : page;
        PageData<GymMemberCart> pageData;
        if ("all".equals(queryType)){
            pageData = memberCartService.list(pageNum, Integer.MAX_VALUE);
        }else {
            pageData = memberCartService.list(pageNum);
        }
        return JsonResult.ok(pageData);
    }

    @GetMapping("member-cart-by-id")
    @ApiOperation("根据id查询会员卡信息")
    @ApiOperationSupport(order = 425)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会员卡id", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult getStandardById(Long id){
        log.debug("开始处理[根据id查询会员ka详情]的业务, 参数: 页码: {}", id);
        GymMemberCart standardById = memberCartService.getStandardById(id);
        return JsonResult.ok(standardById);
    }

    @GetMapping("member-info-by-id")
    @ApiOperation("根据id查询会员卡详情信息")
    @ApiOperationSupport(order = 450)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会员卡id", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult getInfoById(Long id){
        log.debug("开始处理[根据id查询会员ka详情]的业务, 参数: 页码: {}", id);
        List<CartStandardVO> list = memberCartService.getListById(id);
        return JsonResult.ok(list);
    }


}
