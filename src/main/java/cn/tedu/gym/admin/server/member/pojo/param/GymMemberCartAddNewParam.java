package cn.tedu.gym.admin.server.member.pojo.param;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 查询会员的VO类
 */
@Data
public class GymMemberCartAddNewParam {
    private Long memberId;          // '会员id',
    private String name;    //会员卡等级名称
    private Integer memberLevel; //会员卡等级
    private Integer count; //剩余次数
    private Integer status; //会员卡状态
}
