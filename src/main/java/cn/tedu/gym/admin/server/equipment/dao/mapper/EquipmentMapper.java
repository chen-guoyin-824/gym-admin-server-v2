package cn.tedu.gym.admin.server.equipment.dao.mapper;

import cn.tedu.gym.admin.server.equipment.pojo.entity.Equipment;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentStandardVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentTypeListVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * 处理器材数据的Mapper接口
 *
 */
@Repository
public interface EquipmentMapper extends BaseMapper<Equipment>{
    /**
     * 查询器材列表
     * @return
     */
    List<EquipmentListItemVO> list();

    /**
     * 根据ID查询器材信息
     * @param id 器材ID
     * @return
     */
    EquipmentStandardVO getStandardVOById(Long id);

    /**
     * 查询器材类别列表
     * @return
     */
    List<EquipmentTypeListVO> typeList();


}
