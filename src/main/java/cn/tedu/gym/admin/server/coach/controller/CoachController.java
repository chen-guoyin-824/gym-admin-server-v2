package cn.tedu.gym.admin.server.coach.controller;

import cn.tedu.gym.admin.server.coach.pojo.param.CoachAddNewParam;
import cn.tedu.gym.admin.server.coach.pojo.param.CoachUpdateInfoParam;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachDetailVO;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachListItemVO;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachStandardVO;
import cn.tedu.gym.admin.server.coach.service.CoachService;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/coach")
@Slf4j
@Validated
@Api(tags = "4.1.教练管理模块")
public class CoachController {
    @Autowired
    CoachService coachService;


    @PostMapping("/add-coach")
    @ApiOperation("添加教练")
    @ApiOperationSupport(order = 800)
    public JsonResult addNew(@Valid CoachAddNewParam coachAddNewParam){
        log.debug("开始处理[添加教练]的业务, 参数: {}", coachAddNewParam);
        coachService.addNew(coachAddNewParam);
        return JsonResult.ok();
    }

    @GetMapping("list")
    @ApiOperation("查询教练列表")
    @ApiOperationSupport(order = 810)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "queryType", value = "查询类型", example = "all")
    })
    public JsonResult list(Integer page, String queryType){
        log.debug("开始处理[查询教练列表]的业务, 参数: 页码: {}", page);
        Integer pageNum = page==null ? 1 : page;
        PageData<CoachListItemVO> pageData;
        if ("all".equals(queryType)){
            pageData = coachService.list(pageNum, Integer.MAX_VALUE);
        }else {
            pageData = coachService.list(pageNum);
        }
        return JsonResult.ok(pageData);
    }

    @GetMapping("/{id:[0-9]+}")
    @ApiOperation("根据id查询教练信息")
    @ApiOperationSupport(order = 820)
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "用户id", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult getStandardById(@PathVariable Long id){
        log.debug("开始处理[根据id查询教练详情]的业务, 参数: id: {}", id);
        CoachStandardVO standardById = coachService.getStandardById(id);
        CoachDetailVO detail = new CoachDetailVO();
        BeanUtils.copyProperties(standardById, detail);
        if (standardById.getPhoto() != null) {
            List photoList = Arrays.asList(standardById.getPhoto().split(","));
            detail.setPhotoList(photoList);
        }
        log.debug("detail教练详情, 参数: id: {}", detail);
        return JsonResult.ok(detail);
    }

    @PostMapping("/{id:[0-9]+}/delete")
    @ApiOperation("根据id删除教练")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "教练id", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult deleteById(@PathVariable Long id){
        log.debug("开始处理[根据id删除教练]的业务, 参数: id: {}", id);
        coachService.deleteById(id);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/update/info")
    @ApiOperation("修改教练详情")
    @ApiOperationSupport(order = 300)
    public JsonResult updateInfoById(@Validated CoachUpdateInfoParam coachUpdateInfoParam) {
        log.debug("开始处理【修改标签】的请求，参数：{}", coachUpdateInfoParam);
        coachService.updateInfoById(coachUpdateInfoParam);
        return JsonResult.ok();
    }

}