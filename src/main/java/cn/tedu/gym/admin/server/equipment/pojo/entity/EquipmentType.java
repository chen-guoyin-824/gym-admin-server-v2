package cn.tedu.gym.admin.server.equipment.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("gym_equipment_type")
public class EquipmentType implements Serializable {
    /**
     * 器材类别id
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 器材类别名称
     */
    private String typeName;
    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;

    /**
     * 数据最后修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;
}
