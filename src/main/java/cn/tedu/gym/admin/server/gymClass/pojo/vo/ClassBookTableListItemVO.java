package cn.tedu.gym.admin.server.gymClass.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

@Data
public class ClassBookTableListItemVO implements Serializable {
    /**
     * 数据id
     */
    private Long id;
    /**
     * 课程名称
     */
    private String className;
    /**
     * 课程教练
     */
    private String coachName;
    /**
     * 课程预约人数
     */
    private Integer bookNumber;
    /**
     * 课程时长
     */
    private Integer duration;
    /**
     * 上课开始时间
     */
    private LocalTime classTime;
    /**
     * 上课日期
     */
    private LocalDate classDate;
}
