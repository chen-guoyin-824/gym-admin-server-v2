package cn.tedu.gym.admin.server.gymClass.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用于系统首页展示课程经营概况
 */
@Data
public class ClassMoneyCountVO implements Serializable {
    /**
     * 今日课程收入
     */
    private BigDecimal todayMoney;
    /**
     * 昨日课程收入
     */
    private BigDecimal yesterdayMoney;

}
