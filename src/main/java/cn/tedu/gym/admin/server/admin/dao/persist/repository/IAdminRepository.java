package cn.tedu.gym.admin.server.admin.dao.persist.repository;

import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminAddNewParam;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserListItemVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;

import java.util.List;

public interface IAdminRepository {
    /**
     * 根据id查询管理员信息
     * @param id 管理员id
     * @return
     */
    UserListItemVO adminInfoById(Long id);

    /**
     * 查询管理员信息详情
     * @param id 管理员id
     * @return
     */
    UserStandardVO adminDetailById(Long id);

    /**
     * 修改管理员信息
     * @param user 封装了被修改数据的ID和新数据的对象
     * @return
     */
    int updateAdminInfoById(User user);

    /**
     * 添加管理员
     * @param user 封装了新增管理员信息的对象
     * @return 受影响的行数
     */
    int addNewAdmin(User user);

    /**
     * 根据用户名统计查询管理员
     * @param username 用户名
     * @return
     */
    int selectCount(String username);
    /**
     * 根据手机号统计查询管理员
     * @param phone 手机号
     * @return
     */
    int selectCountByPhone(String phone);

    /**
     * 查询管理员列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return
     */
    PageData<UserDetailListVO> adminDetailList(Integer pageNum, Integer pageSize);

    /**
     * 根据ID删除管理员
     * @param id 管理员ID
     * @return
     */
    int deleteById(Long id);

    /**
     * 根据用户id修改用户的数据
     *
     * @param user 封装了用户id和新的数据的对象
     * @return 受影响的行数
     */
    int updateById(User user);
}
