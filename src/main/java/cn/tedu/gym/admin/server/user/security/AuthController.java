package cn.tedu.gym.admin.server.user.security;

import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.user.dao.cache.UserCacheRepository;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Api(tags ="6.1.验证码")
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Resource
    private DefaultKaptcha captchaProducer;
    @Autowired
    private UserCacheRepository userCacheRepository;

    /*
     *获取验证码端口
     * */
    @ApiOperation("获取并保存验证码")
    @ApiOperationSupport(order = 10)
    @GetMapping("/captcha")
    public JsonResult getKaptcha() throws Exception {
        log.info("开始生成验证码");
        //生成验证码
        String code = captchaProducer.createText();
        //生成图片
        BufferedImage image = captchaProducer.createImage(code);
        // 将验证码转换为Base64编码
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ImageIO.write(image, "png", stream);
        //读出转换为Base64编码的值
        String base64Code = Base64.getEncoder().encodeToString(stream.toByteArray());
        stream.close();
        Map<String, String> kaptchaVoMap = new HashMap<>();
        //生成一个随机的key，到时候前端验证时候带这个key过来查询验证码是否正确
        String uuid = UUID.randomUUID().toString();
        kaptchaVoMap.put("uuid", uuid);
        kaptchaVoMap.put("code", "data:image/png;base64," + base64Code);
        //将验证码放入到redis中去
        userCacheRepository.saveAuthByUuid(uuid, code);
        return JsonResult.ok(kaptchaVoMap);
    }
}
