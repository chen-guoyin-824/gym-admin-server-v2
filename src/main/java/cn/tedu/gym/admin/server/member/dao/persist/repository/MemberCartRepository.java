package cn.tedu.gym.admin.server.member.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMemberCart;
import cn.tedu.gym.admin.server.member.pojo.vo.CartStandardVO;

import java.util.List;


public interface MemberCartRepository {
    /**
     * 修改消费次数
     */
    int updateById(GymMemberCart gymMemberCart);
    int deleteById(Long id);
    int insert(GymMemberCart gymMemberCart);
    GymMemberCart getStandardById(Long id);
    PageData<GymMemberCart> list(Integer pageNum, Integer pageSize);
    PageData<GymMemberCart> list(Integer pageNum);
    List<CartStandardVO> getListById(Long id);
}
