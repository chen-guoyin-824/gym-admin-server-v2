package cn.tedu.gym.admin.server.admin.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
@Data
public class AdminAddNewParam implements Serializable {
    /**
     * 管理员用户名
     */
    @NotNull(message = "请提交用户名")
    @Pattern(regexp = "^[a-zA-Z\\u4e00-\\u9fa5]{2,10}$",
            message = "标签名称必须是2~10长度的字符组成，且不允许使用标点符号")
    @ApiModelProperty(value = "用户昵称", required = true)
    private String username;
    /**
     * 密码
     */
    @NotNull(message = "请提交密码")
    @Pattern(regexp = "^[a-zA-Z\\u4e00-\\u9fa5]{2,10}$",
            message = "标签名称必须是2~10长度的字符组成，且不允许使用标点符号")
    @ApiModelProperty(value = "账号密码", required = true)
    private String password;
    /**
     * 管理员姓名
     */
    @NotNull(message = "请提交管理员姓名")
    @Pattern(regexp = "^[a-zA-Z\\u4e00-\\u9fa5]{2,5}$",
            message = "标签名称必须是2~5长度的字符组成，且不允许使用标点符号")
    @ApiModelProperty(value = "管理员姓名", required = true)
    private String name;
    /**
     * 性别0代表保密,1代表男,2代表女
     */
    @NotNull(message = "请选择性别")
    private Integer gender;
    /**
     * 手机号
     */
    @NotNull(message = "请提交手机号码")
    @Pattern(regexp = "^1[3456789]\\d{9}$",
            message = "")
    @ApiModelProperty(value = "手机号码", required = true)
    private String phone;
    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 头像
     */
    private String avatar;



}
