package cn.tedu.gym.admin.server.member.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import cn.tedu.gym.admin.server.member.dao.persist.mapper.MemberCartMapper;
import cn.tedu.gym.admin.server.member.dao.persist.repository.MemberCartRepository;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMemberCart;
import cn.tedu.gym.admin.server.member.pojo.vo.CartStandardVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class MemberCartRepositoryImpl implements MemberCartRepository {
    @Autowired
    private MemberCartMapper memberCartMapper;

    @Override
    public int updateById(GymMemberCart gymMemberCart) {
        log.debug("开始执行【根据ID修改标签数据】，参数：{}",gymMemberCart.getId());
        return memberCartMapper.updateById(gymMemberCart);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除标签数据】，参数：{}", id);
        return memberCartMapper.deleteById(id);
    }

    @Override
    public int insert(GymMemberCart gymMemberCart) {
        log.debug("开始执行【新增会员卡】的数据访问，参数：{}", gymMemberCart);
        return memberCartMapper.insert(gymMemberCart);
    }

    @Override
    public GymMemberCart getStandardById(Long id) {
        log.debug("开始执行【根据ID查询会员卡信息】的数据访问，参数：{}", id);
        return memberCartMapper.getStandardById(id);
    }

    @Override
    public PageData<GymMemberCart> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行[查询会员列表]的数据访问, 参数: 页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<GymMemberCart> list = memberCartMapper.list();
        System.out.println("----------------------------------------------------");
        list.forEach(System.out::println);
        PageInfo<GymMemberCart> pageInfo = new PageInfo<>(list);
        PageData<GymMemberCart> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public PageData<GymMemberCart> list(Integer pageNum) {
        log.debug("开始执行[查询会员列表]的数据访问, 参数: 页码: {}, 每页记录数: {}", pageNum);
        PageHelper.startPage(pageNum);
        List<GymMemberCart> list = memberCartMapper.list();
        System.out.println("----------------------------------------------------");
        list.forEach(System.out::println);
        PageInfo<GymMemberCart> pageInfo = new PageInfo<>(list);
        PageData<GymMemberCart> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public List<CartStandardVO> getListById(Long id) {
        log.debug("开始执行【根据ID查询会员相关信息】的数据访问，参数：{}", id);
        return memberCartMapper.getInfoById(id);
    }
}
