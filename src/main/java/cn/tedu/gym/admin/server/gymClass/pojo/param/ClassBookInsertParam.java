package cn.tedu.gym.admin.server.gymClass.pojo.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClassBookInsertParam implements Serializable {
    /**
     * 课程id
     */
    private Long classId;
    /**
     * 教练id
     */
    private Long coachId;
    /**
     * 课程上课日期
     */
    private String classDate;
    /**
     * 课程上课时间
     */
    private String classTime;

}
