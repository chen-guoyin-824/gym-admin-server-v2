package cn.tedu.gym.admin.server.core.config;

import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.core.filter.JwtAuthorizationFilter;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;

import java.io.PrintWriter;

/**
 * Spring Security的配置类
 */
@Slf4j
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)//开启基于方法的安全检查
//@EnableWebSecurity(debug = true)//开启调试模式, 在控制台将显示很多日志, 在生产环境不宜开启
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtAuthorizationFilter jwtAuthorizationFilter;
    @Bean
    public PasswordEncoder passwordEncoder() {
//         return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //配置 Spring Security 中的会话管理策略, 使其不适用于Session
        //SessionCreationPolicy.NEVER: 从不主动创建Session, 但是, Session存在的话, 会自动使用
        //SessionCreationPolicy.STATELESS: 无状态, 无论是否存在Session, 都不使用
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //过滤器一定是最早接收到请求的组件
        //必须添加在检查SecurityContext的Authentication之前, 具体位置不做严格要求
        //将自定义解析JWT的过滤器添加到Security框架的过滤器链中
        http.addFilterBefore(jwtAuthorizationFilter, LogoutFilter.class);
        //允许跨域访问, 本质上是启用了Security框架自带的CorsFilter
        //如果不启用corsFilter, 也可以改为对所有OPTIONS请求直接许可, 一样可以解决复杂请求预检的跨域攻击
        //注意: 即使此处
        http.cors();
        //处理无认证信息却访问需要认证的资源时的响应
        http.exceptionHandling().authenticationEntryPoint((request, response, authException) -> {
            //设置响应的
            response.setContentType("application/json;charset=utf-8");
            String message = "操作失败, 您当前未登录";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_UNAUTHORIZED, message);
            PrintWriter writer = response.getWriter();
            writer.println(JSON.toJSONString(jsonResult));
            writer.close();
        });

        //白名单
        String[] urls = {
                "/favicon.ico",
                "/doc.html",
                "/**/*.js",
                "/**/*.css",
                "/swagger-resources",
                "/v2/api-docs",
                "/resources/**",
                "/gym/users/login",
                "/auth/captcha",//生成验证码
        };
        //禁用"防止伪造的跨域攻击的防御机制"
        http.csrf().disable();
        //配置请求授权
        //如果某个请求被多次配置, 按照"第一配置原则"处理
        //应该将精确的配置写在前面, 将模糊的配置写在后面
        http.authorizeRequests()
                //允许所有的请求访问, 相当于解除了跨域攻击 OPTIONS复杂请求预检
//                .mvcMatchers(HttpMethod.OPTIONS, "/**")
//                .permitAll()
                .mvcMatchers(urls)//配置某些请求
                .permitAll()//许可, 即不需要通过认证就可以访问
                .anyRequest()//任何请求, 从执行效果来看, 也可以视为: 除了以上配置过的以外的其他请求
                .authenticated();//需要通过认证才可以访问

        // 是否调用以下方法，将决定是否启用默认的登录页面
        // 当未通过认证时，如果有登录页，则自动跳转到登录，如果没有登录页，则直接响应403
        // http.formLogin();

        // super.configure(http); // 不要调用父类的同名方法，许多默认的效果都是父类方法配置的
    }
}
