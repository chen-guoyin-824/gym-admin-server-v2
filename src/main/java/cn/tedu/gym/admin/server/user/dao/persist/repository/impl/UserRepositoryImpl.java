package cn.tedu.gym.admin.server.user.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.coach.pojo.entity.Coach;
import cn.tedu.gym.admin.server.user.dao.persist.mapper.UserMapper;
import cn.tedu.gym.admin.server.user.dao.persist.repository.UserRepository;
import cn.tedu.gym.admin.server.user.pojo.entity.User;
import cn.tedu.gym.admin.server.user.pojo.vo.UserLoginInfoVO;
import cn.tedu.gym.admin.server.user.pojo.vo.UserStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class UserRepositoryImpl implements UserRepository {
    @Autowired
    UserMapper userMapper;

    @Override
    public int insert(User user) {
        log.debug("开始执行插入用户信息，参数:{}",user);
        int count = userMapper.insert(user);
        return count;
    }

    @Override
    public UserStandardVO getStandardByUsername(String username) {
        log.debug("开始处理[根据用户名查询用户信息]的功能");
        UserStandardVO standardUser = userMapper.getStandardByUsername(username);
        return standardUser;
    }

    @Override
    public UserLoginInfoVO getLoginInfoByUsername(String username) {
        log.debug("开始执行【根据用户名查询用户登录信息】的数据访问，参数：{}", username);
        return userMapper.getLoginInfoByUsername(username);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行删除用户信息，参数:{}",id);
        return userMapper.deleteById(id);
    }

    @Override
    public long getIdByPhone(String phone) {
        log.debug("开始执行通过phone查询用户id，参数:{}",phone);
        return userMapper.getIdByPhone(phone);
    }

    @Override
    public int updateById(User user) {
        log.debug("开始执行【根据ID修改用户数据】，参数：{}", user);
        return userMapper.updateById(user);
    }
}
