package cn.tedu.gym.admin.server.gymClass.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassBook;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.GymClass;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface ClassRepository {
    /**
     * 插入课程
     * @param gymClass 课程实体类
     * @return 影响课程表的行数
     */
    int insert(GymClass gymClass);

    /**
     * 根据id删除课程
     * @param id 课程id
     * @return 影响课程表的行数
     */
    int deleteById(Long id);

    /**
     * 根据课程id修改课程信息
     * @param gymClass 课程实体类
     * @return 影响课程表的行数
     */
    int updateById(GymClass gymClass);

    /**
     * 查询课程列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 课程列表
     */
    PageData<ClassListItemVO> list(Integer pageNum, Integer pageSize, Integer type);

    /**
     * 根据教练id查询课程列表
     * @param coachId 教练id
     * @return 课程列表
     */
    List<ClassListItemVO> listByCoachId(Long coachId);

    /**
     * 根据id查询课程详情
     * @param id 课程id
     * @return 封装课程详情的对象
     */
    ClassStandardVO getStandardById(Long id);

    /**
     * 通过一周的日期时间查询课程预约列表
     * @param dateList 当日所在星期的日期时间列表
     * @return 课程预约列表
     */
    List<ClassBookTableListItemVO> listByWeekTime(List<LocalDate> dateList, LocalTime dateTime);

    /**
     * 用于系统首页展示经营概况
     * @param classDate 系统当前时间 年月日
     * @return
     */
    List<HomeClassOperateVO> classOperate(LocalDate classDate,Integer type);

    /**
     * 用于系统首页展示课程收入
     * @return
     */
    ClassMoneyCountVO classMoneyCount();
}
