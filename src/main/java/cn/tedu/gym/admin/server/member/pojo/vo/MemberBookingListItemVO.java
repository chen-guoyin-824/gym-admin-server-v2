package cn.tedu.gym.admin.server.member.pojo.vo;

import lombok.Data;

@Data
public class MemberBookingListItemVO {
    private Long  id;          // '会员详情id',
    private Long userId;      // '用户id',
    private String birthday;     // '生日',
    private String idcard;       // '身份证号',
    private String city;         // '城市',
    private String career;       // '职业',
    private Integer height;       // '身高',
    private Integer weight;       // '体重',
    private String signature;    // '个性签名',
    private String exp;          // '健身经验',
    private String target; //'健身目的',
}
