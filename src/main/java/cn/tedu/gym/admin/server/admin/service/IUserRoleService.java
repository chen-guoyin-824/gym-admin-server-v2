package cn.tedu.gym.admin.server.admin.service;

import cn.tedu.gym.admin.server.admin.pojo.entity.UserRole;

public interface IUserRoleService {
    /**
     * 新增用户角色管理数据 用于新增管理员
     * @param userRole
     * @return
     */
    void insert(UserRole userRole);
}
