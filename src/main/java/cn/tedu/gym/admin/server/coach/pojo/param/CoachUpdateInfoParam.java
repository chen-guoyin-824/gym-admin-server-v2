package cn.tedu.gym.admin.server.coach.pojo.param;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CoachUpdateInfoParam implements Serializable {
    //id
    private long id;
    //userId
    private long userId;
    //排序
    private long sort;
    //手机
    private String phone;
    //签名
    private String target;
    //简介
    private String profile;
    /**
     * 从业时间
     */
    private String worktime;
    /**
     * 擅长标签
     */
    private String skills;
    /**
     * 每日私教课上限
     */
    private Integer upperLimit;
    //相册
    private List photoList;
}
