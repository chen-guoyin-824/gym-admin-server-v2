package cn.tedu.gym.admin.server.member.service.impl;

import cn.tedu.gym.admin.server.coach.dao.persist.repository.CoachRepository;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachStandardVO;
import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.member.dao.persist.repository.MemberCartRepository;
import cn.tedu.gym.admin.server.member.dao.persist.repository.MemberRepository;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMember;
import cn.tedu.gym.admin.server.member.pojo.param.MemberAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.vo.*;
import cn.tedu.gym.admin.server.member.service.MemberService;
import cn.tedu.gym.admin.server.user.dao.persist.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MemberServiceImpl implements MemberService {
    @Value("${gym.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    MemberCartRepository memberCartRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CoachRepository coachRepository;

    @Override
    public void addNew(MemberAddNewParam memberAddNewParam) {
        log.debug("开始处理[添加会员]的业务, 参数: {}", memberAddNewParam);
        System.out.println("service层 YS666" + memberAddNewParam);
        GymMember gymMember = new GymMember();
        BeanUtils.copyProperties(memberAddNewParam, gymMember);
        gymMember.setUserId(userRepository.getLoginInfoByUsername(memberAddNewParam.getUsername()).getId());
        gymMember.setCoachId(memberAddNewParam.getCoachId());
        memberRepository.insert(gymMember);

//        log.debug("开始处理[添加会员]的业务, 参数: {}", memberAddNewParam);
//        //TODO 检查课程名称是否重复
//        GymMember gymMember = new GymMember();
//        BeanUtils.copyProperties(memberAddNewParam, gymMember);
//        String coachName = memberAddNewParam.getCoachName();
//        UserStandardVO standardByUsername = userRepository.getStandardByUsername(coachName);
//        Long coachId = standardByUsername.getId();
//        gymMember.setUserId(coachId);
//        memberRepository.insert(gymMember);
//        User user = new User();
//        userRepository.insert(user);
//        GymMember gymMember = new GymMember();
//        BeanUtils.copyProperties(memberAddNewParam, gymMember);
//        String coachName = memberAddNewParam.getCoachName();
//        UserStandardVO standardByUsername = userRepository.getStandardByUsername(coachName);
//        Long coachId = standardByUsername.getId();
//        gymMember.setCoachID(coachId);
//        gymMember.setUserId(user.getId());
//        memberRepository.insert(gymMember);
//        GymMemberCart gymMemberCart = new GymMemberCart();
//        memberCartRepository.insert(gymMemberCart);
//        gymMemberCart.setMemberId(gymMember.getId());

    }

    @Override
    public PageData<MemberListItemVO> list1(Integer pageNum) {
        log.debug("开始处理[查询会员列表]的业务, 参数: ✌✌页码: {}", pageNum);
        PageData<MemberListItemVO> pageData = memberRepository.list1(pageNum, defaultQueryPageSize);
        List<MemberListItemVO> list = pageData.getList();
        List<MemberListItemVO> list1 = new ArrayList<>();
        for (MemberListItemVO memberListItemVO : list) {
            Long coachId = memberListItemVO.getCoachId();
            CoachStandardVO standardById = coachRepository.getStandardById(coachId);
            String name = standardById.getName();
            System.out.println("zbxbx钟毕鑫钟毕鑫："+name);
            memberListItemVO.setCoachName(name);
            list1.add(memberListItemVO);
        }
        pageData.setList(list1);
        return pageData;
    }


    @Override
    public PageData<MemberListItemVO> list1(Integer pageNum, Integer pageSize) {
        log.debug("开始处理[查询会员列表]的业务, 参数: ✌✌页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageData<MemberListItemVO> pageData = memberRepository.list1(pageNum, pageSize);
        List<MemberListItemVO> list = pageData.getList();
        List<MemberListItemVO> list1 = new ArrayList<>();
        for (MemberListItemVO memberListItemVO : list) {
            Long coachId = memberListItemVO.getCoachId();
            CoachStandardVO standardById = coachRepository.getStandardById(coachId);
            String name = standardById.getName();
            System.out.println("zbxbx钟毕鑫钟毕鑫："+name);
            memberListItemVO.setCoachName(name);
            list1.add(memberListItemVO);
        }
        pageData.setList(list1);
        return pageData;
    }

    @Override
    public MemberStandardVO getStandardById(Long id) {
        log.debug("开始处理[根据id查询会员详情]的业务, 参数: {}", id);
        MemberStandardVO standardById = memberRepository.getStandardById(id);
        if (standardById == null){
            String message = "查询详情失败, 查询id不存在";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return standardById;
    }

    @Override
    public MemberListItemVO getListById(Long id) {
        log.debug("开始处理[根据会员卡id查询会员详情]的业务, 参数: {}", id);
        MemberListItemVO standardById = memberRepository.getListByCardId(id);
        if (standardById == null){
            String message = "查询详情失败, 查询id不存在";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return standardById;
    }

//    @Override
//    public MemberListItemDTO getListById(Long id) {
//        log.debug("开始处理[根据id查询会员详情]的业务, 参数: {}", id);
//        MemberListItemDTO ListById = memberRepository.getListById(id);
//        if (ListById == null){
//            String message = "查询详情失败, 查询id不存在";
//            log.warn(message);
//            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
//        }
//        return ListById;
//    }

    @Override
    public PageData<MemberBookingListItemVO> listBooking(Integer pageNum) {
        log.debug("开始处理[查询预约记录列表]的业务, 参数: 页码: {}", pageNum);
        PageData<MemberBookingListItemVO> pageData = memberRepository.listBooking(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public PageData<MemberBookingListItemVO> listBooking(Integer pageNum, Integer pageSize) {
        log.debug("开始处理[查询预约记录列表]的业务, 参数: 页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageData<MemberBookingListItemVO> pageData = memberRepository.listBooking(pageNum, pageSize);
        return pageData;
    }

    @Override
    public void deleteById(Long id) {
        log.debug("开始处理【根据ID删除会员】的业务，参数：{}", id);
        MemberStandardVO queryResult = memberRepository.getStandardById(id);
        if (queryResult == null) {
            // 是：数据不存在，抛出异常
            String message = "删除会员失败，尝试删除的文章数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        int rows = memberRepository.deleteById(id);
        if (rows != 1) {
            String message = "删除会员失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_DELETE, message);
        }
        Long userId = queryResult.getUserId();
        userRepository.deleteById(userId);
        Long memberId = queryResult.getId();
        memberCartRepository.deleteById(memberId);
    }

    @Override
    public List<MemberNewListVO> memberNew() {
        log.debug("开始处理【用于首页展示最新办卡】的业务，");
        return memberRepository.memberNew();
    }

    @Override
    public HomeMemberVO homeMember() {
        log.debug("开始处理【用于首页展示会员相关】的业务，");
        return memberRepository.homeMember();
    }


}
