package cn.tedu.gym.admin.server.user.dao.persist.mapper;

import cn.tedu.gym.admin.server.user.pojo.entity.User;
import cn.tedu.gym.admin.server.user.pojo.vo.UserLoginInfoVO;
import cn.tedu.gym.admin.server.user.pojo.vo.UserStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return 封装的用户信息对象
     */
    UserStandardVO getStandardByUsername(String username);

    /**
     * 根据用户名查询用户的登录信息
     *
     * @param username 用户名
     * @return 匹配的用户的登录信息，如果没有匹配的数据，则返回null
     */
    UserLoginInfoVO getLoginInfoByUsername(String username);

    long getIdByPhone(String phone);


}
