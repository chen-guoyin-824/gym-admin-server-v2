package cn.tedu.gym.admin.server.gymClass.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.ClassBookMapper;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.MemberBookMapper;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassBookRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassBook;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Slf4j
@Repository
public class ClassBookRepositoryImpl implements ClassBookRepository {
    @Autowired
    ClassBookMapper bookingMapper;
    @Autowired
    MemberBookMapper memberBookMapper;

    @Override
    public int insertBookClass(ClassBook classBook) {
        log.debug("开始执行[向预约课程表插入数据], 参数: {}", classBook);
        return bookingMapper.insert(classBook);
    }

    @Override
    public int update(ClassBook classBook) {
        log.debug("开始执行[修改预约课程信息], 参数: {}", classBook);
        return bookingMapper.updateById(classBook);
    }

    @Override
    public ClassBookStartedVO selectById(Long id) {
        log.debug("开始执行[根据预约ID查询详情]的数据访问, ID: {}", id);
        return bookingMapper.selectById(id);
    }

    @Override
    public PageData<ClassBookListItemVO> listBooking(Integer pageNum, Integer pageSize) {
        log.debug("开始执行[查询课程预约记录列表]的数据请求, 页码:{},每页记录数:{}",pageNum,pageSize);
        PageHelper.startPage(pageNum,pageSize);
        List<ClassBookListItemVO> list = bookingMapper.listBooking();
        PageInfo<ClassBookListItemVO> pageInfo=new PageInfo<>(list);
        PageData<ClassBookListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public ClassBookStartedVO getStandardByDateTime(LocalDate date, LocalTime time) {
        log.debug("开始执行[通过上课日期和时间查询预约课程信息]的数据请求, 参数: {}, {}",date, time);
        return bookingMapper.getStandardByDateTime(date, time);
    }

    @Override
    public PageData<ClassBookListItemVO> listByClassIdANDCoachId(Long classId, Long coachId, Integer pageNum, Integer pageSize) {
        log.debug("开始执行[根据课程ID和教练ID查询列表]的数据请求,课程ID:{},教练ID:{},页码:{},每页记录数:{} ",classId,coachId,pageNum,pageSize);
        PageHelper.startPage(pageNum,pageSize);
        List<ClassBookListItemVO> list = bookingMapper.listByClassIdANDCoachId(classId,coachId);
        PageInfo<ClassBookListItemVO> pageInfo=new PageInfo<>(list);
        PageData<ClassBookListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }
}
