package cn.tedu.gym.admin.server.admin.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserListItemVO implements Serializable {
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 密码
     */
    //private String password;

}
