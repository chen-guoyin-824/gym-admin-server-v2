package cn.tedu.gym.admin.server.gymClass.service;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassUpdateInfoParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.*;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Transactional
public interface ClassService {
    /**
     * 添加课程信息
     * @param classAddNewParam 封装了前端传入的课程信息
     */
    void addNew(ClassAddNewParam classAddNewParam);

    /**
     * 根据课程id删除课程
     * @param id 课程id
     */
    void deleteById(Long id);

    /**
     * 根据课程id修改课程信息
     * @param id 需要修改的课程id
     * @param classUpdateInfoParam 封装了需要修改的课程信息
     */
    void updateById(Long id, ClassUpdateInfoParam classUpdateInfoParam);

    /**
     * 根据教练id查询课程列表
     * @param coachId 教练id
     * @return 课程列表
     */
    List<ClassListItemVO> listByCoachId(Long coachId);

    /**
     * 查询课程列表
     * @param pageNum 页码
     * @return 课程列表
     */
    PageData<ClassListItemVO> list(Integer pageNum, Integer type);

    /**
     * 查询课程列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 课程列表
     */
    PageData<ClassListItemVO> list(Integer pageNum, Integer pageSize, Integer type);

    /**
     * 根据id查询课程详情
     * @param id 课程id
     * @return 封装课程详情的对象
     */
    ClassStandardVO getStandardById(Long id);

    List<ClassBookTableListItemVO> listByWeekTime(LocalDate localDate, LocalTime dateTime);

    /**
     * 用于系统首页展示经营概况
     * @param classDate 系统当前时间 年月日
     * @return
     */
    List<HomeClassOperateVO> classOperate(LocalDate classDate,Integer type);

    /**
     * 用于系统首页展示课程收入
     * @return
     */
    ClassMoneyCountVO classMoneyCount();
}
