package cn.tedu.gym.admin.server.equipment.pojo.vo;

import lombok.Data;

import java.io.Serializable;
@Data
public class EquipmentTypeListVO implements Serializable {
    private Long id;
    private String typeName;
}
