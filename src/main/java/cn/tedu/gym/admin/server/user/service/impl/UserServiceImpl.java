package cn.tedu.gym.admin.server.user.service.impl;

import cn.tedu.gym.admin.server.member.pojo.param.MemberAddNewParam;
import cn.tedu.gym.admin.server.user.pojo.entity.User ;
import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.po.UserJwtInfoPO;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.user.dao.cache.UserCacheRepository;
import cn.tedu.gym.admin.server.user.dao.persist.repository.UserRepository;
import cn.tedu.gym.admin.server.user.pojo.param.UserLoginInfoParam;
import cn.tedu.gym.admin.server.user.pojo.vo.UserLoginResultVO;
import cn.tedu.gym.admin.server.user.security.CustomUserDetails;
import cn.tedu.gym.admin.server.user.service.UserService;
import com.alibaba.fastjson.JSON;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Value("${gym.jwt.secret-key}")
    private String secretKey;
    @Value("${gym.jwt.duration-in-minute}")
    private long durationInMinute;
    @Autowired
    private UserCacheRepository userCacheRepository;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    public UserServiceImpl() {
        log.debug("创建业务类对象：UserServiceImpl");
    }

    @Override
    public void userAddNew(MemberAddNewParam memberAddNewParam) {
        log.debug("开始处理【新增用户】的业务，参数：{}", memberAddNewParam);
        User user = new User();
        BeanUtils.copyProperties(memberAddNewParam, user);
        System.out.println("===========================6666===========" + memberAddNewParam.getImage() + "======6666==============================");
        user.setAvatar(memberAddNewParam.getImage());
        userRepository.insert(user);
    }

    @Override
    public UserLoginResultVO login(UserLoginInfoParam userLoginInfoParam, String remoteAddr, String userAgent, HttpServletRequest request) {
        log.debug("开始处理【用户登录】的业务，参数：{}", userLoginInfoParam);

//        // 获取前端传上来的请求头中的count值
//        String str = request.getHeader("Count");
//        int count = Integer.parseInt(str);
//        log.info("count: " + count);
//        // 判断登入次数是否过多, 输错3次后需要验证码进行验证
//        if (count > 2) {
//            // 获取前端传上来的请求头中的uuid值
//            String uuid = request.getHeader("AuthUuid");
//            log.debug("uuid：{}", uuid);
//            // 获取redis中的验证码值
//            String code = userCacheRepository.getAuthByUuid(uuid);
//            // 比较前端传上来的验证码
//            if (!userLoginInfoParam.getCode().toLowerCase().equals(code)) {
//                String message = "验证码错误";
//                log.warn(message);
//                throw new ServiceException(ServiceCode.ERROR_AUTH_ERROR, message);
//            }
//        }

        Authentication authentication = new UsernamePasswordAuthenticationToken(
                userLoginInfoParam.getUsername(), userLoginInfoParam.getPassword());
        log.debug("准备调用AuthenticationManager的认证方法，判断此用户名、密码是否可以成功登录……");
        Authentication authenticateResult
                = authenticationManager.authenticate(authentication);
        log.debug("验证用户登录成功，返回的认证结果：{}", authenticateResult);

        Object principal = authenticateResult.getPrincipal();
        log.debug("从认证结果中获取当事人：{}", principal);
        CustomUserDetails userDetails = (CustomUserDetails) principal;
        Long id = userDetails.getId();
        log.debug("从认证结果中的当事人中获取ID：{}", id);
        String username = userDetails.getUsername();
        log.debug("从认证结果中的当事人中获取用户名：{}", username);
        String avatar = userDetails.getAvatar();
        log.debug("从认证结果中的当事人中获取头像：{}", avatar);
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        log.debug("从认证结果中的当事人中获取权限列表：{}", authorities);
        String authoritiesJsonString = JSON.toJSONString(authorities);
        log.debug("将权限列表对象转换为JSON格式的字符串：{}", authoritiesJsonString);

        Date date = new Date(System.currentTimeMillis() + 1L * 60 * 1000 * durationInMinute);
        //                                                 ↑ 注意加L，避免int溢出为负数
        Map<String, Object> claims = new HashMap<>();
        claims.put("id", id);
        claims.put("username", username);
        // 生成JWT时，不再存入权限列表
        // claims.put("authoritiesJsonString", authoritiesJsonString);
        String jwt = Jwts.builder()
                .setHeaderParam("alg", "HS256")
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();

        // 生成JWT之后，需要将权限列表存入到Redis中
        UserJwtInfoPO userJwtInfoPO = new UserJwtInfoPO();
        userJwtInfoPO.setUserAgent(userAgent);
        userJwtInfoPO.setIp(remoteAddr);
        userJwtInfoPO.setAuthoritiesJsonString(authoritiesJsonString);
        userCacheRepository.saveLoginInfo(jwt, userJwtInfoPO);

        // 将用户状态存入到Redis中
        userCacheRepository.saveEnableByUserId(id, 1);

        UserLoginResultVO userLoginResultVO = new UserLoginResultVO()
                .setId(id)
                .setUsername(username)
                .setAvatar(avatar)
                .setToken(jwt);
        return userLoginResultVO;
        // 改为使用JWT后，不必在登录成功后就将认证信息存入到SecurityContext中
        // log.debug("准备将认证信息结果存入到SecurityContext中……");
        // SecurityContext securityContext = SecurityContextHolder.getContext();
        // securityContext.setAuthentication(authenticateResult);
        // log.debug("已经将认证信息存入到SecurityContext中，登录业务处理完成！");
    }

//    @Override
//    public void rebuildSearch() {
//        log.debug("开始向Redis中存入用户相关信息");
//        userRepository.
//    }


}
