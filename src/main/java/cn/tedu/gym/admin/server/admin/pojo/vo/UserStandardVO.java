package cn.tedu.gym.admin.server.admin.pojo.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class UserStandardVO implements Serializable {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 姓名
     */
    private String name;
    /**
     * 性别,0表示保密,1表示男,2表示女
     */
    private Integer gender;
    /**
     * 电话号码
     */
    private String phone;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 角色,0表示超级管理员,1表示管理,2表示教练,3表示会员
     */
    private Integer role;
    /**
     * 启用 0表示启用,1表示禁用
     */
    private Integer enable;

}
