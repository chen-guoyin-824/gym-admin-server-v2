package cn.tedu.gym.admin.server.gymInfo.service;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymInfo.pojo.entity.GymInfo;
import cn.tedu.gym.admin.server.gymInfo.pojo.param.GymInfoAddParam;
import cn.tedu.gym.admin.server.gymInfo.pojo.param.GymInfoUpdateInfoParam;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoListItemVO;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoStandardVO;

import java.util.List;

/**
 * @auther xw
 * @create 2023/7/7
 */
public interface IGymInfoService {
    /**
     * 启用状态的显示文本
     */
    String ENABLE_TEXT[] = {"禁用", "启用"};
    void addNew(GymInfoAddParam addParam);

    void deleteById(Long id);

    PageData<GymInfoListItemVO> list(Integer pageNum);
    PageData<GymInfoListItemVO> list(Integer pageNum,Integer pageSize);

    void setEnable(Long id);

    void setDisable(Long id);

    void updateById(Long id ,GymInfoUpdateInfoParam gymInfoUpdateInfoParam);

    List<GymInfoStandardVO> getStandardByAddress(String address);

    GymInfoStandardVO getStandardById(Long id);
}
