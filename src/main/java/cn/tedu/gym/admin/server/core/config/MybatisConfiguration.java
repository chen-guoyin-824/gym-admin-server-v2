package cn.tedu.gym.admin.server.core.config;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
@MapperScan({"cn.tedu.gym.admin.server.gymClass.dao.persist.mapper",
        "cn.tedu.gym.admin.server.admin.dao.persist.mapper",
        "cn.tedu.gym.admin.server.user.dao.persist.mapper",
        "cn.tedu.gym.admin.server.gymInfo.dao.persist.mapper",
        "cn.tedu.gym.admin.server.coach.dao.persist.mapper",
        "cn.tedu.gym.admin.server.member.dao.persist.mapper",
        "cn.tedu.gym.admin.server.equipment.dao.mapper"
})
public class MybatisConfiguration {
}
