package cn.tedu.gym.admin.server.admin.dao.persist.mapper;

import cn.tedu.gym.admin.server.admin.pojo.entity.UserRole;
import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper extends BaseMapper<UserRole> {


    /**
     * 查询角色列表
     *
     * @return 角色列表
     */
    List<RoleListItemVO> list();
}
