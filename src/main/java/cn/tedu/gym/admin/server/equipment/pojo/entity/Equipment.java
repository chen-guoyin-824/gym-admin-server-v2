package cn.tedu.gym.admin.server.equipment.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("gym_equipment")
public class Equipment implements Serializable {
    @TableId(type = IdType.AUTO)
    /**
     * 器材id
     */
    private Long id;

    /**
     * 器材分类id
     */
    private Long typeId;
    /**
     * 器材名称
     */

    private String name;

    /**
     * 器材图片
     */
    private String img;


    /**
     * 是否启用， 1=启用， 0=不启用
     */
    private Integer enable;

    /**
     * 器材状态
     */
    private Integer state;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;

    /**
     * 数据最后修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;
}
