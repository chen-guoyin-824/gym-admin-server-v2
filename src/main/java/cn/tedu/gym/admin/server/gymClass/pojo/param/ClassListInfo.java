package cn.tedu.gym.admin.server.gymClass.pojo.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClassListInfo implements Serializable {
    /**
     * 页码
     */
    private Integer page;
    /**
     * 课程类别
     */
    private Integer type;
}
