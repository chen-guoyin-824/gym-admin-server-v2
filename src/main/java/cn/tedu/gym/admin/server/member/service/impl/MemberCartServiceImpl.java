package cn.tedu.gym.admin.server.member.service.impl;

import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.member.dao.persist.repository.MemberCartRepository;
import cn.tedu.gym.admin.server.member.dao.persist.repository.MemberRepository;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMemberCart;
import cn.tedu.gym.admin.server.member.pojo.param.GymMemberCartAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.param.MemberAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.vo.CartStandardVO;
import cn.tedu.gym.admin.server.member.service.MemberCartService;
import cn.tedu.gym.admin.server.user.dao.persist.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class MemberCartServiceImpl implements MemberCartService {
    public static final int INT = 300;
    @Value("${gym.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    private MemberCartRepository memberCartRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    UserRepository userRepository;
    @Override
    public void updateCountById(Long id) {
        GymMemberCart gymMemberCart = memberCartRepository.getStandardById(id);
        int count = gymMemberCart.getCount();
        count--;
        gymMemberCart.setCount(count);
        memberCartRepository.updateById(gymMemberCart);
    }

    @Override
    public void updateStatusById(Long id) {
        GymMemberCart gymMemberCart = memberCartRepository.getStandardById(id);
//        if (gymMemberCart.getStatus()!=1 ||gymMemberCart.getStatus()!=0){
//            String message = "修改状态失败，数据不存在！";
//            log.warn(message);
//            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
//        }
        if (gymMemberCart.getStatus() == 1) {
            gymMemberCart.setStatus(0);
        } else if (gymMemberCart.getStatus() == 0) {
            gymMemberCart.setStatus(1);
        }
        memberCartRepository.updateById(gymMemberCart);
    }

    @Override
    public void insert(GymMemberCartAddNewParam gymMemberCartAddNewParam) {
        log.debug("开始处理【发布会员卡】的业务，当事人：{}", gymMemberCartAddNewParam);
        gymMemberCartAddNewParam.setMemberId(gymMemberCartAddNewParam.getMemberId());
        if (gymMemberCartAddNewParam.getName()==null){gymMemberCartAddNewParam.setName("普通会员");}
        if (gymMemberCartAddNewParam.getMemberLevel()==null){gymMemberCartAddNewParam.setMemberLevel(1);}
        if (gymMemberCartAddNewParam.getStatus()==null){gymMemberCartAddNewParam.setStatus(1);}
        if (gymMemberCartAddNewParam.getCount()==null){gymMemberCartAddNewParam.setCount(0);}
        GymMemberCart gymMemberCart = new GymMemberCart();
        BeanUtils.copyProperties(gymMemberCartAddNewParam,gymMemberCart);
        int rows = memberCartRepository.insert(gymMemberCart);
        if (rows != 1) {
            String message = "发布文章失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }

    @Override
    public void insertPlus(MemberAddNewParam memberAddNewParam) {
        log.debug("开始处理【新增会员卡】的业务，数据为：{}", memberAddNewParam);
        GymMemberCart cartAddNewParam = new GymMemberCart();
        BeanUtils.copyProperties(memberAddNewParam,cartAddNewParam);
        //获取用户Id
        cartAddNewParam.setMemberId(userRepository.getStandardByUsername(memberAddNewParam.getName()).getId());
        //获取会员等级
        Integer memberLevel = memberAddNewParam.getMemberLevel();
        cartAddNewParam.setMemberLevel(memberLevel);
        //转化成中文会员等级
        cartAddNewParam.setName((memberLevel>1) ? "超级会员" : (memberLevel>0) ? "普通会员" : "一般会员");
        memberCartRepository.insert(cartAddNewParam);
    }

    @Override
    public void delete(Long id) {
        log.debug("开始处理【根据ID删除会员】的业务，参数：{}", id);
        GymMemberCart queryResult = memberCartRepository.getStandardById(id);
        if (queryResult == null) {
            // 是：数据不存在，抛出异常
            String message = "删除文章失败，尝试删除的文章数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        int rows = memberCartRepository.deleteById(id);
        if (rows != 1) {
            String message = "删除文章失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_DELETE, message);
        }
    }

    @Override
    public GymMemberCart getStandardById(Long id) {
        log.debug("开始处理【根据ID查询文章详情】的业务，参数：{}", id);
        GymMemberCart queryResult = memberCartRepository.getStandardById(id);
        if (queryResult == null) {
            String message = "查询文章详情失败，文章数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return queryResult;
    }

    @Override
    public PageData<GymMemberCart> list(Integer pageNum) {
        log.debug("开始处理[查询会员列表]的业务, 参数: ✌✌页码: {}", pageNum);
        PageData<GymMemberCart> pageData = memberCartRepository.list(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public PageData<GymMemberCart> list(Integer pageNum, Integer pageSize) {
        log.debug("开始处理[查询会员列表]的业务, 参数: ✌✌页码: {}", pageNum);
        PageData<GymMemberCart> pageData = memberCartRepository.list(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public void addTimesById(Long id, int counts, boolean manuallyInput) {
        GymMemberCart gymMemberCart = memberCartRepository.getStandardById(id);
        if (gymMemberCart != null) {
            int currentTimes = gymMemberCart.getCount();
            int newTimes;
            if (manuallyInput) {
                counts = 1;
                newTimes = currentTimes+counts; // 使用手动输入的次数
            } else {
                newTimes = currentTimes + counts; // 自动添加次数
            }
            gymMemberCart.setCount(newTimes);
            System.out.println("Updated times: " + newTimes);
            memberCartRepository.updateById(gymMemberCart);
        }
    }

    @Override
    public void addTimesById(Long id) {
        GymMemberCart gymMemberCart = memberCartRepository.getStandardById(id);
        int count = gymMemberCart.getCount();
        count++;
        gymMemberCart.setCount(count);
        memberCartRepository.updateById(gymMemberCart);
//        if (gymMemberCart != null) {
//            int currentTimes = gymMemberCart.getCount();
//            int newTimes = currentTimes +1;
//            gymMemberCart.setCount(newTimes);
//            System.out.println("Updated times: " + newTimes);
//            memberCartRepository.updateById(gymMemberCart);
//        }
    }

    @Override
    public List<CartStandardVO> getListById(Long id) {
        log.debug("开始处理【根据ID查询文章详情】的业务，参数：{}", id);
        List<CartStandardVO> queryResult = memberCartRepository.getListById(id);
        if (queryResult == null) {
            String message = "查询文章详情失败，文章数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return queryResult;
    }
}
