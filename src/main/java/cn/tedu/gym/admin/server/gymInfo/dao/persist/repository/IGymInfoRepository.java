package cn.tedu.gym.admin.server.gymInfo.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymInfo.pojo.entity.GymInfo;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoListItemVO;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoStandardVO;

import java.util.List;

/**
 * @auther xw
 * @create 2023/7/7
 */

public interface IGymInfoRepository {
    /**
     * 插入标签数据
     *
     * @param gymInfo 标签数据
     * @return 受影响的行数
     */
    int insert(GymInfo gymInfo);

    int deleteById(Long id);

    int updateById(GymInfo gymInfo);

    /**
     * 查询场馆数据列表
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 文章数据列表
     */
    PageData<GymInfoListItemVO> list(Integer pageNum, Integer pageSize);

    /**
     * 根据名字查询场馆
     *
     * @param title
     * @return
     */

    int countByTitle(String title);

    GymInfoStandardVO getStandardById(Long id);

    List<GymInfoStandardVO> getStandardByAddress(String address);


}
