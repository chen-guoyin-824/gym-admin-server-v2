package cn.tedu.gym.admin.server.user.service;

import cn.tedu.gym.admin.server.member.pojo.param.MemberAddNewParam;
import cn.tedu.gym.admin.server.user.pojo.param.UserLoginInfoParam;
import cn.tedu.gym.admin.server.user.pojo.vo.UserLoginResultVO;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    /**
     * 插入新增用户数据
     */
    void userAddNew(MemberAddNewParam memberAddNewParam);
    /**
     * 用户登录
     *
     * @param userLoginInfoParam 封装了登录信息的对象
     */
    UserLoginResultVO login(UserLoginInfoParam userLoginInfoParam, String remoteAddr, String userAgent, HttpServletRequest request);
//
//    void rebuildSearch();
}
