package cn.tedu.gym.admin.server.member.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMember;
import cn.tedu.gym.admin.server.member.pojo.vo.*;

import java.util.List;

public interface MemberRepository {
    /**
     * 查询会员列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageData<MemberListItemVO> list1(Integer pageNum, Integer pageSize);
    /**
     * 插入课程
     * @param
     * @return 影响会员表的行数
     */
    int insert(GymMember gymMember);

    /**
     * 根据id查询会员详情
     * @param id 会员id
     * @return 封装课程详情的对象
     */
    MemberStandardVO getStandardById(Long id);

    //MemberListItemDTO getListById(Long id);


    /**
     * 预约记录列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 课程预约列表
     */
    PageData<MemberBookingListItemVO> listBooking(Integer pageNum, Integer pageSize);


    MemberListItemVO getListByCardId(Long id);

    int deleteById(Long id);
    /**
     * 用于首页展示最新办卡
     * @return
     */
    List<MemberNewListVO> memberNew();

    /**
     *用于首页展示
     * @return
     */
    HomeMemberVO homeMember();

}
