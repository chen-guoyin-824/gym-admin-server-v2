package cn.tedu.gym.admin.server.coach.pojo.param;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class CoachAddNewParam implements Serializable {
    //用户头像
    private String avatar;
    //姓名
    private String name;
    //排序
    private long sort;
    //用户名
    private String username;
    //密码
    private String password;
    //性别
    private Integer gender;
    //手机
    private String phone;
    //签名
    private String target;
    //简介
    private String profile;
    /**
     * 从业时间
     */
    private String worktime;
    /**
     * 擅长标签
     */
    private String skills;
    /**
     * 每日私教课上限
     */
    private Integer upperLimit;
    //相册
    private List photoList;
//    //场馆
//    private String address;
    //场馆id
    private Long gymId;
    //是否启用
    private Integer enable;


}
