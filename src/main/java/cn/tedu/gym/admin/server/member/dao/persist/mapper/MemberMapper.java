package cn.tedu.gym.admin.server.member.dao.persist.mapper;

import cn.tedu.gym.admin.server.member.pojo.entity.GymMember;
import cn.tedu.gym.admin.server.member.pojo.vo.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MemberMapper extends BaseMapper<GymMember> {
    /**
     * 查询会员列表
     * @return 会员列表
     */
    List<MemberListItemVO> list1();

    /**
     * 根据课程id查询会员详情
     * @param id 课程id
     * @return 封装课程信息的对象
     */
    MemberStandardVO getStandardById(Long id);
    MemberListItemVO getListById(Long id);

    /**
     * 预约记录列表
     * @return 课程预约记录列表
     */
    List<MemberBookingListItemVO> listBooking();

    /**
     * 用于首页展示最新办卡
     * @return
     */
    List<MemberNewListVO> memberNew();



    /**
     *用于首页展示
     * @return
     */
    HomeMemberVO homeMember();

}
