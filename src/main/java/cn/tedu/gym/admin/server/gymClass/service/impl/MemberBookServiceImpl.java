package cn.tedu.gym.admin.server.gymClass.service.impl;

import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassBookRepository;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassRepository;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.MemberBookRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassBook;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassMember;
import cn.tedu.gym.admin.server.gymClass.pojo.param.MemberBookAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassStandardVO;
import cn.tedu.gym.admin.server.gymClass.service.MemberBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MemberBookServiceImpl implements MemberBookService {
    @Value("${gym.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    MemberBookRepository memberBookRepository;
    @Autowired
    ClassBookRepository classBookRepository;
    @Autowired
    ClassRepository classRepository;

    @Override
    public void addNew(MemberBookAddNewParam memberBookAddNewParam) {
        log.debug("开始执行[新增会员预约课程]的业务, 参数: {}", memberBookAddNewParam);
        //TODO 检查代码的合理性
        ClassBookStartedVO classBookStartedVO = classBookRepository.selectById(memberBookAddNewParam.getBookClassId());
        ClassStandardVO standardById = classRepository.getStandardById(classBookStartedVO.getId());
        if (standardById.getPersonMax() <= classBookStartedVO.getBookNumber()){
            String message = "会员预约课程错误, 当前课程已满员";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
        System.out.println(classBookStartedVO.getId());
        ClassMember classMember = new ClassMember();
        BeanUtils.copyProperties(memberBookAddNewParam, classMember);
        classMember.setSignInStatus(0);
        int rows = memberBookRepository.insert(classMember);
        if (rows != 1){
            String message = "添加会员预约课程失败, 插入错误";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
        Long classBookId = classBookStartedVO.getId();
        ClassBook classBook = new ClassBook();
        classBook.setId(classBookId);
        classBook.setBookNumber(classBookStartedVO.getBookNumber()+1);
        classBookRepository.update(classBook);
    }

    @Override
    public int cancelById(Long id) {
        log.debug("开始执行[取消预约]的业务,ID:{} ",id);
        //TODO 查询要取消的数据是否存在
        ClassBookStartedVO classBookStartedVO = classBookRepository.selectById(id);
        if (classBookStartedVO==null){
            String message="操作失败,当前预约记录不存在";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,message);
        }
        int row = memberBookRepository.cancelById(id);
        if (row!=1){
            String message="服务器忙,请稍后再试!";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_UNKNOWN,message);
        }
        return row;
    }

    @Override
    public void setSignedInStatus(Long id) {
        //TODO 验证一下id所在的数据是否存在, 或者状态是否已是已签到
        log.debug("开始处理[设置签到状态为已签到]的业务, 参数: {}", id);
        ClassMember updateClassMember = new ClassMember();
        updateClassMember.setSignInStatus(1);
        updateClassMember.setId(id);
        int rows = memberBookRepository.update(updateClassMember);
        if (rows != 1) {
            String message = "设置签到状态为已签到失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }
    }

    @Override
    public PageData<ClassBookDetailListVO> listBookingDetail(Long bookClassId, Integer pageNum) {
        log.debug("开始处理[根据课程ID查询预约详情列表]的业务, 课程ID:{},页码: {} ",bookClassId,pageNum );
        PageData<ClassBookDetailListVO> pageData = memberBookRepository.listBookingDetail(bookClassId, pageNum, defaultQueryPageSize);
        return pageData;

    }

    @Override
    public PageData<ClassBookDetailListVO> listBookingDetail(Long bookClassId, Integer pageNum, Integer pageSize) {
        log.debug("开始处理[根据课程ID查询预约详情列表]的业务, 课程ID:{},页码: {}, 每页记录数: {}",bookClassId, pageNum, pageSize);
        PageData<ClassBookDetailListVO> pageData = memberBookRepository.listBookingDetail(bookClassId, pageNum, pageSize);
        return pageData;
    }


}
