package cn.tedu.gym.admin.server.admin.service.impl;

import cn.tedu.gym.admin.server.admin.dao.persist.repository.IUserRoleRepository;
import cn.tedu.gym.admin.server.admin.pojo.entity.UserRole;
import cn.tedu.gym.admin.server.admin.service.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl implements IUserRoleService {
    @Autowired
    IUserRoleRepository userRoleRepository;
    @Override
    public void insert(UserRole userRole) {
        userRoleRepository.insert(userRole);
    }
}
