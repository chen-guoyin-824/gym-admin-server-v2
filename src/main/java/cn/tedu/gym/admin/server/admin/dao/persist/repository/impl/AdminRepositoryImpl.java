package cn.tedu.gym.admin.server.admin.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.admin.dao.persist.mapper.AdminMapper;
import cn.tedu.gym.admin.server.admin.dao.persist.repository.IAdminRepository;
import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminAddNewParam;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserListItemVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Wrapper;
import java.util.List;

@Slf4j
@Repository
public class AdminRepositoryImpl implements IAdminRepository {
    @Autowired
    AdminMapper adminMapper;

    public AdminRepositoryImpl() {
        log.debug("创建存储器对象:AdminRepositoryImpl");
    }

    @Override
    public UserListItemVO adminInfoById(Long id) {
        log.debug("开始执行[查询管理员信息]的数据访问,管理员id:{}", id);
        return adminMapper.adminInfoById(id);
    }

    @Override
    public UserStandardVO adminDetailById(Long id) {
        log.debug("开始执行[查询管理员信息详情]的数据访问,管理员id:{}", id);
        UserStandardVO userStandardVO = adminMapper.adminDetailById(id);
        return userStandardVO;
    }

    @Override
    public int updateAdminInfoById(User user) {
        log.debug("开始执行[修改管理员信息]的数据访问,参数:{}", user);
        return adminMapper.updateById(user);
    }

    @Override
    public int addNewAdmin(User user) {
        log.debug("开始执行[新增管理员]的数据访问,参数:{}", user);
        return adminMapper.insert(user);
    }

    @Override
    public int selectCount(String username) {
        log.debug("开始执行[根据用户名统计查询管理员]的数据访问,参数:{}", username);
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.eq("username",username);
        return adminMapper.selectCount(wrapper);
    }

    @Override
    public int selectCountByPhone(String phone) {
        log.debug("开始执行[根据手机号统计查询管理员]的数据访问,参数:{}", phone);
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.eq("phone",phone);
        return adminMapper.selectCount(wrapper);
    }

    @Override
    public PageData<UserDetailListVO> adminDetailList(Integer pageNum,Integer pageSize) {
        log.debug("开始执行[查询管理员列表]的数据访问,页码:{},每页记录数:{}",pageNum,pageSize);
        PageHelper.startPage(pageNum,pageSize);
        List<UserDetailListVO> list = adminMapper.adminDetailList();
        PageInfo<UserDetailListVO> pageInfo=new PageInfo<>(list);
        PageData<UserDetailListVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;

    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行[根据ID删除管理员]的数据访问,ID:{}",id);
        return adminMapper.deleteById(id);
    }

    @Override
    public int updateById(User user) {
        log.debug("开始执行【更新用户】的数据访问，参数：{}", user);
        return adminMapper.updateById(user);
    }
}
