package cn.tedu.gym.admin.server.gymClass.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.ClassMapper;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassBook;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.GymClass;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Slf4j
@Repository
public class ClassRepositoryImpl implements ClassRepository {

    @Autowired
    ClassMapper classMapper;

    @Override
    public PageData<ClassListItemVO> list(Integer pageNum, Integer pageSize, Integer type) {
        log.debug("开始执行[查询课程列表]的数据访问, 参数: 页码: {}, 每页记录数: {}, 课程类型: {}", pageNum, pageSize, type);
        PageHelper.startPage(pageNum, pageSize);
        List<ClassListItemVO> list = classMapper.list(type);
        for (ClassListItemVO classListItemVO : list) {
            System.out.println("列表项: " + classListItemVO);
        }
        PageInfo<ClassListItemVO> pageInfo = new PageInfo<>(list);
        PageData<ClassListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        List<ClassListItemVO> list1 = pageData.getList();
        for (ClassListItemVO classListItemVO : list1) {
            System.out.println("列表项:" + classListItemVO);
        }
        return pageData;
    }

    @Override
    public List<ClassListItemVO> listByCoachId(Long coachId) {
        log.debug("开始执行[根据教练id查询课程列表], 参数: {}", coachId);
        List<ClassListItemVO> classList = classMapper.listByCoachId(coachId);
        return classList;
    }

    @Override
    public int insert(GymClass gymClass) {
        log.debug("开始执行[插入课程数据], 参数: {}", gymClass);
        return classMapper.insert(gymClass);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行[根据课程id删除课程], 参数: {}", id);
        return classMapper.deleteById(id);
    }

    @Override
    public int updateById(GymClass gymClass) {
        log.debug("开始执行[根据课程id修改课程信息], 参数: {}", gymClass);
        return classMapper.updateById(gymClass);
    }

    @Override
    public ClassStandardVO getStandardById(Long id) {
        log.debug("开始执行[根据课程id查询课程详情], 参数: {}", id);
        ClassStandardVO standardById = classMapper.getStandardById(id);
        return standardById;
    }

    @Override
    public List<ClassBookTableListItemVO> listByWeekTime(List<LocalDate> dateList, LocalTime dateTime) {
        log.debug("开始执行[通过一周的日期时间查询课程预约列表], 参数: {}", dateList);
        List<ClassBookTableListItemVO> classBookTableList = classMapper.listByWeekTime(dateList, dateTime);
        return classBookTableList;
    }

    @Override
    public List<HomeClassOperateVO> classOperate(LocalDate classDate,Integer type) {
        log.debug("开始执行[用于系统首页展示经营概况的团课查询], 参数: {}", classDate);
        return classMapper.classOperate(classDate,type);
    }

    @Override
    public ClassMoneyCountVO classMoneyCount() {
        log.debug("开始执行[用于系统首页展示课程收入]");
        return classMapper.classMoneyCount();
    }
}
