package cn.tedu.gym.admin.server.member.service;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.member.pojo.param.MemberAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.vo.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface MemberService {
    /**
     * 添加会员信息
     * @param memberAddNewParam 封装了前端传入的课程信息
     */
    void addNew(MemberAddNewParam memberAddNewParam);

    /**
     * 查询会员列表
     * @param pageNum
     * @return
     */

    PageData<MemberListItemVO> list1(Integer pageNum);

    /**
     * 查询会员列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageData<MemberListItemVO> list1(Integer pageNum, Integer pageSize);


    /**
     * 根据id查询会员详情
     * @param id 会员id
     * @return 封装会员详情的对象
     */
    MemberStandardVO getStandardById(Long id);

    MemberListItemVO getListById(Long id);

    /**
     * 预约记录列表 使用默认的每页记录数
     * @param pageNum 页码
     * @return 会员预约列表
     */
    PageData<MemberBookingListItemVO> listBooking(Integer pageNum);

    /**
     * 预约记录列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 会员预约列表
     */
    PageData<MemberBookingListItemVO> listBooking(Integer pageNum, Integer pageSize);

    void deleteById(Long id);

    /**
     * 用于首页展示最新办卡
     * @return
     */
    List<MemberNewListVO> memberNew();
    /**
     *用于首页展示
     * @return
     */
    HomeMemberVO homeMember();
}
