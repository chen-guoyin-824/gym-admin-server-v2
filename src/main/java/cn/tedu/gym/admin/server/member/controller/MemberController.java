package cn.tedu.gym.admin.server.member.controller;

import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.coach.service.CoachService;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.member.pojo.param.GymMemberCartAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.param.MemberAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.vo.HomeMemberVO;
import cn.tedu.gym.admin.server.member.pojo.vo.MemberListItemVO;
import cn.tedu.gym.admin.server.member.pojo.vo.MemberNewListVO;
import cn.tedu.gym.admin.server.member.pojo.vo.MemberStandardVO;
import cn.tedu.gym.admin.server.member.service.MemberCartService;
import cn.tedu.gym.admin.server.member.service.MemberService;
import cn.tedu.gym.admin.server.resource.pojo.vo.UploadResult;
import cn.tedu.gym.admin.server.user.service.UserService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/member")
@Slf4j
@Validated
@Api(tags = "1.2.会员管理模块")
public class MemberController {

    @Value("${gym.upload.host}")
    private String host;
    @Value("${gym.upload.base-dir-name}")
    private String baseDirName;
    @Value("${gym.upload.root-dir-name}")
    private String uploadRootDirName;
    @Value("${gym.upload.article-image.max-size}")
    private Integer articleImageMaxSize;
    @Value("${gym.upload.article-image.types}")
    private List<String> articleImageValidTypes;

    private String ImageDirName = "class-image/";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd/");

    @Autowired
    UserService userService;
    @Autowired
    MemberService memberService;
    @Autowired
    MemberCartService memberCartService;
    @Autowired
    CoachService coachService;

    @PostMapping("/add-new")
    @ApiOperation("添加会员")
    @ApiOperationSupport(order = 110)
    public JsonResult addNew(@Valid MemberAddNewParam memberAddNewParam){
        log.debug("开始处理[添加用户]的业务, 参数: {}", memberAddNewParam);
        userService.userAddNew(memberAddNewParam);
        log.debug("开始处理[添加会员]的业务, 参数: {}", memberAddNewParam);
        memberService.addNew(memberAddNewParam);
        log.debug("开始处理[添加会员卡]的业务, 参数: {}", memberAddNewParam);
        memberCartService.insertPlus(memberAddNewParam);
        return JsonResult.ok();
    }


    @GetMapping("list")
    @ApiOperation("查询会员列表")
    @ApiOperationSupport(order = 415)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "queryType", value = "查询类型", example = "all")
    })
    public JsonResult list1(Integer page, String queryType){
        log.debug("开始处理[查询会员列表]的业务, 参数: 页码: {}", page);
        Integer pageNum = page==null ? 1 : page;
        PageData<MemberListItemVO> pageData;
        if ("all".equals(queryType)){
            pageData = memberService.list1(pageNum, Integer.MAX_VALUE);
        }else {
            pageData = memberService.list1(pageNum);
        }
        return JsonResult.ok(pageData);
    }

    @GetMapping("standard-by-id")
    @ApiOperation("根据id查询会员信息")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会员id", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult getStandardById(Long id){
        log.debug("开始处理[根据id查询会员详情]的业务, 参数: 页码: {}", id);
        MemberStandardVO standardById = memberService.getStandardById(id);
        return JsonResult.ok(standardById);
    }

    @PostMapping("/deleteById")
    @ApiOperation("根据ID删除管理员")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会员ID", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult deleteById(Long id){
        log.debug("开始处理[根据ID删除会员]的请求,ID:{}",id);
        memberService.deleteById(id);
        return JsonResult.ok();
    }
    private String GymImageDirName = "member-profile-picture/";
    @PostMapping("/upload/member-profile-picture")
    @ApiOperation("上传会员头像")//TODO:未实现图片上传
    public JsonResult uploadGymInfoImage(@RequestParam("file") MultipartFile multipartFile) throws Throwable {

        if (multipartFile == null || multipartFile.isEmpty()) {
            String message = "上传场馆图片失败，请选择您要上传的文件！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPLOAD_EMPTY, message);
        }

        long size = multipartFile.getSize();
        if (size > articleImageMaxSize * 1024 * 1024) {
            String message = "上传场馆图片失败，不允许使用超过" + articleImageMaxSize + "MB的图片文件！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPLOAD_EXCEED_MAX_SIZE, message);
        }

        String contentType = multipartFile.getContentType();
        if (!articleImageValidTypes.contains(contentType)) {
            String message = "上传场馆图片失败，请使用以下类型的图片文件：" + articleImageValidTypes;
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPLOAD_INVALID_TYPE, message);
        }

        String dirName = simpleDateFormat.format(new Date());
        File uploadBaseDir = new File(uploadRootDirName, baseDirName);
        File articleImageDir = new File(uploadBaseDir, GymImageDirName);
        File uploadDir = new File(articleImageDir, dirName);

        if (!uploadDir.exists()) {
            uploadDir.mkdirs();
        }

        String newFileName = UUID.randomUUID().toString();
        String originalFilename = multipartFile.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String newFullFileName = newFileName + suffix;
        File newFile = new File(uploadDir, newFullFileName);

        multipartFile.transferTo(newFile);

        String url = new StringBuilder()
                .append(host)
                .append(baseDirName)
                .append(GymImageDirName)
                .append(dirName)
                .append(newFullFileName)
                .toString();

        UploadResult uploadResult = new UploadResult();
        uploadResult.setUrl(url);
        uploadResult.setFileSize(size);
        uploadResult.setContentType(contentType);
        uploadResult.setFileName(newFullFileName);
        System.out.println(uploadResult);
        return JsonResult.ok(uploadResult);
    }

    @GetMapping("memberNew")
    @ApiOperation("首页展示最新办卡")
    @ApiOperationSupport(order = 430)
    public JsonResult memberNew( ){
        log.debug("开始处理[用于首页展示最新办卡]的业务,");
        List<MemberNewListVO> list = memberService.memberNew();
        for (MemberNewListVO listVO : list) {
            listVO.setTime(listVO.getGmtCreate().toString());
        }
        return JsonResult.ok(list);
    }
    @GetMapping("memberCount")
    @ApiOperation("首页展示会员相关")
    @ApiOperationSupport(order = 440)
    public JsonResult memberCount( ){
        log.debug("开始处理[用于首页展示最新办卡]的业务,");
        HomeMemberVO homeMemberVO = memberService.homeMember();
        return JsonResult.ok(homeMemberVO);
    }

}
