package cn.tedu.gym.admin.server.admin.dao.persist.repository;

import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;

import java.util.List;

public interface IRoleRepository {

    /**
     * 查询角色列表
     *
     * @return 角色列表
     */
    List<RoleListItemVO> list();
}
