package cn.tedu.gym.admin.server.gymClass.pojo.param;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class ClassBookAddNewParam implements Serializable {
    /**
     * 课程id
     */
    private Long classId;
    /**
     * 教练id
     */
    private Long coachId;
    /**
     * 课程上课日期
     */
    private LocalDate classDate;
    /**
     * 课程上课时间
     */
    private LocalTime classTime;

}
