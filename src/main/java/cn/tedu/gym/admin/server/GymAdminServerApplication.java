package cn.tedu.gym.admin.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GymAdminServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GymAdminServerApplication.class, args);
    }

}
