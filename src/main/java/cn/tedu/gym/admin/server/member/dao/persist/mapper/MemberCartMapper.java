package cn.tedu.gym.admin.server.member.dao.persist.mapper;



import cn.tedu.gym.admin.server.member.pojo.entity.GymMemberCart;
import cn.tedu.gym.admin.server.member.pojo.vo.CartStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberCartMapper extends BaseMapper<GymMemberCart> {


    GymMemberCart getStandardById(Long id);
    List<GymMemberCart> list();
    List<CartStandardVO> getInfoById(Long id);

}
