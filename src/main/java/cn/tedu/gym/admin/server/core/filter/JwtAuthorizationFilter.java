package cn.tedu.gym.admin.server.core.filter;

import cn.tedu.gym.admin.server.common.pojo.po.UserJwtInfoPO;
import cn.tedu.gym.admin.server.common.security.CurrentPrincipal;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.user.dao.cache.UserCacheRepository;
import com.alibaba.fastjson.JSON;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 处理JWT的过滤器
 * 此过滤器
 * 1.尝试接收客户端的请求中携带的JWT数据
 * 2.尝试解析JWT数据
 * 3.将解析得到的用户数据创建为Authentication对象, 存入到SecurityContext上下文中
 */
@Slf4j
@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter implements Filter {
    @Value("${gym.jwt.secret-key}")
    private String secretKey;

    @Autowired
    private UserCacheRepository userCacheRepository;

    public static final int JWT_MIN_LENGTH = 113;

    public JwtAuthorizationFilter(){
        log.info("创建过滤对象: JwtAuthorizationFilter");
    }
    //该方法包含了过滤器初始化和销毁的过程, 更加便捷
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.debug("处理JWT的过滤器");
        //尝试接收客户端的请求中携带的JWT数据
        // 业内惯用的做法是：客户端会将JWT放在请求头中名为Authorization的属性中
        String jwt = request.getHeader("Authorization");
        log.debug("客户端携带的JWT：{}", jwt);

        //没有必要尝试解析格式明显错误的JWT数据, 即判断JWT的基本有效性
        //StringUtils:字符串工具类, hasText()方法判断字符串变量是否含有字符
        if (!StringUtils.hasText(jwt) || jwt.length()<JWT_MIN_LENGTH){
            //对于无效的JWT, 应该直接放行
            log.warn("当前请求中客户端没有携带有效的JWT,将放行");
            filterChain.doFilter(request, response);
            return;
        }

        //从Redis中读取此jwt对应的数据
        UserJwtInfoPO loginInfo = userCacheRepository.getLoginInfo(jwt);
        //判断Redis中是否存在此JWT相关数据
        if (loginInfo == null){
            //放行, 不会向SecurityContext中存入认证信息, 则相当于没有携带JWT
            log.warn("在Redis中无此JWT对应的信息，将直接放行，交由后续的组件进行处理");
            filterChain.doFilter(request, response);
            return;
        }
        //判断此次请求, 与当初登录成功时的相关信息是否相同
        String userAgent = loginInfo.getUserAgent();
        String ip = loginInfo.getIp();
        if (!userAgent.equals(request.getHeader("user-agent"))
        && !ip.equals(request.getRemoteAddr())){
            // 本次请求的信息与当初登录时完全不同，则视为无效的JWT
            log.warn("本次请求的信息与当初登录时完全不同，将直接放行，交由后续的组件进行处理");
            filterChain.doFilter(request, response);
            return;
        }

        //尝试解析JWT数据
        log.debug("尝试解析JWT数据...");
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(jwt)
                    .getBody();
        }catch (ExpiredJwtException e){
            log.warn("解析JWT时出现ExpiredJwtException异常");
            response.setContentType("application/json;charset=utf-8");
            String message = "操作失败, 您的登录信息已过期, 请重新登陆";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_JWT_EXPIRED, message);
            PrintWriter writer = response.getWriter();
            writer.println(JSON.toJSONString(jsonResult));
            writer.close();
        }catch (SignatureException e){
            log.warn("解析JWT时出现SignatureException异常");
            response.setContentType("application/json;charset=utf-8");
            String message = "非法访问, 你的本次操作已经被记录";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_JWT_SIGNATURE, message);
            PrintWriter writer = response.getWriter();
            writer.println(JSON.toJSONString(jsonResult));
            writer.close();
        }catch (MalformedJwtException e){
            log.warn("解析JWT时出现MalformedJwtException异常");
            response.setContentType("application/json;charset=utf-8");
            String message = "非法访问, 你的本次操作已经被记录";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_JWT_MALFORMED, message);
            PrintWriter writer = response.getWriter();
            writer.println(JSON.toJSONString(jsonResult));
            writer.close();
        }catch (Throwable e){
            log.warn("解析JWT时出现异常: {}", e);
            response.setContentType("application/json;charset=utf-8");
            String message = "服务器忙，请稍后再试！【同学们，看到这句时，你应该检查服务器端的控制台，并在JwtAuthorizationFilter中解析JWT时添加处理异常的catch代码块】";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_UNKNOWN, message);
            PrintWriter writer = response.getWriter();
            writer.println(JSON.toJSONString(jsonResult));
            writer.close();
        }

        //从解析结果中获取用户信息
        Long id = claims.get("id", Long.class);
        String username = claims.get("username", String.class);
        String authoritiesJsonString = loginInfo.getAuthoritiesJsonString();
        log.debug("JWT中的用户id = {}", id);
        log.debug("JWT中的用户名 = {}", username);

        Integer userEnable = userCacheRepository.getEnableByUserId(id);
        if (userEnable != 1){
            String message = "用户已被禁用";
            log.debug(message);
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_UNAUTHORIZED_DISABLED, message);
            PrintWriter writer = response.getWriter();
            writer.println(JSON.toJSONString(jsonResult));
            writer.close();
            return;
        }

        //将解析得到的用户数据创建为Authentication对象, 存入到SecurityContext上下文中
        CurrentPrincipal principal = new CurrentPrincipal();
        principal.setId(id);
        principal.setUsername(username);
        Object credentials = null;
        List<SimpleGrantedAuthority> authorities
                = JSON.parseArray(authoritiesJsonString, SimpleGrantedAuthority.class);
        Authentication authentication = new UsernamePasswordAuthenticationToken(principal, credentials, authorities);

        //将Authentication对象存入到SecurityContext上下文中
        //Security框架的SecurityContext上下文默认放在Session会话当中
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);

        // 过滤器链继续执行，即：放行
        filterChain.doFilter(request, response);
    }
//    @Override
//    //初始化配置,只执行一次
//    public void init(FilterConfig filterConfig) throws ServletException {
//        Filter.super.init(filterConfig);
//    }

//    @Override
//    //执行过滤操作
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//    }

//    @Override
//    //销毁的方法,只执行一次
//    public void destroy() {
//        Filter.super.destroy();
//    }
}
