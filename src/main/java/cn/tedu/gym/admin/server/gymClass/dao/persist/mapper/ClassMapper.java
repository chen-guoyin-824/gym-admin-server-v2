package cn.tedu.gym.admin.server.gymClass.dao.persist.mapper;

import cn.tedu.gym.admin.server.gymClass.pojo.entity.GymClass;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface ClassMapper extends BaseMapper<GymClass>{
    /**
     * 查询课程列表
     * @return 课程列表
     */
    List<ClassListItemVO> list(Integer type);

    /**
     * 根据课程id查询课程详情
     * @param id 课程id
     * @return 封装课程信息的对象
     */
    ClassStandardVO getStandardById(Long id);

    /**
     * 根据教练姓名查询课程列表
     * @param coachId 教练id
     * @return 课程列表
     */
    List<ClassListItemVO> listByCoachId(Long coachId);

    /**
     * 通过一周的日期时间查询课程预约列表
     * @param dateList 当日所在星期的日期时间列表
     * @return 课程预约列表
     */
    List<ClassBookTableListItemVO> listByWeekTime(@Param("list") List<LocalDate> dateList, LocalTime dateTime);

    /**
     * 用于系统首页展示经营概况
     * @param classDate 系统当前时间 年月日
     * @return
     */
    List<HomeClassOperateVO> classOperate(LocalDate classDate,Integer type);

    /**
     * 用于系统首页展示课程收入
     * @return
     */
    ClassMoneyCountVO classMoneyCount();

}
