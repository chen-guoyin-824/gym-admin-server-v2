package cn.tedu.gym.admin.server.gymClass.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassMember;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;

public interface MemberBookRepository {
    /**
     * 插入会员预约课程
     * @param classMember 会员预约课程信息
     * @return 影响记录条数
     */
    int insert(ClassMember classMember);

    /**
     * 根据课程ID查询预约详情列表
     * @param bookClassId 课程ID
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 预约详情列表
     */
    PageData<ClassBookDetailListVO> listBookingDetail(Long bookClassId, Integer pageNum, Integer pageSize);

    /**
     * 取消预约
     * @param id 数据ID
     * @return 影响行数
     */
    int cancelById(Long id);

    /**
     * 根据会员预约id修改会员预约信息
     * @param classMember 要进行修改的会员预约信息
     * @return 影响会员预约表的行数
     */
    int update(ClassMember classMember);
}
