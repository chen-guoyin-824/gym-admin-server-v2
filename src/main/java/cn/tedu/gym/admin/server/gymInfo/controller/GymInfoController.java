package cn.tedu.gym.admin.server.gymInfo.controller;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import cn.tedu.gym.admin.server.gymInfo.pojo.param.GymInfoAddParam;
import cn.tedu.gym.admin.server.gymInfo.pojo.param.GymInfoUpdateInfoParam;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoListItemVO;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoStandardVO;
import cn.tedu.gym.admin.server.gymInfo.service.IGymInfoService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 处理场馆相关请求的控制器类
 *
 * @auther xw
 * @create 2023/7/7
 */
@RestController
@Slf4j
@Validated
@RequestMapping("/gymInfo")
@Api(tags = "5.1. 场馆管理模块")
public class GymInfoController {

    @Autowired
    private IGymInfoService gymService;

    public GymInfoController(){
        log.info("创建控制器类对象: GymInfoController");
    }

    @PostMapping("insert")
    @ApiOperation("添加场馆")
    @ApiOperationSupport(order = 100)
    public JsonResult insert(@Valid GymInfoAddParam gymInfoAddParam){
        log.info("controller开始处理[添加场馆]请求,参数:{}",gymInfoAddParam);
        gymService.addNew(gymInfoAddParam);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/delete")
    @ApiOperation("删除场馆")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "场馆ID", required = true, dataType = "long")
    })
    public JsonResult deleteById(@PathVariable @Range(min = 1, message = "请提交有效的文章ID值！") Long id) {
        log.debug("开始处理【根据ID删除文章】的请求，参数：{}", id);
        gymService.deleteById(id);
        return JsonResult.ok();
    }


    @PostMapping("/{id:[0-9]+}/update")
    @ApiOperation("修改场馆")
    @ApiOperationSupport(order = 300)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "场馆ID", required = true, dataType = "long")
    })
    public JsonResult updateInfoById(@PathVariable @Range(min = 1, message = "请提交有效的Id值")Long id,
                                     @Valid GymInfoUpdateInfoParam gymInfoUpdateInfoParam){
        log.debug("开始处理【修改场馆详情】的请求，ID：{}，新数据：{}", id, gymInfoUpdateInfoParam);
        gymService.updateById(id,gymInfoUpdateInfoParam);
        return JsonResult.ok();
    }


    @PostMapping("/{id:[0-9]+}/enable")
    @ApiOperation("启用场馆")
    @ApiOperationSupport(order = 310)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "场馆ID", required = true, dataType = "long")
    })
    public JsonResult setEnable(@PathVariable Long id) {
        log.debug("开始处理【启用场馆】的请求，参数：{}", id);
        gymService.setEnable(id);
        return JsonResult.ok();
    }
    @PostMapping("/{id:[0-9]+}/disable")
    @ApiOperation("禁用场馆")
    @ApiOperationSupport(order = 311)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "场馆ID", required = true, dataType = "long")
    })
    public JsonResult setDisable(@PathVariable Long id) {
        log.debug("开始处理【禁用场馆】的请求，参数：{}", id);
        gymService.setDisable(id);
        return JsonResult.ok();
    }



    @GetMapping("")
    @ApiOperation("查询场馆列表")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "long"),
})
    public JsonResult list(@Range(min = 1, message = "请提交有效的页码值!") Integer page){
        log.debug("开始处理【查询场馆列表】的请求，页码：{}", page);
        Integer pageNum = page == null ? 1 : page;
        PageData<GymInfoListItemVO> pageData = gymService.list(pageNum);
        return JsonResult.ok(pageData);
    }
    @GetMapping("/{id:[0-9]+}")
    @ApiOperation("根据Id查询场馆")
    @ApiOperationSupport(order = 430)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID", defaultValue = "1", paramType = "query", dataType = "Long"),
    })
    public JsonResult getStandardById(@PathVariable Long id){
        log.debug("开始处理【根据Id查询场馆】的请求，id：{}", id);
        GymInfoStandardVO queryResult = gymService.getStandardById(id);
        System.out.println(queryResult);
        return JsonResult.ok(queryResult);
    }
    @GetMapping("selectByAddress")
    @ApiOperation("根据地址查询场馆")
    @ApiOperationSupport(order = 430)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "address", value = "地址", defaultValue = "重庆", paramType = "query", dataType = "String"),
    })
    public JsonResult getStandardByAddress(String address){
        log.debug("开始处理【根据address查询场馆】的请求，address：{}", address);
        List<GymInfoStandardVO> list = gymService.getStandardByAddress(address);
        return JsonResult.ok(list);
    }
}
