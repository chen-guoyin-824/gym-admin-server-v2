package cn.tedu.gym.admin.server.user.dao.persist.repository;

import cn.tedu.gym.admin.server.coach.pojo.entity.Coach;
import cn.tedu.gym.admin.server.user.pojo.entity.User;
import cn.tedu.gym.admin.server.user.pojo.vo.UserLoginInfoVO;
import cn.tedu.gym.admin.server.user.pojo.vo.UserStandardVO;

public interface UserRepository {


    /**
     * 插入新增用户数据
     * @param user
     * @return
     */
    int insert(User user);
    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return 封装的用户信息对象
     */
    UserStandardVO getStandardByUsername(String username);

    /**
     * 根据用户名查询用户的登录信息
     *
     * @param username 用户名
     * @return 匹配的用户的登录信息，如果没有匹配的数据，则返回null
     */
    UserLoginInfoVO getLoginInfoByUsername(String username);

    int deleteById(Long id);

    /**
     * 根据用户手机号查询用户的id信息
     *
     * @param phone 手机号码
     * @return 根据用户手机号查询用户的id信息，如果没有匹配的数据，则返回null
     */
    long getIdByPhone(String phone);

    /**
     * 根据ID修改教练
     * @param user 用户信息
     * @return
     */
    int updateById(User user);
}
