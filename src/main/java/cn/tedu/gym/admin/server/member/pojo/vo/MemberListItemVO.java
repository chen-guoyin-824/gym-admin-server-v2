package cn.tedu.gym.admin.server.member.pojo.vo;

import lombok.Data;

@Data
public class MemberListItemVO {
    private Long id;          // '会员id',
    private Long coachId;          // '教练id',
    private String name;//姓名
    private String coachName;//教练姓名
    private String phone;   //  电话
    private String cartName;    //会员卡等级名称
    private Integer level; //会员卡等级
    private Integer count; //剩余次数
    private Integer status; //会员卡状态


}
