package cn.tedu.gym.admin.server.gymInfo.dao.persist.mapper;

import cn.tedu.gym.admin.server.gymInfo.pojo.entity.GymInfo;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoListItemVO;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @auther xw
 * @create 2023/7/7
 */
@Repository
public interface GymInfoMapper extends BaseMapper<GymInfo> {
    /**
     * 场馆列表
     * @return 场馆数据列表
     */
    List<GymInfoListItemVO> list();

    /**
     * 根据课程id查询课程详情
     * @param id 课程id
     * @return 封装课程信息的对象
     */
    GymInfoStandardVO getStandardById(Long id);

    List<GymInfoStandardVO> getStandardByAddress(String address);
}
