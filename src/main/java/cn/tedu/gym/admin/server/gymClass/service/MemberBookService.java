package cn.tedu.gym.admin.server.gymClass.service;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.pojo.param.MemberBookAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface MemberBookService {

    /**
     * 新增会员预约课程
     */
    void addNew(MemberBookAddNewParam memberBookAddNewParam);

    /**
     * 取消预约
     * @param id 数据ID
     * @return 影响数据的条数
     */
    int cancelById(Long id);

    /**
     * 设置签到状态为已签到
     * @param id 尝试显示的类别的ID
     */
    void setSignedInStatus(Long id);

    /**
     * 根据课程ID查询预约详情列表  使用默认的每页记录数
     * @param bookClassId 预约课程ID
     * @param pageNum 页码
     * @return 用户预约列表
     */
    PageData<ClassBookDetailListVO> listBookingDetail(Long bookClassId, Integer pageNum);
    /**
     * 根据课程ID查询预约详情列表
     * @param bookClassId 预约课程ID
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 用户预约列表
     */
    PageData<ClassBookDetailListVO> listBookingDetail(Long bookClassId, Integer pageNum, Integer pageSize);

}
