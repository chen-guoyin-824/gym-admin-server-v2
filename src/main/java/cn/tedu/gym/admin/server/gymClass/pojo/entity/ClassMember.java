package cn.tedu.gym.admin.server.gymClass.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("gym_book_member")
public class ClassMember implements Serializable {
    /**
     * 数据id
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 预约课程id
     */
    private Long bookClassId;
    /**
     * 预约会员id
     */
    private Long memberId;
    /**
     * 会员签到状态
     */
    private Integer signInStatus;
    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;
    /**
     * 数据最后一次修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;
}
