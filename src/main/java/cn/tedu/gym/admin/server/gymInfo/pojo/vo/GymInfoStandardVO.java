package cn.tedu.gym.admin.server.gymInfo.pojo.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Will 2023/7/11
 */
@Data
public class GymInfoStandardVO {
    private Long id;
    /**
     * 场馆图片
     */
    private String img;
    /**
     * 场馆名称
     */
    private String title;
    /**
     * 电话
     */
    private String phone;
    private String email;
    /**
     * 介绍
     */
    private String introduce;
    /**
     * 地址
     */
    private String address;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否启用  0表示启用,1表示禁用
     */
    private Integer enable;
    /**
     * 数据创建时间
     */
    private LocalDateTime gmtCreate;
}
