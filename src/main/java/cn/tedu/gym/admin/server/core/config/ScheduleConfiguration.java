package cn.tedu.gym.admin.server.core.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author java@tedu.cn
 * @version 1.0
 */
@Slf4j
@Configuration
@EnableScheduling
public class ScheduleConfiguration {
}
