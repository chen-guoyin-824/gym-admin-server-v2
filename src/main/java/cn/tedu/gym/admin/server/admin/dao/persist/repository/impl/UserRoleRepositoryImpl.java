package cn.tedu.gym.admin.server.admin.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.admin.dao.persist.mapper.RoleMapper;
import cn.tedu.gym.admin.server.admin.dao.persist.repository.IUserRoleRepository;
import cn.tedu.gym.admin.server.admin.pojo.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleRepositoryImpl implements IUserRoleRepository {
    @Autowired
    RoleMapper userRoleMapper;
    @Override
    public int insert(UserRole userRole) {
        return userRoleMapper.insert(userRole);
    }
}
