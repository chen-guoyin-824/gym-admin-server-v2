package cn.tedu.gym.admin.server.coach.service;


import cn.tedu.gym.admin.server.coach.pojo.param.CoachAddNewParam;
import cn.tedu.gym.admin.server.coach.pojo.param.CoachUpdateInfoParam;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachListItemVO;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CoachService {
    /*
     */
/**
 * 添加教练信息
 * @param  */


    void addNew(CoachAddNewParam coachAddNewParam);

    /**
     * 查询课程列表
     * @param pageNum
     * @return
     */
    PageData<CoachListItemVO> list(Integer pageNum);

    /**
     * 查询课程列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageData<CoachListItemVO> list(Integer pageNum, Integer pageSize);

    /**
     * 根据id查询课程详情
     * @param id 课程id
     * @return 封装课程详情的对象
     */


    CoachStandardVO getStandardById(Long id);

    /**
     * 预约记录列表 使用默认的每页记录数
     * @param pageNum 页码
     * @return 课程预约列表
     *//*
    PageData<ClassBookListItemVO> listBooking(Integer pageNum);

    *//**
     * 预约记录列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 课程预约列表
     *//*
    PageData<ClassBookListItemVO> listBooking(Integer pageNum, Integer pageSize);*/

    /**
     * 根据ID删除教练
     * @param id 教练ID
     * @return
     */
    int deleteById(Long id);

    void updateInfoById(CoachUpdateInfoParam coachUpdateInfoParam);
}