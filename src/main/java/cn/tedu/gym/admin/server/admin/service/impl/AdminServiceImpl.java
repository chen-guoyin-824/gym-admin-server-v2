package cn.tedu.gym.admin.server.admin.service.impl;

import cn.tedu.gym.admin.server.admin.dao.persist.repository.IAdminRepository;
import cn.tedu.gym.admin.server.admin.dao.persist.repository.IUserRoleRepository;
import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.admin.pojo.entity.UserRole;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminAddNewParam;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminUpdateInfoParam;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserListItemVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserStandardVO;
import cn.tedu.gym.admin.server.admin.service.IAdminService;
import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AdminServiceImpl implements IAdminService {
    @Value("${gym.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    IAdminRepository adminRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    IUserRoleRepository userRoleRepository;

    public AdminServiceImpl() {
        log.debug("创建业务类对象:AdminServiceImpl");
    }

    @Override
    public UserListItemVO adminInfoById(Long id) {
        log.debug("开始处理[查询管理员信息]的业务,管理员id:{}",id);
        UserListItemVO adminInfo = adminRepository.adminInfoById(id);
        if (adminInfo==null){
            String message="操作失败,该用户不存在!";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,message);
        }
        return adminInfo;
    }

    @Override
    public int updateAdminInfoById(AdminUpdateInfoParam adminUpdateInfoParam) {
        log.debug("开始处理[修改管理员信息]的业务,参数:{}",adminUpdateInfoParam);
        Long id = adminUpdateInfoParam.getId();
        UserStandardVO userStandardVO = adminRepository.adminDetailById(id);
        if (userStandardVO==null){
            String message="操作失败,当前用户不存在!";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,message);
        }

        String oldPassword = adminUpdateInfoParam.getPassword();
        if (!passwordEncoder.matches(oldPassword,userStandardVO.getPassword())){
            String message="操作失败,请输入正确的旧密码!";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_BAD_PASSWORD,message);
        }
        User user=new User();
        BeanUtils.copyProperties(adminUpdateInfoParam,user);
        user.setPassword(passwordEncoder.encode(adminUpdateInfoParam.getNewPassword()));

        return adminRepository.updateAdminInfoById(user);
    }

    @Override
    public int updateAvatar(Long id,String imgUrl) {
        log.debug("开始处理[修改管理员头像]的业务,头像路径:{}",imgUrl);
        User user=new User();
        user.setId(id);
        user.setAvatar(imgUrl);
        return adminRepository.updateAdminInfoById(user);
    }

    @Override
    public void addNewAdmin(AdminAddNewParam adminAddNewParam) {
        log.debug("开始处理[新增管理员]的业务,参数:{}",adminAddNewParam);
        String username=adminAddNewParam.getUsername();
        int rows = adminRepository.selectCount(username);
        if (rows>0){
            String message="操作失败,用户名已存在,请重新输入用户名!";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,message);
        }
        String phone=adminAddNewParam.getPhone();
        rows=adminRepository.selectCountByPhone(phone);
        if (rows>0){
            String message="操作失败,手机号已被注册,请重新输入手机号!";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,message);
        }
        User user=new User();
        BeanUtils.copyProperties(adminAddNewParam,user);
        String password = adminAddNewParam.getPassword();
        password = passwordEncoder.encode(password);
        user.setEnable(0);
        user.setSort(99);
        user.setPassword(password);
        adminRepository.addNewAdmin(user);

        //为管理员添加角色
        UserRole userRole=new UserRole();
        userRole.setRoleId(adminAddNewParam.getRoleId());
        userRole.setUserId(user.getId());
        userRoleRepository.insert(userRole);

    }

    @Override
    public PageData<UserDetailListVO> adminDetailList(Integer pageNum) {
        log.debug("开始处理[查询管理员列表]的业务,页码:{}",pageNum);
        return adminRepository.adminDetailList(pageNum, defaultQueryPageSize);
    }

    @Override
    public PageData<UserDetailListVO> adminDetailList(Integer pageNum, Integer pageSize) {
        log.debug("开始处理[查询管理员列表]的业务,页码:{},每页记录数:{}",pageNum,pageSize);
        return adminRepository.adminDetailList(pageNum,pageSize);
    }

    @Override
    public void deleteById(Long id) {
        log.debug("开始处理[根据ID删除管理员]的业务,ID:{}",id);
        UserStandardVO userStandardVO = adminRepository.adminDetailById(id);
        if (userStandardVO==null){
            String message="操作失败,该管理员不存在!";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,message);
        }
         adminRepository.deleteById(id);
    }

    @Override
    public void setEnable(Long id) {
        updateEnableById(id, 0);
    }

    @Override
    public void setDisable(Long id) {
        updateEnableById(id, 1);
    }

    private void updateEnableById(Long id, Integer enable) {
        log.debug("开始处理【{}用户】的业务，ID：{}，目标状态：{}", ENABLE_TEXT[enable], id, enable);
        UserStandardVO queryResult = adminRepository.adminDetailById(id);
        if (queryResult == null) {
            String message = ENABLE_TEXT[enable] + "用户失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        if (queryResult.getEnable().equals(enable)) {
            String message = ENABLE_TEXT[enable] + "用户失败，当前用户已经处于"
                    + ENABLE_TEXT[enable] + "状态！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        User user = new User();
        user.setId(id);
        user.setEnable(enable);
        log.debug("即将修改数据，参数：{}", user);
        int rows = adminRepository.updateById(user);
        if (rows != 1) {
            String message = ENABLE_TEXT[enable] + "用户失败，服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }

        // 将用户状态同步写入到Redis中
        /*Integer enableByUserId = userCacheRepository.getEnableByUserId(id);
        if (enableByUserId != null) {
            userCacheRepository.saveEnableByUserId(id, enable);
        }*/
    }
}
