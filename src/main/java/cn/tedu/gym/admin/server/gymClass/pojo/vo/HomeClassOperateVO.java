package cn.tedu.gym.admin.server.gymClass.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 该类用于系统首页展示经营概况
 */
@Data
public class HomeClassOperateVO implements Serializable {
    private LocalDate classData;
    private String time;
    private String className;
    private String name;
}
