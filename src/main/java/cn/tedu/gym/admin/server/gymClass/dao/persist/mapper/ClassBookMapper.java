package cn.tedu.gym.admin.server.gymClass.dao.persist.mapper;

import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassBook;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface ClassBookMapper extends BaseMapper<ClassBook> {
    /**
     * 预约记录列表
     * @return 课程预约记录列表
     */
    List<ClassBookListItemVO> listBooking();

    /**
     * 根据预约ID查询详情
     * @param id 数据ID
     * @return 封装了预约课程详情的对象
     */
    ClassBookStartedVO selectById(Long id);

    /**
     * 通过上课日期和时间查询预约课程信息
     * @param date 上课日期
     * @param time 上课时间
     * @return 预约课程信息对象
     */
    ClassBookStartedVO getStandardByDateTime(LocalDate date, LocalTime time);

    /**
     * 根据课程ID和教练ID查询列表
     * @param classId 课程ID
     * @param coachId 教练ID
     * @return 预约列表
     */
    List<ClassBookListItemVO>listByClassIdANDCoachId(Long classId, Long coachId);
}
