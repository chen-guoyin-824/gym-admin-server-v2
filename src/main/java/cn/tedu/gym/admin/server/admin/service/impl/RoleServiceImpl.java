package cn.tedu.gym.admin.server.admin.service.impl;

import cn.tedu.gym.admin.server.admin.dao.persist.repository.IRoleRepository;
import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;
import cn.tedu.gym.admin.server.admin.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    IRoleRepository roleRepository;
    @Override
    public List<RoleListItemVO> list() {
        return roleRepository.list();
    }
}
