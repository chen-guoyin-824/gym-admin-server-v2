package cn.tedu.gym.admin.server.common.security;

import lombok.Data;

import java.io.Serializable;

/**
 * 当事人数据
 */
@Data
public class CurrentPrincipal implements Serializable {
    /**
     * 当事人ID
     */
    private Long id;
    /**
     * 当事人用户名
     */
    private String username;
}
