package cn.tedu.gym.admin.server.admin.dao.persist.repository;

import cn.tedu.gym.admin.server.admin.pojo.entity.UserRole;

public interface IUserRoleRepository {
    /**
     * 新增用户角色管理数据 用于新增管理员
     * @param userRole
     * @return
     */
    int insert(UserRole userRole);
}
