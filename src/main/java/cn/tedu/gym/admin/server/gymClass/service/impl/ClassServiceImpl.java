package cn.tedu.gym.admin.server.gymClass.service.impl;

import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassBookRepository;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.GymClass;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassUpdateInfoParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.*;
import cn.tedu.gym.admin.server.gymClass.service.ClassService;
import cn.tedu.gym.admin.server.user.dao.persist.repository.UserRepository;
import cn.tedu.gym.admin.server.user.pojo.vo.UserStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ClassServiceImpl implements ClassService {

    @Value("${gym.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;

    @Autowired
    ClassRepository classRepository;
    @Autowired
    ClassBookRepository classBookRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public void addNew(ClassAddNewParam classAddNewParam) {
        log.debug("开始处理[添加课程]的业务, 参数: {}", classAddNewParam);
        //TODO 检查课程名称是否重复
        GymClass gymClass = new GymClass();
        BeanUtils.copyProperties(classAddNewParam, gymClass);
        String coachName = classAddNewParam.getCoachName();
        UserStandardVO standardByUsername = userRepository.getStandardByUsername(coachName);
        Long coachId = standardByUsername.getId();
        gymClass.setCoachId(coachId);
        classRepository.insert(gymClass);
    }

    @Override
    public void deleteById(Long id) {
        log.debug("开始处理[根据课程id删除课程]的业务, 参数: {}", id);
        //TODO 判断该课程信息是否存在
        int rows = classRepository.deleteById(id);
        if (rows != 1){
            String message = "删除课程失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_DELETE, message);
        }
    }

    @Override
    public void updateById(Long id, ClassUpdateInfoParam classUpdateInfoParam) {
        log.debug("开始处理[根据课程id修改课程信息]的业务, 参数: id: {}, 修改信息: {}", id, classUpdateInfoParam);
        //TODO 查询id所对应的课程是否存在
        GymClass gymClass = new GymClass();
        BeanUtils.copyProperties(classUpdateInfoParam, gymClass);
        gymClass.setId(id);
        String coachName = classUpdateInfoParam.getCoachName();
        UserStandardVO standardByUsername = userRepository.getStandardByUsername(coachName);
        gymClass.setCoachId(standardByUsername.getId());
        int rows = classRepository.updateById(gymClass);
        if (rows != 1){
            String message = "修改课程失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }
    }

    @Override
    public List<ClassListItemVO> listByCoachId(Long coachId) {
        log.debug("开始处理[根据教练id查询课程列表]的业务, 参数: {}", coachId);
        List<ClassListItemVO> list = classRepository.listByCoachId(coachId);
        return list;
    }

    @Override
    public PageData<ClassListItemVO> list(Integer pageNum, Integer type) {
        log.debug("开始处理[查询课程列表]的业务, 参数: ✌✌页码: {}", pageNum);
        PageData<ClassListItemVO> pageData = classRepository.list(pageNum, defaultQueryPageSize, type);
        return pageData;
    }

    @Override
    public PageData<ClassListItemVO> list(Integer pageNum, Integer pageSize, Integer type) {
        log.debug("开始处理[查询课程列表]的业务, 参数: ✌✌页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageData<ClassListItemVO> pageData = classRepository.list(pageNum, pageSize, type);
        return pageData;
    }

    @Override
    public ClassStandardVO getStandardById(Long id) {
        log.debug("开始处理[根据id查询课程详情]的业务, 参数: {}", id);
        ClassStandardVO standardById = classRepository.getStandardById(id);
        if (standardById == null){
            String message = "查询详情失败, 查询id不存在";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return standardById;
    }

    @Override
    public List<ClassBookTableListItemVO> listByWeekTime(LocalDate localDate, LocalTime dateTime) {
        log.debug("开始处理[根据具体时间和日期查询课表数据]的业务, 参数: {}, {}", localDate, dateTime);

        // 获取当前周的第一天（即当前日期所在周的周一）
        LocalDate firstDayOfWeek = localDate.with(DayOfWeek.MONDAY);

        // 获取当前周的全部日期
        List<LocalDate> allDatesOfWeek = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            LocalDate date = firstDayOfWeek.plusDays(i);
            allDatesOfWeek.add(date);
        }

        // 格式化输出日期z
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (LocalDate date : allDatesOfWeek) {
            String formattedDate = date.format(formatter);
            System.out.println(formattedDate);
        }
        List<ClassBookTableListItemVO> classBookTableList = classRepository.listByWeekTime(allDatesOfWeek, dateTime);
        List<ClassBookTableListItemVO> classAllBookTableList = new ArrayList<>();
        for (LocalDate date : allDatesOfWeek) {
            int count = 0;
            for (ClassBookTableListItemVO classBookTable : classBookTableList) {
                if (date.equals(classBookTable.getClassDate())){
                    classAllBookTableList.add(classBookTable);
                    count++;
                }
            }
            if(count==0){
                ClassBookTableListItemVO classBookTableListItemVO = new ClassBookTableListItemVO();
                classBookTableListItemVO.setClassDate(date);
                classAllBookTableList.add(classBookTableListItemVO);
            }
        }
        return classAllBookTableList;
    }

    @Override
    public List<HomeClassOperateVO> classOperate(LocalDate classDate,Integer type) {
        log.debug("开始执行[用于系统首页展示经营概况的团课查询], 参数: {}", classDate);
        return classRepository.classOperate(classDate,type);
    }

    @Override
    public ClassMoneyCountVO classMoneyCount() {
        log.debug("开始执行[用于系统首页展示课程收入]");
        return classRepository.classMoneyCount();
    }
}
