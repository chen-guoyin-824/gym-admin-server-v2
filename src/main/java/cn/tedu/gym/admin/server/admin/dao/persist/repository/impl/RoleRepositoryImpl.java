package cn.tedu.gym.admin.server.admin.dao.persist.repository.impl;

import cn.tedu.gym.admin.server.admin.dao.persist.mapper.RoleMapper;
import cn.tedu.gym.admin.server.admin.dao.persist.repository.IRoleRepository;
import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements IRoleRepository {
    @Autowired
    RoleMapper roleMapper;
    @Override
    public List<RoleListItemVO> list() {
        return roleMapper.list();
    }
}
