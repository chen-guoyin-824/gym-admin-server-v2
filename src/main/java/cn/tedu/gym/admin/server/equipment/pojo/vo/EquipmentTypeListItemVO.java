package cn.tedu.gym.admin.server.equipment.pojo.vo;

import lombok.Data;

import java.io.Serializable;
@Data
public class EquipmentTypeListItemVO implements Serializable {
    /**
     * 器材类别id
     */
    private Long id;
    /**
     * 器材类别名称
     */
    private String typeName;

}
