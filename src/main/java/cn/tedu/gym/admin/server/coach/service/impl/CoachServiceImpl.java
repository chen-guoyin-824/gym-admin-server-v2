package cn.tedu.gym.admin.server.coach.service.impl;


import cn.tedu.gym.admin.server.coach.dao.persist.repository.CoachRepository;
import cn.tedu.gym.admin.server.coach.pojo.entity.Coach;
import cn.tedu.gym.admin.server.coach.pojo.param.CoachAddNewParam;
import cn.tedu.gym.admin.server.coach.pojo.param.CoachUpdateInfoParam;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachListItemVO;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachStandardVO;
import cn.tedu.gym.admin.server.coach.service.CoachService;
import cn.tedu.gym.admin.server.common.ex.ServiceException;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.web.ServiceCode;
import cn.tedu.gym.admin.server.user.dao.persist.repository.UserRepository;
import cn.tedu.gym.admin.server.user.pojo.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;


@Slf4j
@Service
public class CoachServiceImpl implements CoachService {

    @Value("${gym.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;

    @Autowired
    CoachRepository coachRepository;
    @Autowired
    UserRepository userRepository;



  /*  @Override
    public void addNew(ClassAddNewParam classAddNewParam) {
        log.debug("开始处理[添加课程]的业务, 参数: {}", classAddNewParam);
        //TODO 检查课程名称是否重复
        GymClass gymClass = new GymClass();
        BeanUtils.copyProperties(classAddNewParam, gymClass);
        String coachName = classAddNewParam.getCoachName();
        UserStandardVO standardByUsername = userRepository.getStandardByUsername(coachName);
        Long coachId = standardByUsername.getId();
        gymClass.setCoachId(coachId);
        classRepository.insert(gymClass);
    }*/

    @Override
    public void addNew(CoachAddNewParam coachAddNewParam) {
        log.debug("开始处理[添加教练列表]的业务, 参数: {}", coachAddNewParam);
        User user=new User();
        BeanUtils.copyProperties(coachAddNewParam, user);
        log.debug("user, 参数: {}", user);
        userRepository.insert(user);
        long id = userRepository.getIdByPhone(user.getPhone());
        Coach coach = new Coach();
        BeanUtils.copyProperties(coachAddNewParam, coach);
        coach.setUserId(id);
        if (coachAddNewParam.getPhotoList() != null){
            String photo = (String) coachAddNewParam.getPhotoList().stream()
                    .map(n -> String.valueOf(n))
                    .collect(Collectors.joining(","));
            coach.setPhoto(photo);
        }
        log.debug("coach, 参数: {}", coach);
        coachRepository.insert(coach);
    }

    @Override
    public PageData<CoachListItemVO> list(Integer pageNum) {
        log.debug("开始处理[查询教练列表]的业务, 参数: ✌✌页码: {}", pageNum);
        PageData<CoachListItemVO> pageData = coachRepository.list(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public PageData<CoachListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始处理[查询教练列表]的业务, 参数: ✌✌页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageData<CoachListItemVO> pageData = coachRepository.list(pageNum, pageSize);
        return pageData;
    }

    @Override
    public CoachStandardVO getStandardById(Long id) {
        log.debug("开始处理[根据id查询教练详情]的业务, 参数: {}", id);
        CoachStandardVO standardById = coachRepository.getStandardById(id);
        if (standardById == null){
            String message = "查询详情失败, 查询id不存在";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return standardById;
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始处理[根据id删除教练]的业务, 参数: {}", id);
        CoachStandardVO standardById = coachRepository.getStandardById(id);
        if (standardById == null){
            String message = "查询详情失败, 该教练不存在";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        userRepository.deleteById(standardById.getUserId());
        return coachRepository.deleteById(id);
    }

    @Override
    public void updateInfoById(CoachUpdateInfoParam coachUpdateInfoParam) {
        log.debug("开始处理【修改教练详情】的业务，参数：{}", coachUpdateInfoParam);

        Long id = coachUpdateInfoParam.getId();
        CoachStandardVO currentCoach = coachRepository.getStandardById(id);
        if (currentCoach == null) {
            String message = "修改标签失败，尝试修改的标签数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        Coach coach = new Coach();
        BeanUtils.copyProperties(coachUpdateInfoParam, coach);
        int rows = coachRepository.updateById(coach);
        User user = new User();
        user.setId(coachUpdateInfoParam.getUserId());
        user.setSort(coachUpdateInfoParam.getSort());
        user.setPhone(coachUpdateInfoParam.getPhone());
        int rows1 = userRepository.updateById(user);
        if (rows != 1 || rows1 != 1) {
            String message = "修改教练信息失败，服务器忙，请稍后再试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }
    }
/*
    @Override
    public PageData<ClassBookListItemVO> listBooking(Integer pageNum) {
        log.debug("开始处理[查询预约记录列表]的业务, 参数: 页码: {}", pageNum);
        PageData<ClassBookListItemVO> pageData = classRepository.listBooking(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public PageData<ClassBookListItemVO> listBooking(Integer pageNum, Integer pageSize) {
        log.debug("开始处理[查询预约记录列表]的业务, 参数: 页码: {}, 每页记录数: {}", pageNum, pageSize);
        PageData<ClassBookListItemVO> pageData = classRepository.listBooking(pageNum, pageSize);
        return pageData;
    }*/
}