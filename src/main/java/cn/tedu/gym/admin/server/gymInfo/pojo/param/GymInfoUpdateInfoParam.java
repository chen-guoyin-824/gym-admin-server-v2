package cn.tedu.gym.admin.server.gymInfo.pojo.param;

import lombok.Data;

/**
 * @author Will 2023/7/10
 */
@Data
public class GymInfoUpdateInfoParam {
    /**
     * 场馆图片
     */
    private String img;
    /**
     * 场馆名称
     */
    private String title;
    /**
     * 电话
     */
    private String phone;
    private String email;
    /**
     * 介绍
     */
    private String introduce;
    /**
     * 地址
     */
    private String address;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否启用  0表示启用,1表示禁用
     */
    private Integer enable;
}
