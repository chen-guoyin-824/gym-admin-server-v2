package cn.tedu.gym.admin.server.equipment.controller;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.equipment.pojo.param.EquipmentAddNewParam;
import cn.tedu.gym.admin.server.equipment.pojo.param.EquipmentUpdateParam;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentTypeListVO;
import cn.tedu.gym.admin.server.equipment.service.IEquipmentService;
import cn.tedu.gym.admin.server.common.security.CurrentPrincipal;
import cn.tedu.gym.admin.server.common.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 处理器材相关请求的控制器类
 *
 * @author java@tedu.cn
 * @version 1.0
 */
@Slf4j
@RestController
@RequestMapping("equipment/")
@Api(tags = "3.2. 器材管理")
public class EquipmentController {

    @Autowired
    IEquipmentService equipmentService;

    @GetMapping("/list")
    @ApiOperation("查询器材列表")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", defaultValue = "1", paramType = "query", dataType = "int")
    })
    public JsonResult list(Integer pageNum){
        PageData<EquipmentListItemVO> pageData = equipmentService.list(pageNum);
        return JsonResult.ok(pageData);
    }
    @GetMapping("/type-list")
    @ApiOperation("查询器材类别列表")
    @ApiOperationSupport(order = 410)
    public JsonResult typeList( ){
        List<EquipmentTypeListVO> list = equipmentService.typeList();
        return JsonResult.ok(list);
    }

    @PostMapping("/deleteById")
    @ApiOperation("根据ID删除器材")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "器材ID", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult deleteById(Long id){
        log.debug("开始处理[根据ID删除器材]的请求,ID:{}",id);
        equipmentService.deleteById(id);
        return JsonResult.ok();
    }

    @PostMapping("/insert")
    @ApiOperation("添加器材")
    @ApiOperationSupport(order = 100)
    public JsonResult insert(EquipmentAddNewParam equipmentAddNewParam){
        log.debug("开始处理[添加器材]的请求,参数:{}",equipmentAddNewParam);
        equipmentService.insert(equipmentAddNewParam);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/enable")
    @ApiOperation("启用器材")
    @ApiOperationSupport(order = 310)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "器材ID", required = true, dataType = "long")
    })
    public JsonResult setEnable(@PathVariable Long id) {
        log.debug("开始处理【启用器材】的请求，参数：{}", id);
        equipmentService.setEnable(id);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/disable")
    @ApiOperation("禁用器材")
    @ApiOperationSupport(order = 311)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "器材ID", required = true, dataType = "long")
    })
    public JsonResult setDisable(@PathVariable Long id) {
        log.debug("开始处理【禁用器材】的请求，参数：{}", id);
        equipmentService.setDisable(id);
        return JsonResult.ok();
    }
    @PostMapping("/update")
    @ApiOperation("编辑器材")
    @ApiOperationSupport(order = 300)
    public JsonResult update(EquipmentUpdateParam equipmentUpdateParam){
        log.debug("开始处理[编辑器材]的请求,参数:{}",equipmentUpdateParam);
        equipmentService.updateById(equipmentUpdateParam);
        return JsonResult.ok();
    }
}
