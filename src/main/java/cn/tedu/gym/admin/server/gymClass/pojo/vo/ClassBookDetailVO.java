package cn.tedu.gym.admin.server.gymClass.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class ClassBookDetailVO implements Serializable {
    /**
     * 课程名称
     */
    private String className;
    /**
     * 课程图片
     */
    private String image;
    /**
     * 上课日期
     */
    private LocalDate classDate;
    /**
     * 上课时间
     */
    private LocalTime classTime;
    /**
     * 教练名字
     */
    private String coachName;
    /**
     * 课程时长
     */
    private Integer time;
    /**
     * 预约人数
     */
    private Integer bookNumber;

}
