package cn.tedu.gym.admin.server.gymClass.service;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.pojo.param.ClassBookAddNewParam;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;

public interface ClassBookService {

    /**
     * 新增预约课程排期
     * @param classBookAddNewParam 前端传入的预约课程信息
     */
    void insertBookClass(ClassBookAddNewParam classBookAddNewParam);

    /**
     * 预约记录列表 使用默认的每页记录数
     * @param pageNum 页码
     * @return 课程预约列表
     */
    PageData<ClassBookListItemVO> listBooking(Integer pageNum);

    /**
     * 预约记录列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 课程预约列表
     */
    PageData<ClassBookListItemVO> listBooking(Integer pageNum, Integer pageSize);

    /**
     * 根据课程ID和教练ID查询列表
     * @param classId 课程ID
     * @param coachId 教练ID
     * @return 预约课程列表
     */
    PageData<ClassBookListItemVO> listByClassIdANDCoachId(Long classId, Long coachId, Integer pageNum, Integer pageSize);

    PageData<ClassBookListItemVO> listByClassIdANDCoachId(Long classId, Long coachId, Integer pageNum);

    /**
     * 根据预约id查询预约详情
     * @param id 预约课程id
     * @return 预约详情
     */
    ClassBookStartedVO getBookStandardById(Long id);
}
