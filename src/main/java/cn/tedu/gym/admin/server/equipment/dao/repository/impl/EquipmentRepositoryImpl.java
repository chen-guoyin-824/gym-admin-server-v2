package cn.tedu.gym.admin.server.equipment.dao.repository.impl;

import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.equipment.dao.mapper.EquipmentMapper;
import cn.tedu.gym.admin.server.equipment.dao.repository.IEquipmentRepository;
import cn.tedu.gym.admin.server.equipment.pojo.entity.Equipment;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.common.util.PageInfoToPageDataConverter;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentTypeListVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理器材数据的存储库实现类
 *
 */
@Slf4j
@Repository
public class EquipmentRepositoryImpl implements IEquipmentRepository {

    @Autowired
   EquipmentMapper equipmentMapper;

    @Override
    public PageData<EquipmentListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始处理查询器材列表的数据请求,页码:{},每页记录数:{}",pageNum,pageSize);
        PageHelper.startPage(pageNum,pageSize);
        List<EquipmentListItemVO> list = equipmentMapper.list();
        PageInfo<EquipmentListItemVO> pageInfo=new PageInfo<>(list);
        PageData<EquipmentListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;

    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始处理根据ID删除器材的数据请求,ID:{}",id);
        return equipmentMapper.deleteById(id);
    }

    @Override
    public EquipmentStandardVO getStandardVOById(Long id) {
        log.debug("开始处理根据ID查询器材的数据请求,ID:{}",id);
        return equipmentMapper.getStandardVOById(id);
    }

    @Override
    public int insert(Equipment equipment) {
        log.debug("开始处理新增器材的数据请求,参数:{}",equipment);
        return equipmentMapper.insert(equipment);
    }

    @Override
    public List<EquipmentTypeListVO> typeList() {
        return equipmentMapper.typeList();
    }

    @Override
    public int updateById(Equipment equipment) {
        log.debug("开始执行【更新器材】的数据访问，参数：{}", equipment);
        return equipmentMapper.updateById(equipment);
    }
}
