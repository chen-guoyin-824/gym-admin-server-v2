package cn.tedu.gym.admin.server.equipment.service;

import cn.tedu.gym.admin.server.equipment.pojo.entity.Equipment;
import cn.tedu.gym.admin.server.equipment.pojo.param.EquipmentAddNewParam;
import cn.tedu.gym.admin.server.equipment.pojo.param.EquipmentUpdateParam;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentTypeListVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 处理器材数据的业务接口
 *
 */
@Transactional
public interface IEquipmentService {
    /**
     * 启用状态的显示文本
     */
    String ENABLE_TEXT[] = {"启用","禁用" };
    /**
     * 查询器材列表
     * @param pageNum 页码
     * @return
     */
    PageData<EquipmentListItemVO> list(Integer pageNum);
    /**
     * 查询器材列表
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return
     */
    PageData<EquipmentListItemVO> list(Integer pageNum, Integer pageSize);
    /**
     * 根据ID删除器材
     * @param id 器材ID
     * @return
     */
    void deleteById(Long id);

    /**
     * 新增器材
     * @param equipmentAddNewParam 封装了新增器材信息的类
     * @return
     */
    int insert(EquipmentAddNewParam equipmentAddNewParam);

    /**
     * 查询器材类别列表
     * @return
     */
    List<EquipmentTypeListVO> typeList();
    /**
     * 启用器材
     *
     * @param id 器材ID
     */
    void setEnable(Long id);

    /**
     * 禁用器材
     *
     * @param id 器材ID
     */
    void setDisable(Long id);
    /**
     * 根据器材id修改器材的数据
     * @param equipmentUpdateParam 封装了器材id和新的数据的对象
     * @return 受影响的行数
     */
    void updateById(EquipmentUpdateParam equipmentUpdateParam);
}
