package cn.tedu.gym.admin.server.coach.pojo.vo;

import lombok.Data;

@Data
public class CoachStandardVO {
    /**
     * 教练ID
     */
    private Long id;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 姓名
     */
    private String name;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 场馆地址
     */
    private String address;
    /**
     * 个人签名
     */
    private String target;
    /**
     * 个人简介
     */
    private String profile;
    /**
     * 从业时间
     */
    private String worktime;
    /**
     * 擅长标签
     */
    private String skills;
    /**
     * 每日私教课上限
     */
    private Integer upperLimit;
    /**
     * 教练相册
     */
    private String photo;
    /**
     * 性别
     */
    private String sex;

}
