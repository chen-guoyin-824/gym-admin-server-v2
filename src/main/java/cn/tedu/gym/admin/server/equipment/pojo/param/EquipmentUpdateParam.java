package cn.tedu.gym.admin.server.equipment.pojo.param;


import lombok.Data;

import java.io.Serializable;

@Data
public class EquipmentUpdateParam implements Serializable {
    /**
     * 器材id
     */
    private Long id;
    /**
     * 器材名称
     */
    private String name;

    /**
     * 器材图片
     */
    private String img;

    /**
     * 器材状态
     */
    private Integer state;

    /**
     * 备注信息
     */
    private String remark;

}
