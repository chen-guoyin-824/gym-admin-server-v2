package cn.tedu.gym.admin.server.admin.pojo.param;

import lombok.Data;
import springfox.documentation.service.ApiListing;

import java.io.Serializable;
@Data

public class AdminUpdateInfoParam  implements Serializable {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 排序序号
     */
    private Integer sort;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 密码
     */
    private String password;
    /**
     * 新密码
     */
    private String newPassword;


}
