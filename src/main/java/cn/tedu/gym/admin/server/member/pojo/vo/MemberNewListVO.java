package cn.tedu.gym.admin.server.member.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 该类用于首页展示最新办卡信息
 */
@Data
public class MemberNewListVO implements Serializable {
    private String name;
    private String cardName;
    private LocalDateTime gmtCreate;
    private String time;
}
