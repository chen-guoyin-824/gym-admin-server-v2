package cn.tedu.gym.admin.server.user.dao.persist.mapper;

import cn.tedu.gym.admin.server.equipment.dao.mapper.EquipmentMapper;
import cn.tedu.gym.admin.server.equipment.pojo.entity.Equipment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class EquipmentMapperTests {

    @Autowired
    private EquipmentMapper equipmentMapper;

    @Test
    void insert() {
        Equipment equipment = new Equipment();
        equipment.setName("器材");
        System.out.println("插入数据之前，参数：" + equipment);
        System.out.println("插入数据之后，参数：" + equipment);
    }
}
