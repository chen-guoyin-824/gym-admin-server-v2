package cn.tedu.gym.admin.server.user.dao.persist.mapper;

import cn.tedu.gym.admin.server.user.pojo.vo.UserStandardVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTests {
    @Autowired
    UserMapper mapper;
    
    @Test
    void getStandard(){
        String username = "zhangsan";
        UserStandardVO standardUser = mapper.getStandardByUsername(username);
        System.out.println(standardUser);
    }
}
