package cn.tedu.gym.admin.server.member.mapper;

import cn.tedu.gym.admin.server.coach.dao.persist.repository.CoachRepository;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachStandardVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.member.dao.persist.mapper.MemberCartMapper;
import cn.tedu.gym.admin.server.member.dao.persist.mapper.MemberMapper;
import cn.tedu.gym.admin.server.member.dao.persist.repository.MemberRepository;
import cn.tedu.gym.admin.server.member.pojo.entity.GymMemberCart;
import cn.tedu.gym.admin.server.member.pojo.param.GymMemberCartAddNewParam;
import cn.tedu.gym.admin.server.member.pojo.vo.HomeMemberVO;
import cn.tedu.gym.admin.server.member.pojo.vo.CartStandardVO;
import cn.tedu.gym.admin.server.member.pojo.vo.MemberListItemVO;
import cn.tedu.gym.admin.server.member.pojo.vo.MemberNewListVO;
import cn.tedu.gym.admin.server.member.service.MemberCartService;
import cn.tedu.gym.admin.server.member.service.MemberService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class test {
    @Autowired
    MemberMapper mapper;
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    MemberCartMapper mapper1;
    @Autowired
    MemberCartService service;
    @Autowired
    CoachRepository coachRepository;
    @Autowired
    MemberService memberService;
    @Autowired
    MemberCartMapper memberCartMapper;
    @Test
    void testCard (){
        List<CartStandardVO> info = memberCartMapper.getInfoById(1L);
        System.out.println(info);

    }
    @Test
    void test(){
        List<MemberListItemVO> memberListItemVOS = mapper.list1();
        System.out.println(memberListItemVOS);
    }
    @Test
    void test1(){
        GymMemberCart standardById = service.getStandardById(1L);
        System.out.println(standardById);
    }
    //(5, 5, 2, '普通会员',15,1,'2023-06-01 08:00:00', '2023-06-01 08:00:00')
//    @Test
//    void test2(){
//        GymMemberCart gymMemberCart = new GymMemberCart();
//        gymMemberCart.setCount(10);
//        gymMemberCart.setId(10L);
//        gymMemberCart.setMemberId(3L);
//        gymMemberCart.setMemberLevel(1);
//        gymMemberCart.setName("普通会员");
//        gymMemberCart.setStatus(1);
//        service.updateCountById(gymMemberCart);
//        System.out.println(gymMemberCart);
//    }
    @Test
    void test3(){

        GymMemberCartAddNewParam gymMemberCartAddNewParam = new GymMemberCartAddNewParam();
        service.insert(gymMemberCartAddNewParam);
        System.out.println(gymMemberCartAddNewParam);
    }

    @Test
    void rest4(){
        MemberListItemVO listById = mapper.getListById(1L);
        System.out.println(listById);

    }

    @Test
    void rest5(){
        CoachStandardVO standardById = coachRepository.getStandardById(1L);
        System.out.println(standardById);
    }

    @Test
    void rest6(){
        PageData<MemberListItemVO> pageData = memberRepository.list1(1, 5);
        List<MemberListItemVO> list = pageData.getList();
        List<MemberListItemVO> list1 = new ArrayList<>();
        for (MemberListItemVO memberListItemVO : list) {
            Long coachId = memberListItemVO.getCoachId();
            CoachStandardVO standardById = coachRepository.getStandardById(coachId);
            String name = standardById.getName();
            System.out.println("zbxbx钟毕鑫钟毕鑫："+name);
            memberListItemVO.setCoachName(name);
            list1.add(memberListItemVO);
        }
        System.out.println("---------------------");
        System.out.println(list1);
        pageData.setList(list1);
        System.out.println("---------------------");
        System.out.println(list);
        System.out.println(pageData);



    }
    @Test
    void test7(){
        PageData<MemberListItemVO> pageData = memberService.list1(1, 5);
        System.out.println(pageData);
    }

    @Test
    void test8(){
        memberService.deleteById(10L);
        System.out.println("删除完成");

    }
    @Test
    void memberNew(){
        List<MemberNewListVO> list = mapper.memberNew();
        System.out.println(list);

    }
    @Test
    void time(){
        HomeMemberVO homeMemberVO = mapper.homeMember();
        System.out.println(homeMemberVO);
    }

    @Test
    void cardTest(){
        //CartStandardVO list = service.getListById(1L);
        //System.out.println(list);
    }

    @Test
    void cardTest1(){
        service.addTimesById(1L);
    }

}
