package cn.tedu.gym.admin.server.GymClass.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassBookRepository;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.GymClass;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassStandardVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.HomeClassOperateVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest
public class ClassRepositoryTests {
    @Autowired
    ClassRepository repository;
    @Autowired
    ClassBookRepository bookRepository;

    @Test
    void list(){
        Integer pageNum = 1;
        Integer pageSize = 5;
        List<?> list = repository.list(pageNum, pageSize, 1).getList();
        for (Object item : list) {
            System.out.println("列表项: " + item);
        }
    }

    @Test
    void insert(){
        GymClass gymClass = new GymClass();
        gymClass.setName("增肌课");
        gymClass.setImage("https://img2.baidu.com/it/u=3420384266,2933740954&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500");
        gymClass.setType(2);
        gymClass.setTime(60);
        gymClass.setPersonMax(1);
        gymClass.setPersonMin(1);
        gymClass.setCoachId(1L);
        gymClass.setMoney(500.0);
        gymClass.setNote("想要成为rapStar吗");
        gymClass.setTag("增肌");
        gymClass.setIntroduce("可以让你拥有八块腹肌, 增肌塑形");
        gymClass.setSort(88);
        int count = repository.insert(gymClass);
        System.out.println("表中受影响行数: " + count);
    }

    @Test
    void getStandardById(){
        Long id = 2L;
        ClassStandardVO standardById = repository.getStandardById(id);
        System.out.println(standardById);
    }

    @Test
    void listBooking(){
        Integer pageNum=1;
        Integer pageSize=5;
        PageData<ClassBookListItemVO> pageData = bookRepository.listBooking(pageNum, pageSize);
        List<ClassBookListItemVO> list = pageData.getList();
        System.out.println(list);
    }

    @Test
    void  listByClassNameANDCoachName(){

        PageData<ClassBookListItemVO> list = bookRepository.listByClassIdANDCoachId(1L,1L,1,2);

    }
    @Test
    void operatelist(){
        LocalDate classDate=LocalDate.parse("2023-06-01");
        List<HomeClassOperateVO> list = repository.classOperate(classDate,1);
        for (HomeClassOperateVO vo : list) {
            System.out.println(vo);
            System.out.println(vo.getClassData().getClass().getName());
        }
    }
}
