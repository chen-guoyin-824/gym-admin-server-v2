package cn.tedu.gym.admin.server.GymClass.dao.persist.mapper;

import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.ClassBookMapper;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@SpringBootTest
public class BookingMapperTests {
    @Autowired
    ClassBookMapper mapper;
    @Test
    void selectById(){
        Long id=2L;
        ClassBookStartedVO classBookStartedVO = mapper.selectById(id);
        System.out.println(classBookStartedVO);
    }

    @Test
    void bookList(){
        List<ClassBookListItemVO> classBookListItemVOS = mapper.listBooking();
        for (ClassBookListItemVO classBookListItemVO : classBookListItemVOS) {
            System.out.println("列表项:" + classBookListItemVO);
        }
    }

    @Test
    void getStandardByDateTime(){
        LocalDate localDate = LocalDate.parse("2023-08-02");
        LocalTime localTime = LocalTime.parse("14:30:00");
        ClassBookStartedVO standardByDateTime = mapper.getStandardByDateTime(localDate, localTime);
        System.out.println(standardByDateTime);
    }
}
