package cn.tedu.gym.admin.server.GymClass.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.MemberBookMapper;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.ClassBookRepository;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.MemberBookRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookStartedVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ClassBookRepositoryTests {
    @Autowired
    ClassBookRepository repository;
    @Autowired
    MemberBookRepository memberBookRepository;

    @Test
    void cancelById(){
        Long id=4L;
        int i = memberBookRepository.cancelById(id);
        System.out.println(i);
    }


    @Test
    void selectById(){
        Long id=2L;
        ClassBookStartedVO classBookStartedVO = repository.selectById(id);
        System.out.println(classBookStartedVO);
    }

    @Test
    void bookList(){
        Integer pageNum = 1;
        Integer pageSize = 5;
        List<?> list = repository.listBooking(pageNum, pageSize).getList();
        for (Object item : list) {
            System.out.println("列表项: " + item);
        }
    }
}
