package cn.tedu.gym.admin.server.GymClass.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.dao.persist.repository.MemberBookRepository;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class MemberBookRepositoryTests {
    @Autowired
    MemberBookRepository repository;

    @Test
    void listBookingDetail(){
        Long bookClassId = 2L;
        Integer pageNum = 1;
        Integer pageSize = 5;
        List<?> list = repository.listBookingDetail(bookClassId, pageNum, pageSize).getList();
        for (Object item : list) {
            System.out.println("列表项: " + item);
        }
    }
}
