package cn.tedu.gym.admin.server.GymClass.service;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.ClassBook;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookDetailListVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassBookTableListItemVO;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.ClassListItemVO;
import cn.tedu.gym.admin.server.gymClass.service.ClassBookService;
import cn.tedu.gym.admin.server.gymClass.service.ClassService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@SpringBootTest
public class ClassServiceTests {
    @Autowired
    ClassService classService;

    @Autowired
    ClassBookService classBookService;

    @Test
    void list(){
        Integer pageNum = 1;
        List<ClassListItemVO> list = classService.list(pageNum, 1).getList();
        for (ClassListItemVO item : list) {
            System.out.println("列表项: " + item);
        }
    }

    @Test
    void listBooking(){
        Integer pageNum = 1;
        PageData<ClassBookListItemVO> pageData = classBookService.listBooking(pageNum);
        List<ClassBookListItemVO> list = pageData.getList();
        System.out.println(list);
    }

    @Test
    void  listByClassNameANDCoachName(){

        PageData<ClassBookListItemVO> list = classBookService.listByClassIdANDCoachId(1L,1L,1,5);
    }

    @Test
    void listByWeekDate(){
        LocalDate localDate = LocalDate.parse("" +
                "");
        LocalTime localTime = LocalTime.parse("10:00:00");
        List<ClassBookTableListItemVO> classBookTableListItemVOS = classService.listByWeekTime(localDate, localTime);
        System.out.println(classBookTableListItemVOS);
    }
}
