package cn.tedu.gym.admin.server.equipment.dao.persist.mapper;

import cn.tedu.gym.admin.server.equipment.dao.mapper.EquipmentMapper;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentStandardVO;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentTypeListVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class EquipmentMapperTests {
    @Autowired
    EquipmentMapper mapper;
    @Test
    void list(){
        List<EquipmentListItemVO> list = mapper.list();
        System.out.println(list);
    }
    @Test
    void  getStandardVOById(){
        Long id=1L;
        EquipmentStandardVO standardVOById = mapper.getStandardVOById(id);
        System.out.println(standardVOById);
    }

    @Test
    void TYPElist(){
        List<EquipmentTypeListVO> list = mapper.typeList();
        System.out.println(list);
    }
}
