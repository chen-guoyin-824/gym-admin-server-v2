package cn.tedu.gym.admin.server.GymClass.service;

import cn.tedu.gym.admin.server.gymClass.service.ClassBookService;
import cn.tedu.gym.admin.server.gymClass.service.ClassService;
import cn.tedu.gym.admin.server.gymClass.service.MemberBookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookingServiceTests {
    @Autowired
    ClassService service;

    @Autowired
    ClassBookService classBookService;
    @Autowired
    MemberBookService memberBookService;

    @Test
    void cancelById(){
        Long id=1L;
        int i = memberBookService.cancelById(id);
        System.out.println(i);
    }
}
