package cn.tedu.gym.admin.server.adminTests.dao.persist.repository;

import cn.tedu.gym.admin.server.admin.dao.persist.repository.IRoleRepository;
import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class RoleRepesitoryTests {
    @Autowired
    IRoleRepository repository;
    @Test
    void list(){
        List<RoleListItemVO> list = repository.list();
        System.out.println(list);
    }
}
