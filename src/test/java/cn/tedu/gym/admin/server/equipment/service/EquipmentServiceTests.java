package cn.tedu.gym.admin.server.equipment.service;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.equipment.pojo.entity.Equipment;
import cn.tedu.gym.admin.server.equipment.pojo.param.EquipmentUpdateParam;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EquipmentServiceTests {
    @Autowired
    IEquipmentService service;
    @Test
    void list(){
        Integer pageNum=1;
        PageData<EquipmentListItemVO> list = service.list(pageNum);
        System.out.println(list);
    }

    @Test
    void deleteById() {
        Long id = 4L;
        service.deleteById(id);
    }
    @Test
    void upata(){
        EquipmentUpdateParam equipment=new EquipmentUpdateParam();
        equipment.setId(3L);
        equipment.setRemark("测试文字");
        service.updateById(equipment);
    }
}
