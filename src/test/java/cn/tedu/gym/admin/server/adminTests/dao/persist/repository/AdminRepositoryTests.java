package cn.tedu.gym.admin.server.adminTests.dao.persist.repository;

import cn.tedu.gym.admin.server.admin.dao.persist.repository.IAdminRepository;
import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AdminRepositoryTests {
    @Autowired
    IAdminRepository repository;

    @Test
    void adminInfoById(){
        Long id=1L;
        Object adminInfo = repository.adminInfoById(id);
        System.out.println(adminInfo);
    }

    @Test
    void updateAdminInfoById(){
        User user=new User();
        user.setId(1L);
        user.setUsername("测试修改用户名001");
        repository.updateAdminInfoById(user);

    }

    @Test
    void selectCount(){
        String username="lisi";
        int i = repository.selectCount(username);
        System.out.println(i);

    }
    @Test
    void selectCountByPhone(){
        String phone="17628262174";
        int i = repository.selectCountByPhone(phone);
        System.out.println(i);

    }
    @Test
    void adminDetailList(){
        PageData<UserDetailListVO> pageData = repository.adminDetailList(1, 5);
        System.out.println(pageData);
    }
}
