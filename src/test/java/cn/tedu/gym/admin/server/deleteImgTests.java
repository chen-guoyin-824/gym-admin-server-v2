package cn.tedu.gym.admin.server;

import cn.tedu.gym.admin.server.resource.controller.ResourceController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @auther xw
 * @create 2023/7/14
 */
@SpringBootTest
public class deleteImgTests {

    @Test
    void deleteImg(){
        ResourceController rc = new ResourceController();
        String url ="http://localhost:8090/resources/gymInfo-image/2023/07/14/6bb82a67-95ab-4351-ac1d-9331b4a02388.png";
        rc.removeImg(url);
    }
}
