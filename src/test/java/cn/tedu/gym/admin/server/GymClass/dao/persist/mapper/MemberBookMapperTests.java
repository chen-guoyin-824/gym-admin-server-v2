package cn.tedu.gym.admin.server.GymClass.dao.persist.mapper;

import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.MemberBookMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class MemberBookMapperTests {
    @Autowired
    MemberBookMapper mapper;

    @Test
    void listBookingDetail(){
        Long bookId = 1L;
        List<?> list = mapper.listBookingDetail(bookId);
        for (Object item : list) {
            System.out.println("列表项: " + item);
        }
    }
}
