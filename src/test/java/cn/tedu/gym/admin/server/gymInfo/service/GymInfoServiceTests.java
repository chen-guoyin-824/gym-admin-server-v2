package cn.tedu.gym.admin.server.gymInfo.service;

import cn.tedu.gym.admin.server.gymInfo.dao.persist.repository.IGymInfoRepository;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoStandardVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Will 2023/7/9
 */
@SpringBootTest
public class GymInfoServiceTests {
    @Autowired
    private IGymInfoRepository iGymInfoRepository;

    @Test
    void deleteById(){
        iGymInfoRepository.deleteById(8L);
    }

    @Test
    void getStandardById(){
        GymInfoStandardVO standardById = iGymInfoRepository.getStandardById(1L);
        System.out.println(standardById);
    }
}
