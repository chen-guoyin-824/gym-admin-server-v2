package cn.tedu.gym.admin.server.adminTests.dao.persist.mapper;

import cn.tedu.gym.admin.server.admin.dao.persist.mapper.RoleMapper;
import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AdminRoleMapperTests {
    @Autowired
    RoleMapper mapper;
    @Test
    void list(){
        List<RoleListItemVO> list = mapper.list();
        System.out.println(list);
    }
}
