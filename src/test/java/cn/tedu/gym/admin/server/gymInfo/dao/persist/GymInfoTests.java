package cn.tedu.gym.admin.server.gymInfo.dao.persist;

import cn.tedu.gym.admin.server.gymInfo.dao.persist.mapper.GymInfoMapper;
import cn.tedu.gym.admin.server.gymInfo.dao.persist.repository.IGymInfoRepository;
import cn.tedu.gym.admin.server.gymInfo.pojo.entity.GymInfo;
import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @auther xw
 * @create 2023/7/7
 */
@SpringBootTest
public class GymInfoTests {

    @Autowired
    GymInfoMapper mapper;

    @Autowired
    IGymInfoRepository repository;

    @Test
    void mapperList(){
        System.out.println(mapper.list());
    }
    @Test
    void repositoryList(){
        Integer pageNum=1;
        Integer pageSize=5;
        List<GymInfoListItemVO> list = repository.list(pageNum, pageSize).getList();
        for (GymInfoListItemVO gymInfoListItemVO : list) {
            System.out.println("测试查询场馆列表:");
            System.out.println(gymInfoListItemVO);
        }
    }
    @Test
    void insert(){
        GymInfo gymInfo = new GymInfo();
        gymInfo.setTitle("成都分馆");
        repository.insert(gymInfo);
        System.out.println("成功");
    }
}
