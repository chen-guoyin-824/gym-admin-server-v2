package cn.tedu.gym.admin.server.equipment.dao.persist.repository;

import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import cn.tedu.gym.admin.server.equipment.dao.repository.IEquipmentRepository;
import cn.tedu.gym.admin.server.equipment.pojo.vo.EquipmentListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EquipmentRepositoryTests {
    @Autowired
    IEquipmentRepository repository;

    @Test
    void list() {
        Integer pageNum = 1;
        Integer pageSize = 5;
        PageData<EquipmentListItemVO> list = repository.list(pageNum, pageSize);
        System.out.println(list);
    }

    @Test
    void deleteById() {
        Long id = 5L;
        repository.deleteById(id);
    }

}
