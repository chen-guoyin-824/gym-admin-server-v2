package cn.tedu.gym.admin.server.GymClass.dao.persist.mapper;

import cn.tedu.gym.admin.server.coach.dao.persist.mapper.CoachMapper;
import cn.tedu.gym.admin.server.coach.pojo.vo.CoachListItemVO;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.ClassBookMapper;
import cn.tedu.gym.admin.server.gymClass.dao.persist.mapper.ClassMapper;
import cn.tedu.gym.admin.server.gymClass.pojo.entity.GymClass;
import cn.tedu.gym.admin.server.gymClass.pojo.vo.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ClassMapperTests {

    @Autowired
    ClassMapper classMapper;

    @Autowired
    ClassBookMapper classBookMapper;

    @Autowired
    CoachMapper coachMapper;

    @Test
    void list(){
        List<ClassListItemVO> list = classMapper.list(1);
        for (ClassListItemVO classListItemVO : list) {
            System.out.println(classListItemVO);
        }
    }

    @Test
    void listByCoachId(){
        Long coachId = 2L;
        List<ClassListItemVO> list = classMapper.listByCoachId(coachId);
        for (ClassListItemVO classListItemVO : list) {
            System.out.println("列表项: " + classListItemVO);
        }
    }

    @Test
    void insert(){
        GymClass gymClass = new GymClass();
        gymClass.setName("减脂课");
        gymClass.setType(2);
        gymClass.setPersonMax(1);
        gymClass.setPersonMin(1);
        gymClass.setImage("http://localhost:9080/resources/article-image/2023/06/29/48a92b53-719f-4034-b7fb-64fab7365a9f.jpeg");
        gymClass.setTime(50);
        int num = classMapper.insert(gymClass);
        System.out.println("受影响的行数: "+num);
    }

    @Test
    void getStandardById(){
        Long id = 2L;
        ClassStandardVO standardById = classMapper.getStandardById(id);
        System.out.println("课程详情: " + standardById);
    }


    @Test
    void  listBooking(){
        List<ClassBookListItemVO> list = classBookMapper.listBooking();
        System.out.println(list);
    }

    @Test
    void  listByClassNameANDCoachName(){
        String className="减脂增肌课";
        String coachName="root";
        List<ClassBookListItemVO> list = classBookMapper.listByClassIdANDCoachId(1L,1L);
        for (ClassBookListItemVO listItemVO : list) {
            System.out.println(listItemVO);
        }
    }

    @Test
    void coachList(){
        List<CoachListItemVO> list = coachMapper.list();
        for (CoachListItemVO coachListItemVO : list) {
            System.out.println("列表项:" + coachListItemVO);
        }
    }

    @Test
    void listByWeekTime(){
        // 获取当前日期
        LocalDate currentDate = LocalDate.now();

        // 获取当前周的第一天（即当前日期所在周的周一）
        LocalDate firstDayOfWeek = currentDate.with(DayOfWeek.MONDAY);

        // 获取当前周的全部日期
        List<LocalDate> allDatesOfWeek = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            LocalDate date = firstDayOfWeek.plusDays(i);
            allDatesOfWeek.add(date);
        }

        // 格式化输出日期z
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (LocalDate date : allDatesOfWeek) {
            String formattedDate = date.format(formatter);
            System.out.println(formattedDate);
        }
        LocalTime dateTime = LocalTime.of(14, 30, 0);
        System.out.println(dateTime);
        List<ClassBookTableListItemVO> classBookTableList = classMapper.listByWeekTime(allDatesOfWeek, dateTime);
        for (ClassBookTableListItemVO classBookTableListItemVO : classBookTableList) {
            System.out.println(classBookTableListItemVO);
        }
    }
    @Test
    void operatelist(){
        LocalDate classDate=LocalDate.parse("2023-06-01");
        List<HomeClassOperateVO> list = classMapper.classOperate(classDate,1);
        for (HomeClassOperateVO vo : list) {
            System.out.println(vo);
        }
    }
    @Test
    void memberMoney(){
        ClassMoneyCountVO classMoneyCountVO = classMapper.classMoneyCount();
        System.out.println(classMoneyCountVO);
    }
}

