package cn.tedu.gym.admin.server;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class WeekendTest {
    @Test
    void weekendTest(){
        // 获取当前日期
        LocalDate currentDate = LocalDate.now();

        // 获取当前周的第一天（即当前日期所在周的周一）
        LocalDate firstDayOfWeek = currentDate.with(DayOfWeek.MONDAY);

        // 获取当前周的全部日期
        List<LocalDate> allDatesOfWeek = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            LocalDate date = firstDayOfWeek.plusDays(i);
            allDatesOfWeek.add(date);
        }

        // 格式化输出日期
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (LocalDate date : allDatesOfWeek) {
            String formattedDate = date.format(formatter);
            System.out.println(formattedDate);
        }
    }
}
