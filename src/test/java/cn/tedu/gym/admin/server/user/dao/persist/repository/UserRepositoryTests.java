package cn.tedu.gym.admin.server.user.dao.persist.repository;

import cn.tedu.gym.admin.server.user.pojo.vo.UserStandardVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserRepositoryTests {
    @Autowired
    UserRepository repository;

    @Test
    void getStandard(){
        UserStandardVO standardUser = repository.getStandardByUsername("zhangsan");
        System.out.println("用户信息: " + standardUser);
    }
}
