package cn.tedu.gym.admin.server.adminTests.service;

import cn.tedu.gym.admin.server.admin.pojo.vo.RoleListItemVO;
import cn.tedu.gym.admin.server.admin.service.IRoleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class RoleServiceTests {
    @Autowired
    IRoleService service;
    @Test
    void list(){
        List<RoleListItemVO> list = service.list();
        System.out.println(list);
    }
}
