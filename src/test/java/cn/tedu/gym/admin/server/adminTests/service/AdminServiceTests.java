package cn.tedu.gym.admin.server.adminTests.service;

import cn.tedu.gym.admin.server.admin.pojo.entity.User;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminAddNewParam;
import cn.tedu.gym.admin.server.admin.pojo.param.AdminUpdateInfoParam;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.admin.service.IAdminService;
import cn.tedu.gym.admin.server.common.pojo.vo.PageData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AdminServiceTests {
    @Autowired
    IAdminService service;

    @Test
    void adminInfoById(){
        Long id=1L;
        Object adminInfo = service.adminInfoById(id);
        System.out.println(adminInfo);
    }


    @Test
    void updateAdminInfoById(){
        AdminUpdateInfoParam admin=new AdminUpdateInfoParam();
        admin.setId(1L);
        admin.setPassword("123456");
        admin.setPhone("66666666");
        admin.setNewPassword("999999");
        service.updateAdminInfoById(admin);

    }
    @Test
    void addNewAdmin(){

        AdminAddNewParam adminAddNewParam=new AdminAddNewParam();
        adminAddNewParam.setUsername("hhh蜜瓜");
        adminAddNewParam.setPassword("666666");
        adminAddNewParam.setName("李囧囧");
        adminAddNewParam.setPhone("15794840071");
        service.addNewAdmin(adminAddNewParam);

    }


    @Test
    void adminDetailList(){
        PageData<UserDetailListVO> pageData =  service.adminDetailList(1, 5);
        System.out.println(pageData);
    }

}
