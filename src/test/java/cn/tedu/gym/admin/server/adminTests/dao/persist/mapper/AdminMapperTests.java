package cn.tedu.gym.admin.server.adminTests.dao.persist.mapper;

import cn.tedu.gym.admin.server.admin.dao.persist.mapper.AdminMapper;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserDetailListVO;
import cn.tedu.gym.admin.server.admin.pojo.vo.UserListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AdminMapperTests {
    @Autowired
    AdminMapper mapper;

    @Test
    void adminInfoById(){
        Long id=1L;
        Object list = mapper.adminInfoById(id);
        System.out.println(list);
    }
    @Test
    void adminDetailList(){
        List<UserDetailListVO> list = mapper.adminDetailList();
        for (UserDetailListVO listVO : list) {
            System.out.println(listVO);
        }
    }

}
