package cn.tedu.gym.admin.server.gymInfo.controller;

import cn.tedu.gym.admin.server.gymInfo.pojo.vo.GymInfoStandardVO;
import cn.tedu.gym.admin.server.gymInfo.service.IGymInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @auther xw
 * @create 2023/7/12
 */
@SpringBootTest
public class GymInfoControllerTests {
    @Autowired
    IGymInfoService iGymInfoService;

    @Test
    void selectById(){
        GymInfoStandardVO standardById = iGymInfoService.getStandardById(1L);
        System.out.println(standardById);
    }
}
