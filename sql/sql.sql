DROP DATABASE gym;
CREATE DATABASE gym;
USE gym;
/*用户表*/
DROP TABLE IF EXISTS gym_user;
CREATE TABLE gym_user
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '用户id',
    username     VARCHAR(50)         DEFAULT NULL COMMENT '用户名',
    password     VARCHAR(64)         DEFAULT NULL COMMENT '密码',
    name         VARCHAR(50)         DEFAULT NULL COMMENT '姓名',
    gender       TINYINT(3) UNSIGNED DEFAULT '0' COMMENT '性别0:保密 1:男 2:女',
    phone        VARCHAR(25)         DEFAULT NULL COMMENT '手机号',
    avatar       VARCHAR(255)        DEFAULT NULL COMMENT '头像URL',
    sort         TINYINT(3) UNSIGNED DEFAULT '0' COMMENT '排序序号',
    enable       TINYINT(3) UNSIGNED DEFAULT '0' COMMENT '是否启用:0:启用,1:停用',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) COMMENT '用户表 ' CHARSET = utf8mb4;


/*会员详情表*/
DROP TABLE IF EXISTS gym_member;
CREATE TABLE gym_member
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '会员详情id',
    user_id      BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '用户id',
    coach_id     BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '教练id',
    birthday     VARCHAR(20)         DEFAULT NULL COMMENT '生日',
    idcard       VARCHAR(20)         DEFAULT NULL COMMENT '身份证号',
    city         VARCHAR(20)         DEFAULT NULL COMMENT '城市',
    career       VARCHAR(20)         DEFAULT NULL COMMENT '职业',
    height       TINYINT UNSIGNED    DEFAULT NULL COMMENT '身高',
    weight       TINYINT UNSIGNED    DEFAULT NULL COMMENT '体重',
    signature    VARCHAR(25)         DEFAULT NULL COMMENT '个性签名',
    exp          VARCHAR(20)         DEFAULT NULL COMMENT '健身经验',
    target       VARCHAR(50)         DEFAULT NULL COMMENT '健身目的',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) COMMENT '会员详情表 ' CHARSET = utf8mb4;

/*教练详情表*/
DROP TABLE IF EXISTS gym_coach;
CREATE TABLE gym_coach
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '教练id',
    user_id      BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '用户id',
    gym_id       BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '场馆id',
    target       VARCHAR(50)         DEFAULT NULL COMMENT '个人签名',
    profile      VARCHAR(255)        DEFAULT NULL COMMENT '个人简介',
    worktime     VARCHAR(10)         DEFAULT NULL COMMENT '从业时间',
    skills       VARCHAR(50)         DEFAULT NULL COMMENT '擅长标签',
    upper_limit  TINYINT(1)          DEFAULT NULL COMMENT '每日私教课上限',
    photo        VARCHAR(500)        DEFAULT NULL COMMENT '教练相册',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'

) COMMENT '教练详情表 ' CHARSET = utf8mb4;
/*角色表*/
DROP TABLE IF EXISTS gym_role;
CREATE TABLE gym_role
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '角色id',
    name         VARCHAR(20)         DEFAULT NULL COMMENT '角色名称',
    description  VARCHAR(255)        DEFAULT NULL COMMENT '简介',
    sort         TINYINT(3) UNSIGNED DEFAULT '0' COMMENT '排序序号',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT '角色表 ';

/*用户角色关联*/
DROP TABLE IF EXISTS gym_user_role;
CREATE TABLE gym_user_role
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '数据ID',
    user_id      BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '用户ID',
    role_id      BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '角色ID',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'

) DEFAULT CHARSET = utf8mb4 COMMENT ='用户角色关联';

/*权限表*/
DROP TABLE IF EXISTS gym_permission;
CREATE TABLE gym_permission
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '数据ID',
    name         VARCHAR(50)         DEFAULT NULL COMMENT '名称',
    value        VARCHAR(255)        DEFAULT NULL COMMENT '值',
    description  VARCHAR(255)        DEFAULT NULL COMMENT '简介',
    sort         TINYINT(3) UNSIGNED DEFAULT '0' COMMENT '排序序号',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT ='权限表';

/*角色权限关联*/
DROP TABLE IF EXISTS gym_role_permission;
CREATE TABLE gym_role_permission
(
    id            BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '数据id',
    role_id       BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '角色ID',
    permission_id BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '权限ID',
    gmt_create    DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified  DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT ='角色权限关联';

/*器材表*/
DROP TABLE IF EXISTS gym_equipment;
CREATE TABLE gym_equipment
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '器材id',
    name         VARCHAR(50)         DEFAULT NULL COMMENT '器材名称',
    type_id      BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '分类id',
    img          VARCHAR(255)        DEFAULT NULL COMMENT '器材图片',
    enable       TINYINT UNSIGNED    DEFAULT NULL COMMENT '是否启用, 1=启用, 0=不启用',
    state        TINYINT UNSIGNED    DEFAULT NULL COMMENT '器材状态,1=良好,2=损坏,3=维修中',
    remark       VARCHAR(50)         DEFAULT NULL COMMENT '备注信息',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT '器材表 ';

/*器材类别表*/
DROP TABLE IF EXISTS gym_equipment_type;
CREATE TABLE gym_equipment_type
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '器材类别id',
    type_name    VARCHAR(50) DEFAULT NULL COMMENT '器材类别名称',
    gmt_create   DATETIME    DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME    DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT '器材类别表 ';

/*课程表*/
DROP TABLE IF EXISTS gym_class;
CREATE TABLE gym_class
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '课程id',
    name         VARCHAR(20)      DEFAULT '' COMMENT '课程名称',
    image        VARCHAR(255)     DEFAULT '' COMMENT '课程图片',
    type         TINYINT UNSIGNED DEFAULT '1' COMMENT '课程类别, 1=团课课程, 2=私教课课程',
    time         TINYINT(3) UNSIGNED COMMENT '课程时长/分钟',
    person_max   INT(3) UNSIGNED COMMENT '上课最大人数',
    person_min   INT(3) UNSIGNED COMMENT '上课最少人数',
    coach_id     BIGINT UNSIGNED COMMENT '教练ID',
    money        DECIMAL(10, 4) UNSIGNED COMMENT '课时费',
    note         VARCHAR(255)     DEFAULT '' COMMENT '课程备注',
    tag          VARCHAR(255)     DEFAULT '' COMMENT '课程标签',
    introduce    VARCHAR(255)     DEFAULT '' COMMENT '课程介绍',
    sort         TINYINT UNSIGNED DEFAULT 0 NOT NULL COMMENT '排序序号',
    gmt_create   DATETIME         DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME         DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT '课程表';
/*场馆信息表*/
DROP TABLE IF EXISTS gym_info;
CREATE TABLE gym_info
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '场馆id',
    img          VARCHAR(255)        DEFAULT NULL COMMENT '场馆图片',
    title        VARCHAR(25)         DEFAULT NULL COMMENT '场馆名称',
    phone        VARCHAR(25)         DEFAULT NULL COMMENT '手机号',
    email        VARCHAR(25)         DEFAULT NULL COMMENT '邮箱',
    introduce    VARCHAR(255)        DEFAULT NULL COMMENT '场馆介绍',
    address      VARCHAR(255)        DEFAULT NULL COMMENT '场馆地址',
    sort         TINYINT UNSIGNED    DEFAULT 0 NOT NULL COMMENT '排序序号',
    enable       TINYINT(3) UNSIGNED DEFAULT NULL COMMENT '是否启用:0:启用,1:停用',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
);
/**
  预约表
 */
DROP TABLE IF EXISTS gym_book_member;
CREATE TABLE gym_book_member
(
    id             BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '数据id',
    book_class_id  BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '预约课程id',
    member_id      BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '会员id',
    sign_in_status TINYINT(1) UNSIGNED DEFAULT 0 COMMENT '会员签到状态, 0表示未签到, 1表示已签到',
    gmt_create     DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified   DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT '预约会员表';

DROP TABLE IF EXISTS gym_book_class;
CREATE TABLE gym_book_class
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '数据id',
    class_id     BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '课程id',
    class_date   DATE                DEFAULT NULL COMMENT '上课日期',
    class_time   TIME                DEFAULT NULL COMMENT '上课时间',
    book_number  INT UNSIGNED        DEFAULT 0 COMMENT '预约人数',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT '预约课程表';
/*
 会员卡表
 */
DROP TABLE IF EXISTS gym_member_card;
CREATE TABLE gym_member_card
(
    id           BIGINT(20) UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '数据id',
    member_id    BIGINT(20) UNSIGNED DEFAULT NULL COMMENT '会员id',
    member_level TINYINT(20)         DEFAULT 0 COMMENT '会员等级',
    name         VARCHAR(30)         DEFAULT '' COMMENT '会员卡名称',
    count        TINYINT(20)         DEFAULT 0 COMMENT '剩余次数',
    status       TINYINT(10)         DEFAULT 0 COMMENT '状态  ',
    gmt_create   DATETIME            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified DATETIME            DEFAULT NULL COMMENT '数据最后修改时间'
) DEFAULT CHARSET = utf8mb4 COMMENT '会员卡表';




